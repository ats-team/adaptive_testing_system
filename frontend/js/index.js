import React from "react";
import { render } from 'react-dom';
import { BrowserRouter, Switch, Route } from 'react-router-dom';

import Home from './components/home'
import App from './components/app'

render((
    <BrowserRouter basename="/ats">
        <Switch>
            <Route exact path='/' component={Home} />
            <Route exact path='/home' component={Home} />
            <Route path='/' component={App} />
        </Switch>
    </BrowserRouter>
), document.getElementById('root'));