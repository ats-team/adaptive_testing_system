import React from "react";

import Test from "./Test";

export default class TestsList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {
        const tests = this.props.tests !== undefined && this.props.tests.length > 0 ?
            this.props.tests.map(test => <Test key={test.id} test={test}/>) : <p>Нет доступных тестов</p>;

        return (
            <div>
                {tests}
            </div>
        );
    }
}