import React from 'react';
import Axios from 'axios';

import TestsFilterForm from './TestsFilterForm';
import TestsList from'./TestsList';
import {common_request_errors_handle} from "../helper_functions";

export default class InvitationsPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};

        this.loadTests = this.loadTests.bind(this);
    }

    componentDidMount() {
        this.loadTests();
    }

    loadTests(params) {
        Axios({
            method: 'get',
            url: '/internal_api/tests/available',
            params: params,
            responseType: 'json'
        })
            .then(response => this.setState({tests: response.data}))
            .catch(common_request_errors_handle);
    }

    render() {
        return (
            <div>
                <div className="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                    <h1 className="h2">Приглашения</h1>
                </div>

                <TestsFilterForm updateTests={this.loadTests}/>
                <TestsList tests={this.state.tests}/>
            </div>
        );
    }
}