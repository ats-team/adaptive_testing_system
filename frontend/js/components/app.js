import React from 'react';
import {Link, Route, Switch} from 'react-router-dom';

import AllPublicTestsPage from "./AllPublicTestsPage";
import TestDescriptionPage from "./TestDescriptionPage";
import TestCreationPage from "./TestCreationPage";
import TestEditPage from "./TestEditPage/TestEditPage";
import LevelListPage from "./LevelListPage/LevelListPage";
import ProfilePage from "./profilePage/ProfilePage";
import InvitationsPage from "./InvitationsPage";
import ResultsOfPassedTestPage from "./ResultsOfPassedTestPage/ResultsOfPassedTestPage";
import PassedTestsPage from "./PassedTestsPage/PassedTestsPage";
import LevelCreationPage from "./levelCreationPage/LevelCreationPage";
import QuestionPage from "./QuestionPage";
import MyTestsPage from "./MyTestsPage/MyTestsPage";
import ResultsOfMyTestPage from "./ResultsOfMyTestPage/ResultsOfMyTestPage";
import PassedQuestionPage from "./PassedQuestionPage";

const NavBar = () => (
    <nav className="navbar navbar-expand-md navbar-dark fixed-top bg-dark flex-md-nowrap p-0 shadow">
        {/* Brand */}
        <Link to="/" className="navbar-brand col-sm-3 col-md-2 col-2 mr-0">
            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
             stroke="currentColor" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round"
             className="feather feather-user-check">
                <path d="M16 21v-2a4 4 0 0 0-4-4H5a4 4 0 0 0-4 4v2"/>
                <circle cx="8.5" cy="7" r="4"/>
                <polyline points="17 11 19 13 23 9"/>
            </svg>
        ATS</Link>
        <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarToggler"
            aria-controls="navbarToggler" aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon"/>
        </button>
        <div className="collapse navbar-collapse" id="navbarToggler">
            {/* Search form */}
            <input className="form-control form-control-dark w-100" type="text"
               placeholder="Поиск (по названию публичного теста)" aria-label="Search" />
            {/* Menu */}
            <ul className="navbar-nav px-3">
                <div className="d-md-none d-lg-none d-xl-none">
                    <li className="nav-item active">
                        <Link to="/profile" className="nav-link" >Профиль <span className="sr-only">(current)</span></Link>
                    </li>
                    <li className="nav-item">
                        <Link to="/tests/available" className="nav-link">Приглашения</Link>
                    </li>
                    <li className="nav-item">
                        <Link to="/passed_tests" className="nav-link">Пройденные тесты</Link>
                    </li>
                    <li className="nav-item">
                        <Link to="/tests/my" className="nav-link">Созданные тесты</Link>
                    </li>
                    <li className="nav-item">
                        <Link to="/tests" className="nav-link">Все тесты</Link>
                    </li>
                </div>
                <li className="nav-item text-nowrap">
                    <a href={"/logout"} className="nav-link">Выход</a>
                </li>
            </ul>
        </div>
</nav>);

const Advertisement = () => (
    <div>
        <h6 className="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
            <span>Реклама</span>
            <a className="d-flex align-items-center text-muted"
               href="https://getbootstrap.com/docs/4.1/examples/dashboard/#">
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                     stroke="currentColor" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round"
                     className="feather feather-plus-circle">
                    <circle cx="12" cy="12" r="10"/>
                    <line x1="12" y1="8" x2="12" y2="16"/>
                    <line x1="8" y1="12" x2="16" y2="12"/>
                </svg>
            </a>
        </h6>
        <ul className="nav flex-column mb-2">
            <li className="nav-item">
                <a className="nav-link" href="https://getbootstrap.com/docs/4.1/examples/dashboard/#">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                         fill="none" stroke="currentColor" strokeWidth="2" strokeLinecap="round"
                         strokeLinejoin="round" className="feather feather-file-text">
                        <path d="M14 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V8z"/>
                        <polyline points="14 2 14 8 20 8"/>
                        <line x1="16" y1="13" x2="8" y2="13"/>
                        <line x1="16" y1="17" x2="8" y2="17"/>
                        <polyline points="10 9 9 9 8 9"/>
                    </svg>
                    VPN для доступа к Telegram по <b>демократичным</b> ценам
                </a>
            </li>
            <li className="nav-item">
                <a className="nav-link" href="https://getbootstrap.com/docs/4.1/examples/dashboard/#">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                         fill="none" stroke="currentColor" strokeWidth="2" strokeLinecap="round"
                         strokeLinejoin="round" className="feather feather-file-text">
                        <path d="M14 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V8z"/>
                        <polyline points="14 2 14 8 20 8"/>
                        <line x1="16" y1="13" x2="8" y2="13"/>
                        <line x1="16" y1="17" x2="8" y2="17"/>
                        <polyline points="10 9 9 9 8 9"/>
                    </svg>
                    Вкусняшки тут
                </a>
            </li>
            <li className="nav-item">
                <a className="nav-link" href="https://getbootstrap.com/docs/4.1/examples/dashboard/#">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                         fill="none" stroke="currentColor" strokeWidth="2" strokeLinecap="round"
                         strokeLinejoin="round" className="feather feather-file-text">
                        <path d="M14 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V8z"/>
                        <polyline points="14 2 14 8 20 8"/>
                        <line x1="16" y1="13" x2="8" y2="13"/>
                        <line x1="16" y1="17" x2="8" y2="17"/>
                        <polyline points="10 9 9 9 8 9"/>
                    </svg>
                    Лопаты для открытия дачного сезона
                </a>
            </li>
            <li className="nav-item">
                <a className="nav-link" href="https://getbootstrap.com/docs/4.1/examples/dashboard/#">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                         fill="none" stroke="currentColor" strokeWidth="2" strokeLinecap="round"
                         strokeLinejoin="round" className="feather feather-file-text">
                        <path d="M14 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V8z"/>
                        <polyline points="14 2 14 8 20 8"/>
                        <line x1="16" y1="13" x2="8" y2="13"/>
                        <line x1="16" y1="17" x2="8" y2="17"/>
                        <polyline points="10 9 9 9 8 9"/>
                    </svg>
                    Обмен криптовалюты по выгодному курсу
                </a>
            </li>
        </ul>
    </div>
);

const SideBar = () => (
    <nav className="col-md-2 d-none d-md-block bg-light sidebar">
    <div className="sidebar-sticky">
        <ul className="nav flex-column">
            <li className="nav-item">
                <Link to="/profile" className="nav-link active">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                         fill="none" stroke="currentColor" strokeWidth="2" strokeLinecap="round"
                         strokeLinejoin="round" className="feather feather-home">
                        <path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"/>
                        <polyline points="9 22 9 12 15 12 15 22"/>
                    </svg>
                    Профиль <span className="sr-only">(current)</span>
                </Link>
            </li>
            <li className="nav-item">
                <Link to="/tests/available" className="nav-link">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                         fill="none" stroke="currentColor" strokeWidth="2" strokeLinecap="round"
                         strokeLinejoin="round" className="feather feather-users">
                        <path d="M17 21v-2a4 4 0 0 0-4-4H5a4 4 0 0 0-4 4v2"/>
                        <circle cx="9" cy="7" r="4"/>
                        <path d="M23 21v-2a4 4 0 0 0-3-3.87"/>
                        <path d="M16 3.13a4 4 0 0 1 0 7.75"/>
                    </svg>
                    Приглашения
                </Link>
            </li>
            <li className="nav-item">
                <Link to="/passed_tests" className="nav-link">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                         fill="none" stroke="currentColor" strokeWidth="2" strokeLinecap="round"
                         strokeLinejoin="round" className="feather feather-bar-chart-2">
                        <line x1="18" y1="20" x2="18" y2="10"/>
                        <line x1="12" y1="20" x2="12" y2="4"/>
                        <line x1="6" y1="20" x2="6" y2="14"/>
                    </svg>
                    Пройденные тесты
                </Link>
            </li>
            <li className="nav-item">
                <Link to="/tests/my" className="nav-link">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                         fill="none" stroke="currentColor" strokeWidth="2" strokeLinecap="round"
                         strokeLinejoin="round" className="feather feather-file-text">
                        <path d="M14 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V8z"/>
                        <polyline points="14 2 14 8 20 8"/>
                        <line x1="16" y1="13" x2="8" y2="13"/>
                        <line x1="16" y1="17" x2="8" y2="17"/>
                        <polyline points="10 9 9 9 8 9"/>
                    </svg>
                    Созданные тесты
                </Link>
            </li>
            <li className="nav-item">
                <Link to="/tests" className="nav-link">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                         fill="none" stroke="currentColor" strokeWidth="2" strokeLinecap="round"
                         strokeLinejoin="round" className="feather feather-layers">
                        <polygon points="12 2 2 7 12 12 22 7 12 2"/>
                        <polyline points="2 17 12 22 22 17"/>
                        <polyline points="2 12 12 17 22 12"/>
                    </svg>
                    Все тесты
                </Link>
            </li>
        </ul>

        <Advertisement />

    </div>
</nav>);

export default class App extends React.Component {
    render() {
        return (
                <div>

                <NavBar />

                <div className="container-fluid">
                    <div className="row">

                        <SideBar />

                        <main role="main" className="col-md-9 ml-sm-auto col-lg-10 px-4">

                            <Switch>
                                <Route exact path='/profile' component={ProfilePage}/>
                                <Route exact path='/tests' component={AllPublicTestsPage}/>
                                <Route exact path='/tests/my' component={MyTestsPage}/>
                                <Route exact path='/tests/my/:testId(\d+)/results' component={ResultsOfMyTestPage}/>
                                <Route exact path='/tests/available' component={InvitationsPage}/>
                                <Route exact path='/tests/create' component={TestCreationPage}/>
                                <Route exact path='/tests/:testId(\d+)/edit' component={TestEditPage}/>
                                <Route exact path='/tests/:testId(\d+)' component={TestDescriptionPage}/>
                                <Route exact path='/tests/:testId(\d+)/question/:questionId(\d+)' component={QuestionPage}/>
                                <Route exact path='/question_creation_data/:questionId(\d+)' component={LevelListPage}/>
                                <Route exact path='/question_creation_data/:questionId(\d+)/create_level'
                                       render={(props) => <LevelCreationPage mode='create' {...props}/>}/>
                                <Route exact path='/question_creation_data/:questionId(\d+)/levels/:levelId(\d+)'
                                       render={(props) => <LevelCreationPage mode='edit' {...props}/>}/>
                                <Route exact path='/passed_tests' component={PassedTestsPage}/>
                                <Route exact path='/passed_tests/:passedTestId(\d+)' component={ResultsOfPassedTestPage}/>
                                <Route exact path='/passed_questions/:passedQuestionId(\d+)' component={PassedQuestionPage}/>
                                {/* insert routes here */}
                            </Switch>

                        </main>
                    </div>
                </div>
            </div>
        );
    }
}
