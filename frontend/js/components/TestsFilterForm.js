import React from "react";

export default class TestsFilterForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            login: '',
            fio: ''
        };
        this.updateTests = props.updateTests;

        this.handleInputChange = this.handleInputChange.bind(this);
        this.filter = this.filter.bind(this);
    }

    handleInputChange(event) {
        const target = event.target;
        this.setState({
            [target.name]: target.value
        })
    }

    filter(event) {
        this.updateTests(this.state);
    }

    render() {
        return (
            <div>
                <p>
                    <button className="btn btn-info collapsed" type="button" data-toggle="collapse" data-target="#collapseExample"
                            aria-expanded="false" aria-controls="collapseExample">
                        Фильтры
                    </button>
                </p>
                <div className="collapse" id="collapseExample">
                    <div className="card card-body mb-2">
                        <div>
                            <div className="form-row mb-2">
                                <div className="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4 mb-2">
                                    <input type="text" className="form-control" placeholder="Название теста"
                                           name="name" onChange={this.handleInputChange} value={this.state.name}/>
                                </div>
                                <div className="col-12 col-sm-12 col-md-12 col-lg-3 col-xl-3 mb-2">
                                    <div className="input-group">
                                        <div className="input-group-prepend">
                                            <span className="input-group-text" id="inputGroupPrepend2">@</span>
                                        </div>
                                        <input type="text" className="form-control" id="validationDefaultUsername"
                                               placeholder="Логин составителя" aria-describedby="inputGroupPrepend2"
                                               name="login" onChange={this.handleInputChange} value={this.state.login}/>
                                    </div>
                                </div>
                                <div className="col-12 col-sm-12 col-md-12 col-lg-3 col-xl-3 mb-2">
                                    <input type="text" className="form-control" placeholder="ФИО составителя"
                                           name="fio" onChange={this.handleInputChange} value={this.state.fio}/>
                                </div>
                                <div className="col-12 col-sm-12 col-md-12 col-lg-2 col-xl-2">
                                    <button onClick={this.filter} className="btn btn-success mb-2">Отфильтровать</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}