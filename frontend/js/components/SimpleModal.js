import React from "react";

export default class TestCreationPage extends React.Component {
    constructor(props) {
        super(props);
        this.close = this.close.bind(this);
    }

    close() {
        this.props.wrapper.setState({modalText: "..."});
        $('#exampleModal').modal('toggle')
    }

    render() {
        return (
            <div className="modal" id="exampleModal" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div className="modal-dialog" role="document">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title" id="exampleModalLabel">{this.props.title}</h5>
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div className="modal-body">
                            {this.props.text}
                        </div>
                        {this.props.showFooter ? <div className="modal-footer">
                            <button type="button" onClick={this.close} className="btn btn-primary">Ок</button>
                        </div> : ""}
                    </div>
                </div>
            </div>
        );
    }
}