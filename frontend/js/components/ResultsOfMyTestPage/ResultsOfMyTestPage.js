import React from 'react';
import Axios from 'axios';

import {common_request_errors_handle} from "../../helper_functions";
import ResultsOfMyTestList from "./ResultsOfMyTestList";

export default class ResultsOfMyTestPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: {},
        };
        this.loadData = this.loadData.bind(this);
    }

    componentDidMount() {
        this.loadData();
    }

    loadData() {
        let this_ = this;
        let testId = this.props.match.params.testId;
        Axios({
            method: 'get',
            url: `/internal_api/tests/my/${testId}/results`,
            responseType: 'json'
        }).then(function (response) {
            this_.setState({data: response.data});
        }).catch(common_request_errors_handle);
    }

    render() {
        return (
            <div className="jumbotron" style={{paddingTop: "20px"}}>
                <h1 className="display-4">{this.state.data.name}</h1>
                <hr className="my-4"/>
                <p className="lead">Дата создания: {new Date(this.state.data.date_created * 1000).toLocaleString("ru")}</p>
                <p className="lead">Вопросов: {this.state.data.question_count}</p>
                <p className="lead">Максимальная продолжительность прохождения: {this.state.data.max_duration/60|0 } мин. {this.state.data.max_duration % 60 } сек.</p>
                <p className="lead">{this.state.data.detailed_description}</p>
                <p className="lead">Количество прохождений: {this.state.data.count_of_participants}</p>
                <ResultsOfMyTestList results={this.state.data.participants} />
            </div>
        )
    }
}