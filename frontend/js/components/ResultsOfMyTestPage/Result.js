import React from 'react';
import {Link} from "react-router-dom";

export default class Result extends React.Component {
    constructor(props) {
        super(props);
        this.state = {}
    }

    render() {
        return (
            <tr>
                <td>
                    <Link to={`/passed_tests/${this.props.result.passed_test_id}`} className="list-group-item-action">
                        <div className="row">
                            <div className="col-3">
                                {this.props.result.login}
                            </div>
                            <div className="col-2">
                                {this.props.result.score}
                            </div>
                            <div className="col-2">
                                {this.props.result.complexity}
                            </div>
                            <div className="col-2">
                                {this.props.result.duration/60|0 } мин. {this.props.result.duration % 60 } сек.
                            </div>
                            <div className="col-3">
                                {new Date(this.props.result.date * 1000).toLocaleString("ru")}
                            </div>
                        </div>
                    </Link>
                </td>
            </tr>
        );
    }
}