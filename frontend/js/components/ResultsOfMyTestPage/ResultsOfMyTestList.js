import React from "react";
import Result from "./Result";


export default class ResultsOfMyTestList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {
        const has_results = this.props.results !== undefined && this.props.results.length > 0;
        const results = has_results ?
            this.props.results.map(result => <Result result={result}/>) : <p>Никто не прошёл этот тест</p>;

        return (
            <div>
                {has_results ?
                    <table className="table table-striped table-hover">
                        <thead>
                        <tr>
                            <td>
                                <div className="row">
                                    <div className="col-3">
                                        Логин
                                    </div>
                                    <div className="col-2">
                                        Балл
                                    </div>
                                    <div className="col-2">
                                        Сложность
                                    </div>
                                    <div className="col-2">
                                        Время
                                    </div>
                                    <div className="col-3">
                                        Дата
                                    </div>
                                </div>
                            </td>
                        </tr>
                        </thead>
                        <tbody>
                            {results}
                        </tbody>
                    </table>
                    : results}
            </div>
        );
    }
}