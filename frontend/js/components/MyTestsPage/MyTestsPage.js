import React from 'react';
import Axios from 'axios';
import {Link} from "react-router-dom";
import MyTestsList from "./MyTestsList";
import MyTestsFilterForm from "./MyTestsFilterForm";


export default class MyTestsPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};

        this.loadTests = this.loadTests.bind(this);
    }

    componentDidMount() {
        this.loadTests({});
    }

    loadTests(params) {
        let this_ = this;
        Axios({
            method: 'get',
            url: '/internal_api/tests/my',
            params: params,
            responseType: 'json'
        }).then(function (response) {
            this_.setState({tests: response.data});
        }).catch(function (error) {
            if (error.response) console.log(error.response.status);
            else if (error.request) console.log(error.request);
            else console.log(error.message);
        });
    }

    render() {
        return (
            <div>
                <div
                    className="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                    <h1 className="h2 mr-2">Созданные тесты <Link to="/tests/create">(создать новый)</Link></h1>
                </div>

                <MyTestsFilterForm updateTests={this.loadTests}/>
                <MyTestsList tests={this.state.tests}/>
            </div>
        );
    }
}