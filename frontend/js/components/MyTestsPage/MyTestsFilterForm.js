import React from "react";

export default class MyTestsFilterForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name: '',
        };
        this.updateTests = props.updateTests;

        this.handleInputChange = this.handleInputChange.bind(this);
        this.filter = this.filter.bind(this);
    }

    handleInputChange(event) {
        const target = event.target;
        this.setState({
            [target.name]: target.value
        })
    }

    filter(event) {
        this.updateTests(this.state);
    }

    render() {
        return (
            <form>
                <div className="form-row mb-2">
                    <div className="col-7 col-md-8 col-lg-9">
                        <input type="text" onChange={this.handleInputChange} className="form-control"
                               placeholder="Название теста"/>
                    </div>
                    <div className="col-2 col-md-3 col-lg-2">
                        <button type="submit" onClick={this.filter} className="btn btn-primary mb-2">Отфильтровать
                        </button>
                    </div>
                </div>
            </form>
        );
    }
}