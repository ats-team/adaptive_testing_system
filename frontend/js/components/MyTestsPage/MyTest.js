import React from 'react';
import {Link} from "react-router-dom";

export default class MyTest extends React.Component {
    constructor(props) {
        super(props);
        this.state = {}
    }

    render() {
        return (
            <div className="card mb-2">
                <h5 className="card-header">{this.props.test.name}</h5>
                <div className="card-body">
                    <h5 className="card-title">Число прохождений: {this.props.test.count_of_participants}</h5>
                    <p className="card-text">{this.props.test.short_description}</p>
                    <p className="card-text">
                        <small className="text-muted">Максимальная продолжительность прохождения: {this.props.test.max_duration/60|0 } мин. {this.props.test.max_duration % 60 } сек.<br />
                            Статус: {this.props.test.is_private ? "приватный" : "публичный"},{' '}
                            {this.props.test.is_available ? "доступный для прохождения" : "недоступный для прохождения"}
                        </small>
                    </p>
                    {this.props.test.is_available ?
                        <Link to={`/tests/my/${this.props.test.id}/results`}><button type="button" className="btn btn-primary">Смотреть результаты</button></Link>
                    : <Link to={`/tests/${this.props.test.id}/edit`}><button type="button" className="btn btn-success">Редактировать</button></Link>}
                </div>
            </div>
        );
    }
}