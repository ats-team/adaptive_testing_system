import React from "react";
import MyTest from "./MyTest";

export default class PassedTestsList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {
        const tests = this.props.tests !== undefined && this.props.tests.length > 0 ?
            this.props.tests.map(test => <MyTest key={test.id} test={test}/>) : <p>Вы не создали ни одного теста</p>;

        return (
            <div>
                {tests}
            </div>
        );
    }
}