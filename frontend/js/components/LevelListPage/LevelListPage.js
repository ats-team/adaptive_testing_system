import React from 'react';
import Axios from 'axios';

import {common_request_errors_handle} from "../../helper_functions";
import LevelList from "./LevelList";
import {Link} from "react-router-dom";

export default class LevelListPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            questionId: '',
        };

        this.loadLevels = this.loadLevels.bind(this);
    }

    componentDidMount() {
        this.loadLevels();
    }

    loadLevels() {
        let this_ = this;
        let questionId = this.props.match.params.questionId;
        Axios({
            method: 'get',
            url: `/internal_api/question_creation_data/${questionId}`,
            responseType: 'json'
        }).then(function (response) {
            this_.setState({data: response.data,
                            questionId: questionId});
        }).catch(common_request_errors_handle);
    }

    render() {
        return (
            <div>
                <div className="pt-3 pb-2 mb-3 border-bottom">
                    <h1 className="h2">{this.state.data.question_name} - уровни <Link to={`/question_creation_data/${this.state.questionId}/create_level`}><span className="fa fa-plus" aria-hidden="true"
                                                                             style={{color: "#009900"}} data-toggle="modal"
                                                                             data-target="#addAndEditElectiveModal"/></Link>
                    </h1>
                    <small id="level-help" className="form-text text-muted mb-2">
                        Заполнение ведётся от простых уровней к сложным.
                    </small>
                </div>
                <LevelList levels={this.state.data.levels} wrapper={this}/>
                <Link to={`/tests/${this.state.data.test_id}/edit`}><button type="button" className="btn btn-info">Назад на страницу редактирования теста</button></Link>
            </div>
        )
    }
}