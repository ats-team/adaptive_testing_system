import React from 'react';
import {Link} from "react-router-dom";
import Axios from "axios/index";
import {common_request_errors_handle} from "../../helper_functions";

export default class Test extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    deleteLevel(id, event) {
        let this_ = this;
        Axios({
            method: 'delete',
            url: `/internal_api/levels`,
            params: {level_id_to_delete: id},
            responseType: 'json'
        }).then(function (response) {
            this_.props.wrapper.loadLevels();
        }).catch(common_request_errors_handle);
    }

    render() {
        return (
            <li className="list-group-item list-group-item-action d-flex justify-content-between align-items-center">
                <Link to={`/levels/update/${this.props.level.id}`}>Уровень {this.props.level.level}</Link>
                <span>
                        <span onClick={this.deleteLevel.bind(this, this.props.level.id)} className="fa fa-remove" aria-hidden="true"
                              style={{color: "#cc0000", fontSize:"1.5em", cursor: "pointer"}}/>
                </span>
            </li>
        );
    }
}