import React from "react";
import Level from "./Level";


export default class LevelList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {
        const levels = this.props.levels !== undefined && this.props.levels.length > 0 ?
            this.props.levels.map(level => <Level level={level} wrapper={this.props.wrapper}/>) : <p>Вы ещё не создали ни одного уровня</p>;

        return (
            <ul className="list-group mb-2">
                {levels}
            </ul>
        );
    }
}