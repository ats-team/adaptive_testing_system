import React from 'react';
import Axios from 'axios';

import {common_request_errors_handle, getURlSearchParamsFromObject} from '../helper_functions';

export default class TestCreationPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            shortDescription: '',
            detailedDescription: '',
            maxDuration: '',
            isPrivate: '',
            isQuestionsDetailsAvailable: ''
        };
        this.handleInputChange = this.handleInputChange.bind(this);
        this.createTest = this.createTest.bind(this);
    }

    handleInputChange(event) {
        const target = event.target;
        let value = '';
        if (target.type === 'checkbox') {
            if (target.checked){
                value = target.value;
            } else {
                value = '';
            }
        } else {
            value = target.value;
        }
        this.setState({
            [target.name]: value
        })
    }

    createTest(event) {
        event.preventDefault();
        let this_ = this;
        Axios({
            method: 'post',
            url: `/internal_api/tests/create`,
            data: getURlSearchParamsFromObject(this.state),
            responseType: 'json'
        }).then(function (response) {
            let created_test_id = response.data.id;
            this_.props.history.push(`/tests/${created_test_id}/edit`);
        }).catch(common_request_errors_handle);
    };

    render() {
        return (
            <div className="mb-2">
                <div
                    className="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                    <h1 className="h2">Создание теста</h1>
                </div>
                <div className="col-md-8">
                    <form onSubmit={this.createTest}>
                        <div className="form-group">
                            <label htmlFor="form-test-name">Название теста</label>
                            <input name="name" value={this.state.name} onChange={this.handleInputChange} type="text" className="form-control" id="form-test-name" required />
                        </div>
                        <div className="form-group">
                            <label htmlFor="form-test-short-description">Краткое описание</label>
                            <input name="shortDescription" value={this.state.shortDescription} onChange={this.handleInputChange} type="text" className="form-control" id="form-test-short-description" />
                        </div>
                        <div className="form-group">
                            <label htmlFor="form-test-detailed-description">Подробное описание</label>
                            <textarea name="detailedDescription" value={this.state.detailedDescription} onChange={this.handleInputChange} className="form-control" id="form-test-detailed-description" rows="3" />
                        </div>
                        <div className="form-group">
                            <label htmlFor="form-password">Продолжительность (в минутах)</label>
                            <input name="maxDuration" value={this.state.maxDuration} onChange={this.handleInputChange} type="number" className="form-control" id="form-test-duration" min="1" step="1" />
                        </div>
                        <div className="form-group">
                            <div className="form-check">
                                <input name="isPrivate" onChange={this.handleInputChange} className="form-check-input" type="checkbox" value="isPrivate"
                                       id="form-test-is-private" />
                                    <label className="form-check-label" htmlFor="form-test-is-private">
                                        Приватный тест
                                    </label>
                            </div>
                        </div>
                        <div className="form-group">
                            <div className="form-check">
                                <input name="isQuestionsDetailsAvailable" onChange={this.handleInputChange} className="form-check-input" type="checkbox" value="isQuestionsDetailsAvailable"
                                       id="form-test-is-questions-available" />
                                    <label className="form-check-label" htmlFor="form-test-is-questions-available">
                                        Участники могут просматривать свои ответы после прохождения
                                    </label>
                            </div>
                        </div>
                        <input type="submit" className="btn btn-success" value="Создать тест" />
                    </form>
                </div>
            </div>
        );
    }
}