import React from "react";
import PassedTest from "./PassedTest";

export default class PassedTestsList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {
        const tests = this.props.tests !== undefined && this.props.tests.length > 0 ?
            this.props.tests.map(test => <PassedTest key={test.id} test={test}/>) : <p>Вы не прошли ни одного теста</p>;

        return (
            <div>
                {tests}
            </div>
        );
    }
}