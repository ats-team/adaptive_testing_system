import React from 'react';
import {Link} from "react-router-dom";

export default class PassedTest extends React.Component {
    constructor(props) {
        super(props);
        this.state = {}
    }

    render() {
        let percent = (this.props.test.received_score / this.props.test.max_score * 100).toFixed(1);
        return (
            <div className="card mb-2">
                <h5 className="card-header">{this.props.test.name}</h5>
                <div className="card-body row">
                    <div className="col-7">
                        <h5 className="card-title">Составитель: {this.props.test.creator_login} ({this.props.test.creator_fio})</h5>
                        <p className="card-text">{this.props.test.detailed_description}</p>
                        <p className="card-text">
                            <small className="text-muted">Продолжительность прохождения: {this.props.test.duration/60|0 } мин. {this.props.test.duration % 60 } сек.<br />
                                Дата прохождения: {new Date(this.props.test.passing_date * 1000).toLocaleString("ru")}</small>
                        </p>
                        <Link to={`/passed_tests/${this.props.test.id}`}><button type="button" className="btn btn-primary">Просмотреть результаты</button></Link>
                    </div>
                    <div className="col-5">
                        <p className="card-text">Результат: {this.props.test.received_score} из {this.props.test.max_score}</p>
                        <div className="progress">
                            <div className="progress-bar" role="progressbar" style={{width: `${percent}%`}} aria-valuenow={percent}
                                 aria-valuemin="0" aria-valuemax="100">{percent}%
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}