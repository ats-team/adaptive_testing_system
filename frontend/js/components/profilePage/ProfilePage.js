import React from 'react';
import ProfileForm from "./ProfileForm";

export default class ProfilePage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {}
    }

    render() {
        return (
            <div>
                <div className="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                    <h1 className="h2">Профиль</h1>
                </div>

                <ProfileForm/>
            </div>
        );
    }
}