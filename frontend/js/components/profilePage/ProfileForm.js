import React from 'react';
import Axios from 'axios';

import {common_request_errors_handle, getURlSearchParamsFromObject} from "../../helper_functions";

const errors_map = {
    'numericValue.fio': 'Имя не может содержать числа',
    'bad.email': 'Не правильный формат email',
    'short.password': 'Пароль должен содержать не менее 8 символов',
    'isBeingUsed.login': 'Логин используется другим пользователем',
    'isBeingUsed.email': 'Адрес электронной почты используется другим пользователем',
    'notEqual.passwords': 'Пароли должны совпадать',
};

export default class ProfileForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: {
                id: '',
                login: '',
                fio: '',
                email: '',
                password: '',
                password_repeat: ''
            },
            is_valid: true,
            error_code: '',
            is_successfully_sent: false
        };

        this.handleInputChange = this.handleInputChange.bind(this);
        this.reloadProfileData = this.reloadProfileData.bind(this);
        this.updateUserData = this.updateUserData.bind(this);
    }

    componentDidMount() {
        this.reloadProfileData();
    }

    handleInputChange(event) {
        let target = event.target;
        this.setState(prevState => ({
            data: Object.assign(prevState.data, {[target.name]: target.value})
        }), this.validateData);
    }

    reloadProfileData() {
        Axios({
            method: 'get',
            url: '/internal_api/profile',
            dataType: 'json'
        }).then(response => this.setState({
            data: Object.assign(response.data, {password: '', password_repeat: ''})
        })).catch(common_request_errors_handle);
    }

    updateUserData() {
        if (this.state.is_valid) {
            Axios({
                method: 'post',
                url: `/internal_api/profile/${this.state.data.id}`,
                data: getURlSearchParamsFromObject(this.state.data)
            }).then(response => this.setState({is_successfully_sent: true}))
                .catch(error => error.response ? (error.response.status === 400 ? this.setState(prevState => ({
                    is_valid: false,
                    error_code: error.response.headers['error_code'],
                    is_successfully_sent: false
                })) : console.log(error.response.status)) : console.error(error.message));
        }
    }

    validateData() {
        let data = this.state.data;
        let is_valid = true;
        let error_code = '';
        if (!data.email.match(/^.+@.+\..+$/)) {
            is_valid = false;
            error_code = 'bad.email';
        } else if (data.fio.match(/.*\d.*/)) {
            is_valid = false;
            error_code = 'numericValue.fio';
        } else if (data.password !== '' && data.password.length < 8) {
            is_valid = false;
            error_code = 'short.password';
        } else if (data.password !== data.password_repeat) {
            is_valid = false;
            error_code = 'notEqual.passwords';
        }
        this.setState(prevState => ({
            is_valid: is_valid,
            error_code: error_code,
            is_successfully_sent: false
        }));
    }

    render() {
        return (
            <div className = "col-md-8">
                <label htmlFor = "form-login">Логин</label>
                <div className="input-group mb-2">
                    <div className="input-group-prepend">
                        <span className="input-group-text" id="inputGroupPrepend2">@</span>
                    </div>
                    <input type="text" className="form-control" id="form-login" aria-describedby="inputGroupPrepend2"
                        value={this.state.data.login} onChange={this.handleInputChange} name='login' required/>
                </div>
                <div className="form-group">
                    <label htmlFor="form-email">Email</label>
                    <input type="email" className="form-control" id="form-email" aria-describedby="emailHelp"
                           value={this.state.data.email} onChange={this.handleInputChange} name='email' required/>
                </div>
                <div className="form-group">
                    <label htmlFor="form-fio">ФИО</label>
                    <input type="text" className="form-control" id="form-fio"
                           value={this.state.data.fio} onChange={this.handleInputChange} name='fio'/>
                </div>
                <div className="form-group">
                    <label htmlFor="form-password">Пароль</label>
                    <input type="password" className="form-control" id="form-password"
                           placeholder="Введите новый пароль" onChange={this.handleInputChange} name='password'/>
                </div>
                <div className="form-group">
                    <label htmlFor="form-repeat-password">Повторение пароля</label>
                    <input type="password" className="form-control" id="form-repeat-password"
                           placeholder="Повторите пароль" onChange={this.handleInputChange} name='password_repeat'/>
                        <small id="emailHelp" className="form-text text-muted">Если вы не хотите менять пароль, оставьте 2
                            последних поля пустыми.
                        </small>
                </div>
                {!this.state.is_valid ?
                    <div className="alert alert-danger" role="alert">
                        {errors_map[this.state.error_code]}
                    </div>
                : null}
                {this.state.is_successfully_sent ?
                    <div className="alert alert-success" role="alert">
                        Данные успешно обновлены
                    </div>
                : null}
                <button onClick={this.updateUserData} className="btn btn-success">Сохранить изменения</button>
            </div>
        );
    }
}