import React from 'react';
import Axios from 'axios';

import {common_request_errors_handle} from '../helper_functions';

export default class TestDescriptionPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            test: {}
        };

        this.startTest = this.startTest.bind(this);
    }

    componentDidMount() {
        let this_ = this;
        let testId = this.props.match.params.testId;
        Axios({
            method: 'get',
            url: `/internal_api/tests/${testId}`,
            responseType: 'json'
        }).then(function (response) {
            this_.setState({test: response.data});
        }).catch(common_request_errors_handle);
    }

    startTest() {
        let testId = this.state.test.test_id;
        let this_ = this;
        Axios({
            method: 'post',
            url: `/internal_api/test/${testId}/start`,
            responseType: 'json'
        }).then(function (response) {
            let next_question_id = response.data.next_question_id;
            this_.props.history.push(`/tests/${testId}/question/${next_question_id}`);
        }).catch(common_request_errors_handle);
    }

    render() {
        return(
            <div className="jumbotron">
                <h1 className="display-4">{this.state.test.name}</h1>
                <p className="lead">Составитель: {this.state.test.creator_login} ({this.state.test.creator_fio}) <br/>
                    {this.state.test.detailed_description}
                </p>
                <hr className="my-4"/>
                <p>Количество вопросов в тесте: {this.state.test.question_count}<br/>
                    Максимальное время прохождения: {this.state.test.max_duration} минут</p>
                <a onClick={this.startTest} className="btn btn-primary btn-lg" role="button">Начать прохождение</a>
            </div>
        );
    }
}