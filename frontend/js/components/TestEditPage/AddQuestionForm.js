import React from "react";
import {common_request_errors_handle, getURlSearchParamsFromObject} from "../../helper_functions";
import Axios from "axios/index";

export default class TestCreationPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name: '',
        };
        this.addQuestion = this.addQuestion.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
    }

    addQuestion(event) {
        event.preventDefault();
        let this_ = this;
        Axios({
            method: 'post',
            url: `/internal_api/questions/create`,
            data: getURlSearchParamsFromObject({
                testId: this_.props.wrapper.state.testId,
                name: this_.state.name,
            }),
            responseType: 'json'
        }).then(function (response) {
            $('#exampleModal').modal('toggle');
            this_.props.wrapper.refreshData();
        }).catch(common_request_errors_handle);
    }

    handleInputChange(event) {
        const target = event.target;
        this.setState({
            [target.name]: target.value
        })
    }

    render() {
        return (
            <form onSubmit={this.addQuestion}>
                <div className="form-group">
                    <label htmlFor="form-test-name">Название вопроса</label>
                    <input name="name" value={this.state.name} onChange={this.handleInputChange} type="text" className="form-control" id="form-test-name" required />
                </div>
                <button className="btn btn-primary" role="button">Создать вопрос</button>
            </form>
        );
    }
}