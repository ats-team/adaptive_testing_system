import React from 'react';
import {Link} from "react-router-dom";

export default class Test extends React.Component {
    constructor(props) {
        super(props);
        this.state = {}
    }

    render() {
        return (
            <li className="list-group-item list-group-item-action d-flex justify-content-between align-items-center">
                <Link to={`/question_creation_data/${this.props.question.question_id}`}>{this.props.question.question_name}</Link>
                <span>
                    <span className="badge badge-primary badge-pill">{this.props.question.level_count}</span>
                    <span className="fa fa-remove" aria-hidden="true"
                                              style={{color: "#cc0000", fontSize: "1.5em", cursor: "pointer"}}/>
                </span>
            </li>
        );
    }
}