import React from 'react';
import Axios from 'axios';

import {common_request_errors_handle, getURlSearchParamsFromObject} from '../../helper_functions';
import QuestionList from "./QuestionList";
import SimpleModal from "../SimpleModal";
import AddQuestionForm from "./AddQuestionForm";
import MakeAvailableForm from "./MakeAvailableForm";

export default class TestCreationPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            testId: '',
            name: '',
            shortDescription: '',
            detailedDescription: '',
            maxDuration: '',
            isPrivate: '',
            isQuestionsDetailsAvailable: '',
            questions: '',
            modalText: '',
            modalTitle: '',
            modalShowFooter: true,
            canMakeAvailable: false
        };
        this.handleInputChange = this.handleInputChange.bind(this);
        this.editTest = this.editTest.bind(this);
        this.openCreateQuestionModal = this.openCreateQuestionModal.bind(this);
        this.refreshData = this.refreshData.bind(this);
        this.openMakeAvailableModal = this.openMakeAvailableModal.bind(this);
    }

    refreshData() {
        let this_ = this;
        let testId = this.props.match.params.testId;
        Axios({
            method: 'get',
            url: `/internal_api/tests/${testId}/edit`,
            responseType: 'json'
        }).then(function (response) {
            this_.setState({
                testId: response.data.test_id,
                name: response.data.test_name,
                shortDescription: response.data.short_description,
                detailedDescription: response.data.detailed_description,
                maxDuration: response.data.max_duration,
                isPrivate: response.data.is_private ? "isPrivate" : "",
                isQuestionsDetailsAvailable: response.data.is_questions_details_available ? "isQuestionsDetailsAvailable" : "",
                questions: response.data.questions,
                canMakeAvailable: response.data.questions.length > 0 && response.data.questions.every((q) => q.level_count > 0)
            });
        }).catch(common_request_errors_handle);
    }

    componentDidMount() {
        this.refreshData();
    }

    handleInputChange(event) {
        const target = event.target;
        let value = '';
        if (target.type === 'checkbox') {
            if (target.checked) {
                value = target.value;
            } else {
                value = '';
            }
        } else {
            value = target.value;
        }
        this.setState({
            [target.name]: value
        })
    }

    editTest(event) {
        event.preventDefault();
        let this_ = this;
        Axios({
            method: 'post',
            url: `/internal_api/tests/${this_.state.testId}/edit`,
            data: getURlSearchParamsFromObject({
                name: this_.state.name,
                shortDescription: this_.state.shortDescription,
                detailedDescription: this_.state.detailedDescription,
                maxDuration: this_.state.maxDuration,
                isPrivate: this_.state.isPrivate,
                isQuestionsDetailsAvailable: this_.state.isQuestionsDetailsAvailable
            }),
            responseType: 'json'
        }).then(function (response) {
            this_.setState({modalTitle: "Изменения успешно внесены!",
                modalText: "Для продолжения работы с тестом нажмите кнопку Ок.",
                modalShowFooter: true});
            $('#exampleModal').modal('toggle')
        }).catch(common_request_errors_handle);
    };

    openCreateQuestionModal(event) {
        event.preventDefault();
        let this_ = this;
        this_.setState({modalTitle: "Создание вопроса",
            modalText: <AddQuestionForm wrapper={this} />,
            modalShowFooter: false});
        $('#exampleModal').modal('toggle')
    }

    openMakeAvailableModal(event) {
        event.preventDefault();
        let this_ = this;
        this_.setState({modalTitle: "Публикация теста",
            modalText: <MakeAvailableForm wrapper={this} />,
            modalShowFooter: false});
        $('#exampleModal').modal('toggle')
    }

    render() {
        return (
            <div className="mb-2">
                <div
                    className="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                    <h1 className="h2">Редактирование теста</h1>
                </div>
                <div className="col-md-8">
                    <form onSubmit={this.editTest}>
                        <div className="form-group">
                            <label htmlFor="form-test-name">Название теста</label>
                            <input name="name" value={this.state.name} onChange={this.handleInputChange} type="text" className="form-control" id="form-test-name" required />
                        </div>
                        <div className="form-group">
                            <label htmlFor="form-test-short-description">Краткое описание</label>
                            <input name="shortDescription" value={this.state.shortDescription} onChange={this.handleInputChange} type="text" className="form-control" id="form-test-short-description" />
                        </div>
                        <div className="form-group">
                            <label htmlFor="form-test-detailed-description">Подробное описание</label>
                            <textarea name="detailedDescription" value={this.state.detailedDescription} onChange={this.handleInputChange} className="form-control" id="form-test-detailed-description" rows="3" />
                        </div>
                        <div className="form-group">
                            <label htmlFor="form-test-duration">Продолжительность (в минутах)</label>
                            <input name="maxDuration" value={this.state.maxDuration} onChange={this.handleInputChange} type="number" className="form-control" id="form-test-duration" min="1" step="1" />
                        </div>
                        <div className="form-group">
                            <div className="form-check">
                                <input checked={this.state.isPrivate === "isPrivate"} name="isPrivate" onChange={this.handleInputChange} className="form-check-input" type="checkbox" value="isPrivate"
                                       id="form-test-is-private" />
                                <label className="form-check-label" htmlFor="form-test-is-private">
                                    Приватный тест
                                </label>
                            </div>
                        </div>
                        <div className="form-group">
                            <div className="form-check">
                                <input checked={this.state.isQuestionsDetailsAvailable === "isQuestionsDetailsAvailable"} name="isQuestionsDetailsAvailable" onChange={this.handleInputChange} className="form-check-input" type="checkbox" value="isQuestionsDetailsAvailable"
                                       id="form-test-is-questions-available" />
                                <label className="form-check-label" htmlFor="form-test-is-questions-available">
                                    Участники могут просматривать свои ответы после прохождения
                                </label>
                            </div>
                        </div>
                        <input type="submit" className="btn btn-success mr-2" value="Внести изменения" />
                        <button className="btn btn-primary" role="button" onClick={this.openMakeAvailableModal} disabled={!this.state.canMakeAvailable}>Опубликовать тест</button>
                        <small id="publish-help" className="form-text text-muted mb-2">Обратите внимание, что после
                            публикации
                            тест станет доступным для прохождения, при этом вы потеряете возможность вносить какие-либо
                            изменения (редактирование названия, описания, вопросов, уровней и т.п.)<br/>
                            Для публикации вы должны:<br/>
                            1) Создать хотя бы 1 вопрос;<br/>
                            2) Заполнить все вопросы хотя бы одним уровнем сложности.
                        </small>
                    </form>
                    <div className="mb-2">
                        <h1 className="h2">Редактирование вопросов <span onClick={this.openCreateQuestionModal} className="fa fa-plus"
                                                                                     aria-hidden="true"
                                                                                     style={{color: "#009900", cursor: "pointer"}}
                                                                                     data-toggle="modal"
                                                                                     data-target="#addAndEditElectiveModal" />
                        </h1>
                        <small id="level-help" className="form-text text-muted mb-2">В синем кружочке указано количество
                            созданных вами уровней сложности
                        </small>
                    </div>
                    <QuestionList questions={this.state.questions} />

                    <SimpleModal title={this.state.modalTitle}
                                 text={this.state.modalText}
                                 showFooter={this.state.modalShowFooter}
                                 wrapper={this} />
                </div>
            </div>
        );
    }
}