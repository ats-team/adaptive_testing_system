import React from "react";
import {common_request_errors_handle} from "../../helper_functions";
import Axios from "axios/index";

export default class TestCreationPage extends React.Component {
    constructor(props) {
        super(props);
        this.makeAvailable = this.makeAvailable.bind(this);
    }

    makeAvailable(event) {
        event.preventDefault();
        let this_ = this;
        Axios({
            method: 'post',
            url: `/internal_api/tests/${this_.props.wrapper.state.testId}/make_available`,
            responseType: 'json'
        }).then(function (response) {
            $('#exampleModal').modal('toggle');
            this_.props.wrapper.props.history.push(`/internal_api/tests/my`);
        }).catch(common_request_errors_handle);
    }

    render() {
        return (
            <div>
                <p>Вы уверены, что хотите опубликовать тест?</p>
                <button onClick={this.makeAvailable} className="btn btn-primary" role="button">Да!</button>
            </div>
        );
    }
}