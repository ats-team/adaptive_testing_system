import React from "react";

import Question from "./Question";

export default class AllTestsList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {
        const questions = this.props.questions !== undefined && this.props.questions.length > 0 ?
            this.props.questions.map(question => <Question question={question}/>) : <p>Вы ещё не создали ни одного вопроса</p>;

        return (
            <ul className="list-group mb-2">
                {questions}
            </ul>
        );
    }
}