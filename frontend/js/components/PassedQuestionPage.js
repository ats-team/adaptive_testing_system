import React from 'react';
import Axios from 'axios';
import {Link} from "react-router-dom";

import {common_request_errors_handle} from '../helper_functions';


export default class PassedQuestionPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: {}
        };
    }

    componentDidMount() {
        let this_ = this;
        let passedQuestionId = this.props.match.params.passedQuestionId;
        Axios({
            method: 'get',
            url: `/internal_api/passed_questions/${passedQuestionId}`,
            responseType: 'json'
        }).then(function (response) {
            this_.setState({data: response.data});
        }).catch(common_request_errors_handle);
    }

    render() {
        return(
            <div className="jumbotron" style={{paddingTop: "20px"}}>
                <h1 className="display-4">{this.state.data.question_name}</h1>
                <p className="lead">{this.state.data.text}</p>
                <p className="lead">Файл: <Link to={`/file/${this.state.data.file_path}`}>Прикреплённый файл</Link></p>
                <img src={this.state.data.picture_path}
                     className="img-thumbnail" />
                    <hr className="my-4"/>
                    <p className="lead">Ваш ответ: {this.state.data.user_answer}</p>
                    <p className="lead">Правильный ответ: {this.state.data.right_answer}</p>
            </div>
        );
    }
}