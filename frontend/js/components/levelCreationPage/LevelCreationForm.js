import React from 'react';

export default class LevelCreationForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: {
                text: undefined,
                right_answer: undefined,
                score: undefined,
                photo: undefined,
                file: undefined
            }
        };

        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleFileUpload = this.handleFileUpload.bind(this);
        this.handleSubmit= this.handleSubmit.bind(this);
    }

    handleInputChange(event) {
        let target = event.target;
        this.setState(prevState => ({
            data: {
                ...prevState.data,
                [target.name]: target.value
            }
        }));
    }

    handleFileUpload(event) {
        let target = event.target;
        this.setState(prevState => ({
            data: {
                ...prevState.data,
                [target.name]: target.files[0]
            }
        }));
    }

    handleSubmit() {
        let data = {};
        Object.entries(this.state.data).forEach(entry => (entry[1] !== undefined ? data[entry[0]] = entry[1] : null));
        this.props.sendData(data);
    }

    render() {
        return(
            <div className = "col-md-8">
                <div className = "mb-2" >
                    <div className = "form-group">
                        <label htmlFor = "form-level-text"> Текст вопроса </label>
                        <textarea className="form-control" id="form-level-text" rows="3"
                                  required value={this.state.data.text !== undefined ? this.state.data.text
                                                    : (this.props.level !== undefined ? this.props.level.text : '')}
                                  onChange={this.handleInputChange} name='text'/>
                    </div>
                        <div className="form-group">
                            <label htmlFor="form-level-right-answer">Правильный ответ</label>
                            <input type="text" className="form-control" id="form-level-right-answer"
                                   required value={this.state.data.right_answer !== undefined ? this.state.data.right_answer
                                                    : (this.props.level !== undefined ? this.props.level.right_answer : '')}
                                   onChange={this.handleInputChange} name='right_answer'/>
                        </div>
                        <div className="form-group">
                            <label htmlFor="form-level-duration">Балл за правильный ответ</label>
                            <input type="number" className="form-control" id="form-level-duration" min="1" step="1"
                                   required value={this.state.data.score !== undefined ? this.state.data.score
                                                    : (this.props.level !== undefined ? this.props.level.score : '')}
                                   onChange={this.handleInputChange} name='score'/>
                        </div>
                        <div className="form-group">
                            <label htmlFor="form-level-photo">Прикрепить фото</label>
                            <input type="file" className="form-control-file" id="form-level-photo" accept="image/*"
                                onChange={this.handleFileUpload} name='photo'/>
                            {this.props.level !== undefined ? <span>Текущий файл: <a href={this.props.level.photo_path}>{this.props.level.photo_name}</a></span> : null}
                        </div>
                        <div className="form-group">
                            <label htmlFor="form-level-file">Прикрепить файл</label>
                            <input type="file" className="form-control-file" id="form-level-file"
                                onChange={this.handleFileUpload} name='file'/>
                            {this.props.level !== undefined ? <span>Текущий файл: <a href={this.props.level.file_path}>{this.props.level.file_name}</a></span> : null}
                        </div>
                        <button onClick={this.handleSubmit} className="btn btn-success">Сохранить</button>
                </div>
            </div>
        );
    }
}