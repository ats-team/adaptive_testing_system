import React from 'react';
import Axios from 'axios';

import LevelCreationForm from "./LevelCreationForm";
import {common_request_errors_handle, prepareFormDataFromObject} from "../../helper_functions";

export default class LevelCreationPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};

        this.updateLevel = this.updateLevel.bind(this);
    }

    componentDidMount() {
        if (this.props.mode === 'edit') this.loadLevel();
    }

    loadLevel() {
        Axios({
            method: 'get',
            url: `/internal_api/levels_creation_data/${this.props.match.params.levelId}`,
            responseType: 'json'
        })
            .then(response => this.setState({level: response.data}))
            .catch(common_request_errors_handle);
    }

    updateLevel(data) {
        let requestUrl = this.props.mode === 'create' ? '/internal_api/levels/create'
            : `/internal_api/levels/update/${this.props.match.params.levelId}`;
        Axios({
            method: 'post',
            url: requestUrl,
            data: prepareFormDataFromObject(data)
        })
            .then(response => this.props.history.push(`/question_creation_data/${this.props.match.params.questionId}`))
            .catch(common_request_errors_handle);
    }

    render() {
        return (
            <div>
                <div
                    className="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                    <h1 className="h2">Вопрос {this.props.match.params.questionId} - создание уровня</h1>
                </div>
                <LevelCreationForm level={this.state.level} sendData={this.updateLevel}/>
            </div>
        );
    }
}