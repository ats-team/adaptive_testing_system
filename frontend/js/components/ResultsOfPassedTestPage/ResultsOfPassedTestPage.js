import React from 'react';
import Axios from 'axios';

import {common_request_errors_handle} from "../../helper_functions";
import PassedQuestionList from "./PassedQuestionList";

export default class ResultsOfPassedTestPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: {},
        };
        this.loadData = this.loadData.bind(this);
    }

    componentDidMount() {
        this.loadData();
    }

    loadData() {
        let this_ = this;
        let passedTestId = this.props.match.params.passedTestId;
        Axios({
            method: 'get',
            url: `/internal_api/passed_tests/${passedTestId}`,
            responseType: 'json'
        }).then(function (response) {
            this_.setState({data: response.data});
        }).catch(common_request_errors_handle);
    }

    render() {
        let percent = (this.state.data.received_score / this.state.data.max_score * 100).toFixed(1);

        return (
            <div className="jumbotron" style={{paddingTop: "20px", backgroundColor: "transparent"}}>
                <h1 className="display-4">{this.state.data.name}</h1>
                <div className="col-12">
                    <p className="card-text">Результат: {this.state.data.received_score} из {this.state.data.max_score}</p>
                    <div className="progress">
                        <div className="progress-bar" role="progressbar" style={{width: `${percent}%`}} aria-valuenow={percent}
                             aria-valuemin="0" aria-valuemax="100">{percent}%
                        </div>
                    </div>
                </div>
                <hr className="my-4"/>
                <p className="lead">Дата прохождения: {new Date(this.state.data.passing_date * 1000).toLocaleString("ru")}</p>
                <p className="lead">Продолжительность прохождения: {this.state.data.duration/60|0 } мин. {this.state.data.duration % 60 } сек.</p>
                <p className="lead">Составитель теста: {this.state.data.creator_login} ({this.state.data.creator_fio})</p>
                <p className="lead">{this.state.data.detailed_description}</p>
                <small id="level-help" className="form-text text-muted mb-2">В синем кружочке указано набранное вами
                    количество баллов за вопрос / максимальное число баллов за вопрос.
                </small>
                <PassedQuestionList passedQuestions={this.state.data.passed_questions_list} />
            </div>
        )
    }
}