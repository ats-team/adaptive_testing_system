import React from 'react';
import {Link} from "react-router-dom";

export default class PassedQuestion extends React.Component {
    constructor(props) {
        super(props);
        this.state = {}
    }

    render() {
        const color = this.props.passedQuestion.received_score === 0 ? 'danger'
            : this.props.passedQuestion.received_score === this.props.passedQuestion.max_score ? 'success'
                : 'warning';
        return (
            <Link to={`/passed_questions/${this.props.passedQuestion.passed_question_id}`}
               className={`list-group-item list-group-item-${color} list-group-item-action d-flex justify-content-between align-items-center`}>
                {this.props.passedQuestion.question_name}
                <span className="badge badge-primary badge-pill">{this.props.passedQuestion.received_score}/{this.props.passedQuestion.max_score}</span>
            </Link>
        );
    }
}