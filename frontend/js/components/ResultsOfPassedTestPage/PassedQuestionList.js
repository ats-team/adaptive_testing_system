import React from "react";
import PassedQuestion from "./PassedQuestion";


export default class PassedQuestionsList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {
        const passedQuestions = this.props.passedQuestions !== undefined && this.props.passedQuestions.length > 0 ?
            this.props.passedQuestions.map(passedQuestion => <PassedQuestion passedQuestion={passedQuestion}/>) : <p>Вы не прошли ни одного вопроса</p>;

        return (
            <div className="list-group mb-2">
                {passedQuestions}
            </div>
        );
    }
}