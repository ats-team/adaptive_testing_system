import React from 'react';
import Axios from 'axios';

import {common_request_errors_handle, getURlSearchParamsFromObject} from "../helper_functions";

export default class QuestionPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: {
                question_id: '',
                level_id: '',
                passed_test_id: '',
                test_name: '',
                picture_path: '',
                file_path: '',
                file_name: '',
                text: '',
                answered_questions_count: '',
                question_count: '',
                end_time: '',
            },
            answer: '',
            left: ''
        };

        this.handleAnswerChange = this.handleAnswerChange.bind(this);
        this.doAnswer = this.doAnswer.bind(this);
        this.tick = this.tick.bind(this);
    }

    componentDidMount() {
        this.timer = setInterval(this.tick, 1000);
        this.loadData();
    }

    componentWillUnmount() {
        clearInterval(this.timer);
    }

    loadData() {
        Axios({
            method: 'get',
            url: `/internal_api/questions/${this.props.match.params.questionId}`,
            responseType: 'json'
        })
            .then(response => this.setState({data: response.data}))
            .catch(common_request_errors_handle);
    }

    handleAnswerChange(event) {
        this.setState({answer: event.target.value});
    }

    tick() {
        this.setState({left: Math.round((this.state.data.end_time - Date.now()) / 1000)});
        if (this.state.left <= 0) {
            this.props.history.push(`/passed_tests/${this.state.data.passed_test_id}`);
        }
    }

    doAnswer(event) {
        Axios({
            method: 'post',
            url: `/internal_api/questions/${this.props.match.params.questionId}`,
            data: getURlSearchParamsFromObject({
                question_id: this.state.data.question_id,
                level_id: this.state.data.level_id,
                answer: this.state.answer
            }),
            responseType: 'json'
        })
            .then(response => this.state.data.answered_questions_count + 1 < this.state.data.question_count ?
                    this.props.history.push(`/tests/${this.props.match.params.testId}/question/${response.data.next_question_id}`)
                : this.props.history.push(`/passed_tests/${response.data.passed_test_id}`))
            .catch(common_request_errors_handle);
    }

    render() {
        return (
            <div className="jumbotron" style={{'paddingTop': '20px'}}>
                <h1 className="display-4">Вопрос {this.state.data.answered_questions_count} / {this.state.data.question_count}</h1>
                <p className="lead">Осталось: {this.state.left / 60|0} мин. {this.state.left % 60} сек.</p>
                <p className="lead">{this.state.data.text}</p>
                <p className="lead">Файл: <a href={this.state.data.file_path}>{this.state.data.file_name}</a></p>
                <img src={this.state.data.picture_path} className="img-thumbnail"/>
                    <hr className="my-4"/>
                    <div>
                        <div className="form-group">
                            <label htmlFor="form-answer">Ответ</label>
                            <input type="text" className="form-control" id="form-answer" name='answer'
                                   placeholder="Введите ваш ответ" value={this.state.answer} onChange={this.handleAnswerChange}/>
                        </div>
                        <button onClick={this.doAnswer} className="btn btn-primary">Ответить</button>
                    </div>
            </div>
        );
    }
}