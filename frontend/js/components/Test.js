import React from 'react';
import {Link} from "react-router-dom";

export default class Test extends React.Component {
    constructor(props) {
        super(props);
        this.state = {}
    }

    render() {
        return (
            <div className="card mb-2">
                <h5 className="card-header">{this.props.test.name}</h5>
                <div className="card-body">
                    <h5 className="card-title">Составитель: {this.props.test.creator_login} ({this.props.test.creator_fio})</h5>
                    <p className="card-text">{this.props.test.short_description}</p>
                    <p className="card-text">
                        <small className="text-muted">Максимальная продолжительность прохождения: {this.props.test.max_duration} минут</small>
                    </p>
                    <Link to={`/tests/${this.props.test.id}`} className="btn btn-primary">Пройти!</Link>
                </div>
            </div>
        );
    }
}