export function common_request_errors_handle(error) {
    if (error.response) {
        console.error(`Error: Response code is ${error.response.status}`);
    } else if (error.request) {
        console.error(`Error: Request was not sent\n${error.request}`);
    } else {
        console.error(`Unknown error: ${error.message}`);
    }
}

export function getURlSearchParamsFromObject(obj) {
    let params = new URLSearchParams();
    Object.entries(obj).forEach(entry => params.append(entry[0], entry[1]));
    return params;
}

export function prepareFormDataFromObject(obj) {
    let data = new FormData();
    Object.entries(obj).forEach(entry => data.append(entry[0], entry[1]));
    return data;
}