package ru.kpfu.itis.adaptivetest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.*;

/**
 * @author Bulat Giniyatullin
 * 13 Март 2018
 */

@Component
public class FileLogger {

    private Logger logger;

    @Autowired
    public FileLogger(Environment environment) throws IOException {
        logger = Logger.getLogger(this.getClass().getName());
        FileHandler fileHandler = new FileHandler(environment.getProperty("custom.logging.file"), true);
        fileHandler.setLevel(Level.ALL);
        fileHandler.setFormatter(new Formatter() {
            private final DateFormat dateformat
                    = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.SSS");

            @Override
            public String format(LogRecord record) {
                StringBuilder builder = new StringBuilder();
                builder.append(dateformat.format(new Date(record.getMillis()))).append("  ");
                builder.append(record.getLevel()).append(": ");
                builder.append(record.getMessage());
                builder.append("\n");
                return builder.toString();
            }
        });

        logger.addHandler(fileHandler);
    }

    public void log(Level level, String msg) {
        logger.log(level, msg);
    }

    public void info(String msg) {
        logger.info(msg);
    }

    public void warning(String msg) {
        logger.warning(msg);
    }

    public void config(String msg) {
        logger.config(msg);
    }

    public void severe(String msg) {
        logger.severe(msg);
    }
}
