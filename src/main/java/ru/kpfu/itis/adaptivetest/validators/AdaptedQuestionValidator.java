package ru.kpfu.itis.adaptivetest.validators;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import ru.kpfu.itis.adaptivetest.forms.AdaptedQuestionForm;
import ru.kpfu.itis.adaptivetest.models.Question;
import ru.kpfu.itis.adaptivetest.repositories.QuestionRepository;

import java.util.Optional;

@Component
public class AdaptedQuestionValidator implements Validator {
    private QuestionRepository questionRepository;

    @Autowired
    public void setQuestionRepository(QuestionRepository questionRepository) {
        this.questionRepository = questionRepository;
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return aClass.getName().equals(AdaptedQuestionForm.class.getName());
    }

    @Override
    public void validate(Object o, Errors errors) {
        AdaptedQuestionForm form = (AdaptedQuestionForm) o;
        Optional<Question> questionOptional = questionRepository.findById(form.getQuestion_id());
        if (!questionOptional.isPresent()) {
            errors.reject("not_found.question", "Вопроса не существует");
        }
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "text", "empty.text", "Не содержит текста");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "right_answer", "empty.right_answer", "Верный ответ не определен");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "score", "empty.score", "Балл не определен");
    }
}
