package ru.kpfu.itis.adaptivetest.validators;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import ru.kpfu.itis.adaptivetest.forms.QuestionCreationForm;

/**
 * @author Rustem Saitgareev
 * 11-602
 * 000
 */
public class QuestionCreationFormValidator implements Validator {
    @Override
    public boolean supports(Class<?> aClass) {
        return aClass.getName().equals(QuestionCreationForm.class.getName());
    }

    @Override
    public void validate(Object o, Errors errors) {
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name", "empty.name", "Пустое название");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "testId", "empty.testId", "Пустое поле идентификатора теста");
    }
}
