package ru.kpfu.itis.adaptivetest.validators;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import ru.kpfu.itis.adaptivetest.forms.TestCreationForm;

/**
 * @author Rustem Saitgareev
 * 11-602
 * 000
 */
@Component
public class TestCreationFormValidator implements Validator {
    @Override
    public boolean supports(Class<?> aClass) {
        return aClass.getName().equals(TestCreationForm.class.getName());
    }

    @Override
    public void validate(Object o, Errors errors) {
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name", "empty.name", "Пустое название");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "maxDuration", "empty.maxDiration", "Пустой поле продолжительности теста");
    }
}
