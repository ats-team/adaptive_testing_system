package ru.kpfu.itis.adaptivetest.validators;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import ru.kpfu.itis.adaptivetest.forms.UserForm;
import ru.kpfu.itis.adaptivetest.models.User;
import ru.kpfu.itis.adaptivetest.repositories.UserRepository;
import ru.kpfu.itis.adaptivetest.security.details.UserDetailsImpl;

import java.util.Optional;
import java.util.regex.Pattern;

/**
 * @author Bulat Giniyatullin
 * 03 Апрель 2018
 */

@Component
public class ProfileFormValidator implements Validator {
    private UserRepository userRepository;

    @Autowired
    public ProfileFormValidator(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return UserForm.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        UserForm userForm = (UserForm) o;
        if (userForm.getFio() != null && !userForm.getFio().equals("") && Pattern.matches(".*\\d.*", userForm.getFio())) {
            errors.reject("numericValue.fio", "Имя не может содержать числа");
        }
        if (userForm.getEmail() != null && !userForm.getEmail().equals("") && !Pattern.matches(".+@.+\\..+", userForm.getEmail())) {
            errors.reject("bad.email", "Не правильный формат email");
        }
        if (userForm.getPassword() != null && !userForm.getPassword().equals("") && userForm.getPassword().length() < 8) {
            errors.reject("short.password", "Пароль должен содержать не менее 8 символов");
        }

        User currentUser = ((UserDetailsImpl) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUser();
        Optional<User> userByLogin = userRepository.findOneByLogin(userForm.getLogin());
        if (userByLogin.isPresent() && !userByLogin.get().equals(currentUser)) {
            errors.reject("isBeingUsed.login", "Логин используется другим пользователем");
        }
        Optional<User> userByEmail = userRepository.findOneByEmail(userForm.getEmail());
        if (userByEmail.isPresent() && !userByEmail.get().equals(currentUser)) {
            errors.reject("isBeingUsed.email", "Адрес электронной почты используется другим пользователем");
        }
    }
}
