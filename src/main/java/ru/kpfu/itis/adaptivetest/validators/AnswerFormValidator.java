package ru.kpfu.itis.adaptivetest.validators;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import ru.kpfu.itis.adaptivetest.forms.AnswerForm;
import ru.kpfu.itis.adaptivetest.models.AdaptedQuestion;
import ru.kpfu.itis.adaptivetest.repositories.AdaptedQuestionRepository;

import java.util.Optional;
@Component
public class AnswerFormValidator implements Validator{
    private AdaptedQuestionRepository questionRepository;

    @Autowired
    public void setQuestionRepository(AdaptedQuestionRepository questionRepository) {
        this.questionRepository = questionRepository;
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return aClass.getName().equals(AnswerForm.class.getName());
    }

    @Override
    public void validate(Object o, Errors errors) {
        AnswerForm form = (AnswerForm) o;
        Optional<AdaptedQuestion> questionOptional = questionRepository.findById(form.getLevel_id());
        if (!questionOptional.isPresent()) {
            errors.reject("not_found.level", "Уровня не существует");
        }
    }
}
