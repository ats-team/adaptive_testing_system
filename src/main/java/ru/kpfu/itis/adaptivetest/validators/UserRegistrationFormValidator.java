package ru.kpfu.itis.adaptivetest.validators;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import ru.kpfu.itis.adaptivetest.forms.UserRegistrationForm;
import ru.kpfu.itis.adaptivetest.models.User;
import ru.kpfu.itis.adaptivetest.repositories.UserRepository;

import java.util.Optional;

/**
 * @author Rustem Saitgareev
 * 11-602
 * 000
 */
@Component
public class UserRegistrationFormValidator implements Validator {
    private final UserRepository usersRepository;

    @Autowired
    public UserRegistrationFormValidator(UserRepository usersRepository) {
        this.usersRepository = usersRepository;
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return aClass.getName().equals(UserRegistrationForm.class.getName());
    }

    // Валидируем объект target
    @Override
    public void validate(Object target, Errors errors) {
        UserRegistrationForm form = (UserRegistrationForm) target;
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "login", "empty.login", "Пустой логин");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "email", "empty.email", "Пустой email");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "password", "empty.password", "Пустой пароль");
        if (!form.getEmail().contains("@")) {
            errors.reject("bad.email", "Невалидный e-mail");
        }
        if (!form.getPassword().equals(form.getRepeatPassword())) {
            errors.reject("bad.repeat_password", "Пароли должны совпадать");
        }
        if (form.getPassword().length() < 6) {
            errors.reject("bad.password", "Длина пароля не может быть меньше шести символов");
        }

        Optional<User> existedUserByLogin = usersRepository.findOneByLogin(form.getLogin());
        if (existedUserByLogin.isPresent()) {
            errors.reject("bad.login", "Логин занят");
        }
        Optional<User> existedUserByEmail = usersRepository.findOneByEmail(form.getEmail());
        if (existedUserByEmail.isPresent()) {
            errors.reject("bad.email", "E-mail адрес занят");
        }
    }
}
