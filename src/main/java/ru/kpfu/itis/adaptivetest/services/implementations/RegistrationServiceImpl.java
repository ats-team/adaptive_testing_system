package ru.kpfu.itis.adaptivetest.services.implementations;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.kpfu.itis.adaptivetest.forms.UserRegistrationForm;
import ru.kpfu.itis.adaptivetest.models.User;
import ru.kpfu.itis.adaptivetest.repositories.UserRepository;
import ru.kpfu.itis.adaptivetest.security.role.Role;
import ru.kpfu.itis.adaptivetest.security.state.State;
import ru.kpfu.itis.adaptivetest.services.interfaces.RegistrationServiceInterface;

/**
 * @author Rustem Saitgareev
 * 11-602
 * 000
 */
@Service
public class RegistrationServiceImpl implements RegistrationServiceInterface {
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

    @Autowired
    public RegistrationServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public void register(UserRegistrationForm user) {
        User newUser = User.builder()
                .login(user.getLogin())
                .email(user.getEmail())
                .fio(user.getFio())
                .password(passwordEncoder.encode(user.getPassword()))
                .role(Role.USER)
                .state(State.CONFIRMED)
                .build();
        userRepository.save(newUser);
    }
}
