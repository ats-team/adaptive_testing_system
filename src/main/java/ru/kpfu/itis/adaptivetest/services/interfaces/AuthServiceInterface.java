package ru.kpfu.itis.adaptivetest.services.interfaces;

import org.springframework.security.core.Authentication;
import ru.kpfu.itis.adaptivetest.models.User;

/**
 * @author Rustem Saitgareev
 * 11-602
 * 000
 */
public interface AuthServiceInterface {
    User getUserByAuthentication(Authentication authentication);
}
