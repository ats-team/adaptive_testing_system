package ru.kpfu.itis.adaptivetest.services.implementations;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.kpfu.itis.adaptivetest.models.PassedQuestion;
import ru.kpfu.itis.adaptivetest.repositories.PassedQuestionRepository;
import ru.kpfu.itis.adaptivetest.services.interfaces.PassedQuestionServiceInterface;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/**
 * @author Bulat Giniyatullin
 * 24 April 2018
 */

@Service
public class PassedQuestionServiceImpl implements PassedQuestionServiceInterface {
    private final PassedQuestionRepository passedQuestionRepository;

    @Autowired
    public PassedQuestionServiceImpl(PassedQuestionRepository passedQuestionRepository) {
        this.passedQuestionRepository = passedQuestionRepository;
    }

    @Override
    public Map<String, Object> getPassedQuestionById(Long questionId) {
        Optional<PassedQuestion> optionalPassedQuestion =
                passedQuestionRepository.findById(questionId);
        Map<String, Object> passedQuestionData = new HashMap<>();
        if (optionalPassedQuestion.isPresent()) {
            PassedQuestion passedQuestion = optionalPassedQuestion.get();
            passedQuestionData.put("passed_question_id", passedQuestion.getId());
            passedQuestionData.put("test_name", passedQuestion
                    .getPassedTest()
                        .getTest()
                            .getName());
            passedQuestionData.put("question_name", passedQuestion
                    .getAdaptedQuestion()
                    .getQuestion()
                    .getName());
            passedQuestionData.put("picture_path", passedQuestion
                    .getAdaptedQuestion()
                        .getPhotoPath());
            passedQuestionData.put("file_path", passedQuestion
                    .getAdaptedQuestion()
                        .getFilePath());
            passedQuestionData.put("text", passedQuestion
                    .getAdaptedQuestion()
                        .getText());
            passedQuestionData.put("user_answer", passedQuestion.getAnswer());
            passedQuestionData.put("right_answer", passedQuestion
                    .getAdaptedQuestion()
                        .getRightAnswer());
        }
        return passedQuestionData;
    }
}
