package ru.kpfu.itis.adaptivetest.services.interfaces;

import org.springframework.security.core.Authentication;
import ru.kpfu.itis.adaptivetest.exceptions.BadQuestionsException;
import ru.kpfu.itis.adaptivetest.forms.TestCreationForm;
import ru.kpfu.itis.adaptivetest.models.Test;
import ru.kpfu.itis.adaptivetest.models.User;

import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * @author Rustem Saitgareev
 * 11-602
 * 000
 */
public interface TestServiceInterface {
    Map<String, Object> getTestInfo(Long id);

    Map<String, Object> getTestInfoForEditPage(Long id);

    List<Map<String, Object>> getAllPublicTests(String nameFilteringValue,
                                                String loginFilteringValue,
                                                String fioFilteringValue,
                                                Integer pageNumber,
                                                Integer pageSize);
    List<Map<String, Object>> getAvailableTests(Authentication authentication,
                                                String nameFilteringValue,
                                                String fioFilteringValue,
                                                String loginFilteringValue,
                                                Integer pageNumber,
                                                Integer pageSize);

    Map<String, Object> create(TestCreationForm form, User user);

    void edit(Test test, TestCreationForm form);

    Optional<Test> getTestById(Long id);

    List<Map<String,Object>> getMyTests(Authentication authentication, String nameFilteringValue, Integer pageNumber, Integer pageSize);

    void makeAvailable(Test test) throws BadQuestionsException;

}
