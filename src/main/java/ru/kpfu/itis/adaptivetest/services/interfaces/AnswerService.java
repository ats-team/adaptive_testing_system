package ru.kpfu.itis.adaptivetest.services.interfaces;

import ru.kpfu.itis.adaptivetest.exceptions.NotFoundException;
import ru.kpfu.itis.adaptivetest.forms.AnswerForm;
import ru.kpfu.itis.adaptivetest.models.PassedTest;
import ru.kpfu.itis.adaptivetest.models.Question;
import ru.kpfu.itis.adaptivetest.models.User;

import java.util.Date;
import java.util.Map;

public interface AnswerService {
    void processAnswer(AnswerForm form, User user, PassedTest passedTest) throws Exception;
    Map<String, Object> getCurrentQuestion(Long question_id, PassedTest passedTest) throws Exception;
    Question getNextQuestion(PassedTest passedTest, Long question_id);
}
