package ru.kpfu.itis.adaptivetest.services.interfaces;

import java.util.Map;

/**
 * @author Bulat Giniyatullin
 * 18 April 2018
 */

public interface ResultsOfTestCreatedByMeServiceInterface {
    Map<String, Object> getTestResults(Long testId);
}
