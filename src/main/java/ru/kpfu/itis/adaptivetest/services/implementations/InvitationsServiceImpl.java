package ru.kpfu.itis.adaptivetest.services.implementations;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.kpfu.itis.adaptivetest.exceptions.NotFoundException;
import ru.kpfu.itis.adaptivetest.models.AvailableTest;
import ru.kpfu.itis.adaptivetest.models.Test;
import ru.kpfu.itis.adaptivetest.models.User;
import ru.kpfu.itis.adaptivetest.repositories.AvailableTestRepository;
import ru.kpfu.itis.adaptivetest.repositories.UserRepository;
import ru.kpfu.itis.adaptivetest.services.interfaces.InvitationsServiceInterface;

import java.util.Optional;

/**
 * @author Bulat Giniyatullin
 * 26 April 2018
 */

@Service
public class InvitationsServiceImpl implements InvitationsServiceInterface {
    private final UserRepository userRepository;
    private final AvailableTestRepository availableTestRepository;

    @Autowired
    public InvitationsServiceImpl(UserRepository userRepository, AvailableTestRepository availableTestRepository) {
        this.userRepository = userRepository;
        this.availableTestRepository = availableTestRepository;
    }

    @Override
    @Transactional
    public void invite(String login, Long testId) throws NotFoundException {
        Optional<User> optionalUser = userRepository.findOneByLogin(login);
        if (optionalUser.isPresent()) {
            User user = optionalUser.get();
            Optional<AvailableTest> optionalAvailableTest =
                    availableTestRepository.findOneByTestIdAndUser(testId, user);
            if (! optionalAvailableTest.isPresent()) {
                Test test = Test.builder().id(testId).build();
                AvailableTest availableTest = AvailableTest.builder()
                        .user(user)
                        .test(test)
                        .build();
                availableTestRepository.save(availableTest);
            }
        } else {
            throw new NotFoundException(User.class);
        }
    }

    @Override
    @Transactional
    public void uninvite(String login, Long testId) {
        availableTestRepository.deleteAvailableTestByTestIdAndUserLogin(testId, login);
    }
}
