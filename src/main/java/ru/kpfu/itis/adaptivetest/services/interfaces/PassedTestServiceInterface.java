package ru.kpfu.itis.adaptivetest.services.interfaces;

import ru.kpfu.itis.adaptivetest.exceptions.NotAccessException;
import ru.kpfu.itis.adaptivetest.exceptions.NotFoundException;

import java.util.List;
import java.util.Map;

/**
 * @author Rustem Saitgareev
 * 11-602
 * 000
 */
public interface PassedTestServiceInterface {
    Map<String, Object> getResultById(Long id) throws NotFoundException, NotAccessException;
    List<Map<String,Object>> getPassedTestList(String nameFilteringValue, String loginFilteringValue, String fioFilteringValue,
                                               Integer pageNumber, Integer pageSize);
}
