package ru.kpfu.itis.adaptivetest.services.implementations;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.kpfu.itis.adaptivetest.models.PassedTest;
import ru.kpfu.itis.adaptivetest.models.Test;
import ru.kpfu.itis.adaptivetest.repositories.TestRepository;
import ru.kpfu.itis.adaptivetest.services.interfaces.ResultsOfTestCreatedByMeServiceInterface;

import java.util.*;

/**
 * @author Bulat Giniyatullin
 * 18 April 2018
 */

@Service
public class ResultsOfTestCreatedByMeServiceImpl implements ResultsOfTestCreatedByMeServiceInterface {
    private final TestRepository testRepository;

    @Autowired
    public ResultsOfTestCreatedByMeServiceImpl(TestRepository testRepository) {
        this.testRepository = testRepository;
    }

    @Override
    public Map<String, Object> getTestResults(Long testId) {
        Map<String, Object> testData = new HashMap<>();
        Optional<Test> optionalTest = testRepository.findById(testId);
        if (optionalTest.isPresent()) {
            Test test = optionalTest.get();
            testData.put("test_id", test.getId());
            testData.put("name", test.getName());
            testData.put("detailed_description", test.getDetailedDescription());
            testData.put("max_duration", test.getMaxDuration());
            testData.put("date_created", test.getTimeStampCreated());
            if (test.getQuestions() == null) {
                testData.put("question_count", 0);
            } else {
                testData.put("question_count", test.getQuestions().size());
            }
            List<PassedTest> passedTestSet = test.getPassedTests();
            testData.put("count_of_participants", passedTestSet.size());
            List<Map<String, Object>> passedTestsList = new LinkedList<>();
            for (PassedTest passedTest: passedTestSet) {
                Map<String, Object> passedTestMap = new HashMap<>();
                passedTestMap.put("passed_test_id", passedTest.getId());
                passedTestMap.put("login", passedTest.getUser().getLogin());
                passedTestMap.put("fio", passedTest.getUser().getFio());
                passedTestMap.put("score", passedTest.getScore());
                passedTestMap.put("complexity", passedTest.getComplexity());
                passedTestMap.put("duration", passedTest.getDuration());
                passedTestMap.put("date", passedTest.getTimeStampPassed());
                passedTestsList.add(passedTestMap);
            }
            testData.put("participants", passedTestsList);
        }
        return testData;
    }
}
