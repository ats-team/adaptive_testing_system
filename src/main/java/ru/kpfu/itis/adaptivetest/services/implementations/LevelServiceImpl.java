package ru.kpfu.itis.adaptivetest.services.implementations;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.kpfu.itis.adaptivetest.exceptions.NotFoundException;
import ru.kpfu.itis.adaptivetest.forms.AdaptedQuestionForm;
import ru.kpfu.itis.adaptivetest.models.AdaptedQuestion;
import ru.kpfu.itis.adaptivetest.models.Question;
import ru.kpfu.itis.adaptivetest.models.User;
import ru.kpfu.itis.adaptivetest.repositories.AdaptedQuestionRepository;
import ru.kpfu.itis.adaptivetest.repositories.QuestionRepository;
import ru.kpfu.itis.adaptivetest.services.interfaces.LevelServiceInterface;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * @author Bulat Giniyatullin
 * 12 April 2018
 */

@Service
public class LevelServiceImpl implements LevelServiceInterface {
    private final AdaptedQuestionRepository adaptedQuestionRepository;
    private  QuestionRepository questionRepository;

    @Autowired
    public LevelServiceImpl(AdaptedQuestionRepository adaptedQuestionRepository) {
        this.adaptedQuestionRepository = adaptedQuestionRepository;
    }

    @Autowired
    public void setQuestionRepository(QuestionRepository questionRepository) {
        this.questionRepository = questionRepository;
    }

    @Override
    @Transactional
    public void deleteLevelById(Long id) {
        adaptedQuestionRepository.deleteAdaptedQuestionById(id);
    }

    @Override
    public void createLevel(AdaptedQuestionForm form) throws NotFoundException{
        Optional<Question> question = questionRepository.findById(form.getQuestion_id());
        if (question.isPresent()) {
            int level = adaptedQuestionRepository.countAdaptedQuestionByQuestionId(question.get().getId()) + 1;
            AdaptedQuestion adaptedQuestion = AdaptedQuestion.builder()
                    .question(question.get())
                    .filePath(form.getFile())
                    .photoPath(form.getPhoto())
                    .rightAnswer(form.getRight_answer())
                    .text(form.getText())
                    .score(form.getScore())
                    .level(level)
                    .build();

            adaptedQuestionRepository.save(adaptedQuestion);
        } else {
            throw new NotFoundException(Question.class);
        }
    }

    @Override
    public Map<String, Object> getLevelInfo(Long id) throws NotFoundException{
        Optional<AdaptedQuestion> adaptedQuestionOptional = adaptedQuestionRepository.findById(id);
        if (adaptedQuestionOptional.isPresent()) {
            AdaptedQuestion adaptedQuestion = adaptedQuestionOptional.get();
            Map<String, Object> map = new HashMap<>();
            map.put("level_id", adaptedQuestion.getLevel());
            map.put("question_id", adaptedQuestion.getQuestion().getId());
            map.put("question_name", adaptedQuestion.getQuestion().getName());
            map.put("text", adaptedQuestion.getText());
            map.put("photo_path", adaptedQuestion.getPhotoPath());
            map.put("file_path", adaptedQuestion.getFilePath());
            map.put("right_answer", adaptedQuestion.getRightAnswer());
            map.put("score", adaptedQuestion.getScore());
            return map;
        }
        else {
            throw new NotFoundException(AdaptedQuestion.class);
        }

    }

    @Override
    public User getAuthorOfTestByLevelId(Long id) throws NotFoundException {
        System.out.println(id);
        Optional<AdaptedQuestion> adaptedQuestionOptional = adaptedQuestionRepository.findById(id);
        if (adaptedQuestionOptional.isPresent()){
            return adaptedQuestionOptional.get().getQuestion().getTest().getAuthor();
        } else {
            throw new NotFoundException(AdaptedQuestion.class);
        }
    }

    private AdaptedQuestion getAdaptedQuestion(long id, int level) {
        List<AdaptedQuestion> questions = adaptedQuestionRepository.findAllByQuestionIdAndLevel(id, level);
        if (questions.size() > 0) {
            return questions.get(0);
        }
        return new AdaptedQuestion();
    }
}
