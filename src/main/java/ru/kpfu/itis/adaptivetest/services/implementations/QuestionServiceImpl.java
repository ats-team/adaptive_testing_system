package ru.kpfu.itis.adaptivetest.services.implementations;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.kpfu.itis.adaptivetest.exceptions.NotFoundException;
import ru.kpfu.itis.adaptivetest.forms.QuestionCreationForm;
import ru.kpfu.itis.adaptivetest.models.AdaptedQuestion;
import ru.kpfu.itis.adaptivetest.models.Question;
import ru.kpfu.itis.adaptivetest.models.Test;
import ru.kpfu.itis.adaptivetest.models.User;
import ru.kpfu.itis.adaptivetest.repositories.QuestionRepository;
import ru.kpfu.itis.adaptivetest.repositories.TestRepository;
import ru.kpfu.itis.adaptivetest.services.interfaces.QuestionServiceInterface;

import java.util.*;

/**
 * @author Bulat Giniyatullin
 * 11 Апрель 2018
 */

@Service
public class QuestionServiceImpl implements QuestionServiceInterface {

    private final QuestionRepository questionRepository;
    private final TestRepository testRepository;

    @Autowired
    public QuestionServiceImpl(QuestionRepository questionRepository, TestRepository testRepository) {
        this.questionRepository = questionRepository;
        this.testRepository = testRepository;
    }

    @Override
    public Map<String, Object> getQuestionWithLevelsById(Long questionId) {
        Map<String, Object> data = new HashMap<>();
        Optional<Question> optionalQuestion = questionRepository.findById(questionId);
        if (optionalQuestion.isPresent()) {
            Question question = optionalQuestion.get();
            data.put("test_id", question.getTest().getId());
            data.put("test_name", question.getTest().getName());
            data.put("question_name", question.getName());
            List<Map<String, Object>> levels = new LinkedList<>();
            for (AdaptedQuestion level: question.getAdaptedQuestions()) {
                HashMap<String, Object> levelMap = new HashMap<>();
                levelMap.put("id", level.getId());
                levelMap.put("level", level.getLevel());
                levels.add(levelMap);
            }
            data.put("levels", levels);
        }
        return data;
    }

    @Override
    public void createQuestion(QuestionCreationForm questionCreationForm) throws NotFoundException {
        Optional<Test> optionalTest = testRepository.findById(questionCreationForm.getTestId());
        if (optionalTest.isPresent()) {
            Question question = Question.builder()
                    .test(Test.builder()
                            .id(questionCreationForm.getTestId())
                            .build())
                    .name(questionCreationForm.getName())
                    .build();
            questionRepository.save(question);
        } else {
            throw new NotFoundException(Test.class);
        }
    }

    @Override
    public User getAuthorOfTestByQuestionId(Long id) throws NotFoundException{
        Optional<Question> question = questionRepository.findById(id);
        if (question.isPresent()) {
            return question.get().getTest().getAuthor();
        }
        else {
            throw new NotFoundException(Question.class);
        }
    }
}
