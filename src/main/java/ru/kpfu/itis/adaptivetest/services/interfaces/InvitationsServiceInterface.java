package ru.kpfu.itis.adaptivetest.services.interfaces;

import ru.kpfu.itis.adaptivetest.exceptions.NotFoundException;

/**
 * @author Bulat Giniyatullin
 * 26 April 2018
 */

public interface InvitationsServiceInterface {
    void invite(String login, Long testId) throws NotFoundException;

    void uninvite(String login, Long testId);
}
