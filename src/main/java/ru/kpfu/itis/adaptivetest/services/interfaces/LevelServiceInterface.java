package ru.kpfu.itis.adaptivetest.services.interfaces;

import ru.kpfu.itis.adaptivetest.exceptions.NotFoundException;
import ru.kpfu.itis.adaptivetest.forms.AdaptedQuestionForm;
import ru.kpfu.itis.adaptivetest.models.User;

import java.util.Map;

/**
 * @author Bulat Giniyatullin
 * 12 April 2018
 */

public interface LevelServiceInterface {
    void deleteLevelById(Long id);
    void createLevel(AdaptedQuestionForm form) throws NotFoundException;
    Map<String, Object> getLevelInfo(Long id) throws NotFoundException;
    User getAuthorOfTestByLevelId(Long id) throws NotFoundException;
}
