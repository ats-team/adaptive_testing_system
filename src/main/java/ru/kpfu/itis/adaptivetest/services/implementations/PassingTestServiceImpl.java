package ru.kpfu.itis.adaptivetest.services.implementations;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import ru.kpfu.itis.adaptivetest.exceptions.AlreadyFinishedException;
import ru.kpfu.itis.adaptivetest.exceptions.AlreadyPassedException;
import ru.kpfu.itis.adaptivetest.exceptions.AlreadyStartedException;
import ru.kpfu.itis.adaptivetest.exceptions.NotFoundException;
import ru.kpfu.itis.adaptivetest.forms.AnswerForm;
import ru.kpfu.itis.adaptivetest.models.*;
import ru.kpfu.itis.adaptivetest.repositories.*;
import ru.kpfu.itis.adaptivetest.security.details.UserDetailsImpl;
import ru.kpfu.itis.adaptivetest.services.interfaces.PassingTestService;
import ru.kpfu.itis.adaptivetest.utils.DateTimeUtil;

import javax.servlet.http.HttpSession;
import java.util.*;

@Service
public class PassingTestServiceImpl implements PassingTestService{
    private TestRepository testRepository;
    private QuestionRepository questionRepository;
    private AvailableTestRepository availableTestRepository;
    private AdaptedQuestionRepository adaptedQuestionRepository;
    private PassedTestRepository passedTestRepository;
    private PassedQuestionRepository passedQuestionRepository;

    private DateTimeUtil dateTimeUtil;

    public HttpSession session() {
        ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
        return attr.getRequest().getSession(true); // true == allow create
    }

    public User getUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return  ((UserDetailsImpl)authentication.getPrincipal()).getUser();
    }

    @Autowired
    public void setPassedQuestionRepository(PassedQuestionRepository passedQuestionRepository) {
        this.passedQuestionRepository = passedQuestionRepository;
    }

    @Autowired
    public void setAdaptedQuestionRepository(AdaptedQuestionRepository adaptedQuestionRepository) {
        this.adaptedQuestionRepository = adaptedQuestionRepository;
    }

    @Autowired
    public void setTestRepository(TestRepository testRepository) {
        this.testRepository = testRepository;
    }

    @Autowired
    public void setQuestionRepository(QuestionRepository questionRepository) {
        this.questionRepository = questionRepository;
    }

    @Autowired
    public void setAvailableTestRepository(AvailableTestRepository availableTestRepository) {
        this.availableTestRepository = availableTestRepository;
    }

    @Autowired
    public void setPassedTestRepository(PassedTestRepository passedTestRepository) {
        this.passedTestRepository = passedTestRepository;
    }

    @Autowired
    public void setDateTimeUtil(DateTimeUtil dateTimeUtil) {
        this.dateTimeUtil = dateTimeUtil;
    }

    @Override
    public Map<String, Object> startTest(Long testId) throws AlreadyStartedException, AccessDeniedException, NotFoundException {
        User user = getUser();

        Optional<Test> testOptional = testRepository.findById(testId);
        if (testOptional.isPresent()) {
            Test test = testOptional.get();
            if (testIsAvailableForUser(user, test)) {
                Optional<PassedTest> passedTestOptional = passedTestRepository.findByUserAndAndTest(user, test);
                if (passedTestOptional.isPresent()) {
                    throw new AlreadyStartedException();
                }
                PassedTest passedTest = PassedTest.builder()
                        .test(test)
                        .complexity(50)
                        .score(0)
                        .user(user)
                        .passedQuestions(new LinkedList<>())
                        .date(new Date())
                        .duration(test.getMaxDuration()) //на случай, если тест завершится "аварийно"
                        .build();
                passedTestRepository.save(passedTest);

                session().setAttribute("passed_test", passedTest);

                Question nextQuestion = getNextQuestion(passedTest, null);
                Map<String, Object> result = new HashMap<>();
                result.put("next_question_id", nextQuestion.getId());

                return result;
            }else {
                throw new AccessDeniedException("403");
            }
        }else {
            throw new NotFoundException(Test.class);
        }
    }

    private boolean testIsAvailableForUser(User user, Test test){
        if (!test.getIsPrivate() && test.getIsAvailable()) return true;
        return availableTestRepository.findAllByUserAndTest(user, test).size() > 0;
    }

    @Override
    public Map<String, Object> finishTest(/*PassedTest passedTest*/) throws AlreadyFinishedException, AccessDeniedException{
        PassedTest passedTest = (PassedTest) session().getAttribute("passed_test");
        if (passedTest == null) {
            throw new AccessDeniedException("403");
        }
        if (!isFinished(passedTest)) {
            int duration = (int) dateTimeUtil.getDifferenceInSeconds(new Date(), passedTest.getDate());
            passedTest.setDuration(duration);
            passedTestRepository.save(passedTest);
            Map<String, Object> result = new HashMap<>();
            result.put("passed_test_id", passedTest.getId());
            session().removeAttribute("passed_test");
            return result;
        } else {
            throw new AlreadyFinishedException();
        }
    }

    public boolean isFinished(PassedTest passedTest) {
        Date date = new Date();
        return date.compareTo(dateTimeUtil.addSeconds(passedTest.getDate(), passedTest.getTest().getMaxDuration())) > 0
                || passedTest.getDuration() != passedTest.getTest().getMaxDuration();
    }

    @Override
    public Map<String, Object> processAnswer(AnswerForm form/*, User user, PassedTest passedTest*/) throws AlreadyFinishedException, AlreadyPassedException, NotFoundException, AccessDeniedException{
        System.out.println("session in service: " + session().getAttribute("passed_test"));
        User user = getUser();
        PassedTest passedTest = (PassedTest)session().getAttribute("passed_test");
        if (passedTest == null || user.getId() != passedTest.getUser().getId()) {
            throw new AccessDeniedException("403");
        }
        //проверяем, не завершен ли тест
        if (!isFinished(passedTest)) {
            Optional<AdaptedQuestion> adaptedQuestionOptional = adaptedQuestionRepository.findById(form.getLevel_id());
            if (adaptedQuestionOptional.isPresent()) {
                //проверяем, не пройден ли уже вопрос
                if (isPassed(adaptedQuestionOptional.get().getQuestion(), passedTest)) {
                    throw new AlreadyPassedException();
                }
                AdaptedQuestion adaptedQuestion = adaptedQuestionOptional.get();
                if (adaptedQuestion.getQuestion().getTest().getId() != passedTest.getTest().getId()) {
                    throw new AccessDeniedException("403");
                }
                int questionsCount = questionRepository.countAllByTest(passedTest.getTest());
                int delta = 100 / questionsCount;
                delta *=  (adaptedQuestion.getRightAnswer().equals(form.getAnswer())) ? 1: -1;
                passedTest.setComplexity(passedTest.getComplexity() + delta);
                PassedQuestion passedQuestion = PassedQuestion.builder()
                        .adaptedQuestion(adaptedQuestion)
                        .passedTest(passedTest)
                        .answer(form.getAnswer())
                        .build();
                passedQuestionRepository.save(passedQuestion);

                if (adaptedQuestion.getRightAnswer().equals(form.getAnswer())) {
                    passedTest.setScore(passedTest.getScore() + adaptedQuestion.getScore());
                }
                if (passedTest.getPassedQuestions() == null){
                    passedTest.setPassedQuestions(new LinkedList<>());
                }
                passedTest.getPassedQuestions().add(passedQuestion);
                passedTestRepository.save(passedTest);
                //подбирается id следующего question
                Question nextQuestion = getNextQuestion(passedTest, adaptedQuestion.getQuestion().getId());
                Map<String, Object> result = new HashMap<>();
                if (nextQuestion != null) {
                    result.put("next_question_id", nextQuestion.getId());
                } else {
                    return finishTest();
                }

                return result;
            } else {
                throw new NotFoundException(AdaptedQuestion.class);
            }
        } else {
            throw new AlreadyFinishedException();
        }

    }

    @Override
    public Map<String, Object> getCurrentQuestion(Long question_id/*, PassedTest passedTest*/) throws AlreadyPassedException, NotFoundException{
        User user = getUser();
        PassedTest passedTest = (PassedTest)session().getAttribute("passed_test");
        if (passedTest == null || user.getId() != passedTest.getUser().getId()) {
            throw new AccessDeniedException("403");
        }
        Optional<Question> question = questionRepository.findById(question_id);
        if (question.isPresent()) {
            if (isPassed(question.get(), passedTest)){
                throw new AlreadyPassedException();
            }
        } else {
            throw new NotFoundException(Question.class);
        }
        if (question.get().getTest().getId() != passedTest.getTest().getId()) {
            throw new AccessDeniedException("403");
        }
        //подбирается adaptedQuestion
        int count = adaptedQuestionRepository.countAdaptedQuestionByQuestionId(question_id);
        int level = passedTest.getComplexity() * count / 100 + 1;
        level = level > count? count: level;
        //System.out.println("levels count: " + count);
        //System.out.println("current level: " + level);
        List<AdaptedQuestion> adaptedQuestions = adaptedQuestionRepository.findAllByQuestionIdAndLevel(question_id, level);
        if (adaptedQuestions.size() > 0) {
            AdaptedQuestion adaptedQuestion = adaptedQuestions.get(0);

            //считается, сколько вопросов уже пройдено
            int passed_count = passedQuestionRepository.countAllByPassedTest(passedTest);

            int questionCount = questionRepository.countAllByTest(passedTest.getTest());

            Date endTime = dateTimeUtil.addSeconds(passedTest.getDate(), passedTest.getTest().getMaxDuration());

            Map<String, Object> result = new HashMap<>();
            result.put("question_id", question_id);
            result.put("level_id", adaptedQuestion.getId());
            result.put("test_name", passedTest.getTest().getName());
            result.put("picture_path", adaptedQuestion.getPhotoPath());
            result.put("file_path", adaptedQuestion.getFilePath());
            result.put("text", adaptedQuestion.getText());
            result.put("answered_questions_count", passed_count);
            result.put("question_count", questionCount);
            result.put("end_time", endTime.getTime());
            result.put("passed_test_id", passedTest.getId());
            return result;
        } else {
            throw new NotFoundException(AdaptedQuestion.class);
        }
    }

    @Override
    public Question getNextQuestion(PassedTest passedTest, Long question_id) {
        if (question_id == null){
            question_id = 0L;
        }
        List<Question> nonPassedQuestions = questionRepository.getNonPassedQuestions(passedTest, question_id, passedTest.getTest());
        if (nonPassedQuestions.size() > 0) {
            return nonPassedQuestions.get(0);
        } else {
            return null;
        }
    }

    public boolean isPassed(Question question, PassedTest passedTest) {
        Optional<PassedQuestion> passedQuestion = passedQuestionRepository.findByPassedTestAndAdaptedQuestion_Question(passedTest, question);
        return  (passedQuestion.isPresent());
    }

}
