package ru.kpfu.itis.adaptivetest.services.interfaces;

import org.springframework.security.access.AccessDeniedException;
import ru.kpfu.itis.adaptivetest.exceptions.AlreadyFinishedException;
import ru.kpfu.itis.adaptivetest.exceptions.AlreadyPassedException;
import ru.kpfu.itis.adaptivetest.exceptions.AlreadyStartedException;
import ru.kpfu.itis.adaptivetest.exceptions.NotFoundException;
import ru.kpfu.itis.adaptivetest.forms.AnswerForm;
import ru.kpfu.itis.adaptivetest.models.PassedTest;
import ru.kpfu.itis.adaptivetest.models.Question;
import ru.kpfu.itis.adaptivetest.models.User;

import java.util.Map;

public interface PassingTestService {
    Map<String, Object> startTest(Long testId) throws AlreadyStartedException, AccessDeniedException, NotFoundException;
    Map<String, Object> finishTest(/*PassedTest passedTest*/) throws AlreadyFinishedException, AccessDeniedException;
    Map<String, Object> processAnswer(AnswerForm form/*, User user, PassedTest passedTest*/) throws AlreadyFinishedException, AlreadyPassedException, NotFoundException, AccessDeniedException;
    Map<String, Object> getCurrentQuestion(Long question_id/*, PassedTest passedTest*/) throws AlreadyPassedException, NotFoundException, AccessDeniedException;
    Question getNextQuestion(PassedTest passedTest, Long question_id);
}
