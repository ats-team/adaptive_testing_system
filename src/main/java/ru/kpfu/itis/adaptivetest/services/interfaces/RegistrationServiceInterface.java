package ru.kpfu.itis.adaptivetest.services.interfaces;

import ru.kpfu.itis.adaptivetest.forms.UserRegistrationForm;

/**
 * @author Rustem Saitgareev
 * 11-602
 * 000
 */
public interface RegistrationServiceInterface {
    void register(UserRegistrationForm user);
}
