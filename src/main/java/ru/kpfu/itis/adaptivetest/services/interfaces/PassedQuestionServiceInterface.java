package ru.kpfu.itis.adaptivetest.services.interfaces;

import java.util.Map;

/**
 * @author Bulat Giniyatullin
 * 24 April 2018
 */

public interface PassedQuestionServiceInterface {
    Map<String, Object> getPassedQuestionById(Long questionId);
}
