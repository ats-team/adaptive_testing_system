package ru.kpfu.itis.adaptivetest.services.implementations;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import ru.kpfu.itis.adaptivetest.exceptions.NotAccessException;
import ru.kpfu.itis.adaptivetest.exceptions.NotFoundException;
import ru.kpfu.itis.adaptivetest.models.PassedQuestion;
import ru.kpfu.itis.adaptivetest.models.PassedTest;
import ru.kpfu.itis.adaptivetest.models.User;
import ru.kpfu.itis.adaptivetest.repositories.PassedQuestionRepository;
import ru.kpfu.itis.adaptivetest.repositories.PassedTestRepository;
import ru.kpfu.itis.adaptivetest.security.details.UserDetailsImpl;
import ru.kpfu.itis.adaptivetest.services.interfaces.PassedTestServiceInterface;

import java.util.*;

import static org.springframework.data.jpa.domain.Specification.where;
import static ru.kpfu.itis.adaptivetest.specifications.PassedTestSpecification.*;

/**
 * @author Rustem Saitgareev
 * 11-602
 * 000
 */
@Service
public class PassedTestServiceImpl implements PassedTestServiceInterface {
    private PassedTestRepository passedTestRepository;
    private PassedQuestionRepository passedQuestionRepository;

    @Autowired
    public PassedTestServiceImpl(PassedTestRepository passedTestRepository, PassedQuestionRepository passedQuestionRepository) {
        this.passedTestRepository = passedTestRepository;
        this.passedQuestionRepository = passedQuestionRepository;
    }

    @Override
    public Map<String, Object> getResultById(Long id) throws NotFoundException, NotAccessException {
        Optional<PassedTest> optionalPassedTest = passedTestRepository.findById(id);
        if (optionalPassedTest.isPresent()) {
            PassedTest passedTest = optionalPassedTest.get();
            User user = ((UserDetailsImpl) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUser();
            if (passedTest.getUser().getId() == user.getId() || passedTest.getTest().getAuthor().getId() == user.getId()) {
                Map<String, Object> result = new HashMap<>();
                result.put("passed_test_id", passedTest.getId());
                result.put("name", passedTest.getTest().getName());
                result.put("detailed_description", passedTest.getTest().getDetailedDescription());
                result.put("creator_login", passedTest.getTest().getAuthor().getLogin());
                result.put("creator_fio", passedTest.getTest().getAuthor().getFio());
                result.put("passing_date", passedTest.getTimeStampPassed());
                result.put("duration", passedTest.getDuration());
                result.put("received_score", passedTest.getScore());
                result.put("max_score", passedTest.getTest().getMaxScore());
                if (passedTest.getTest().getIsQuestionsDetailsAvailable() || passedTest.getTest().getAuthor().getId() == user.getId()) {
                    List<Map<String, Object>> passedQuestionsList = new LinkedList<>();
                    for (Object[] entity : passedQuestionRepository.findPassedQuestionWithMaxScoreByPassedTest(passedTest)) {
                        Map<String, Object> element = new HashMap<>();
                        PassedQuestion passedQuestion = (PassedQuestion) entity[0];
                        Integer maxScoreForQuestion = (Integer) entity[1];
                        element.put("passed_question_id", passedQuestion.getId());
                        element.put("question_name", passedQuestion.getAdaptedQuestion().getQuestion().getName());
                        element.put("received_score", passedQuestion.getAnswer().equals(passedQuestion.getAdaptedQuestion().getRightAnswer())
                                ? passedQuestion.getAdaptedQuestion().getScore() : 0);
                        element.put("max_score", maxScoreForQuestion);
                        passedQuestionsList.add(element);
                    }
                    result.put("passed_questions_list", passedQuestionsList);
                }
                return result;
            } else {
                throw new NotAccessException();
            }
        } else {
            throw new NotFoundException(PassedTest.class);
        }
    }

    @Override
    public List<Map<String, Object>> getPassedTestList(String nameFilteringValue, String loginFilteringValue, String fioFilteringValue,
                                                       Integer pageNumber, Integer pageSize) {
        User user = ((UserDetailsImpl) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUser();
        Pageable pageable = PageRequest.of(pageNumber == null ? 0 : pageNumber, pageSize == null ? Integer.MAX_VALUE : pageSize);
        Page<PassedTest> tests = passedTestRepository.findAll(
                where(isToUser(user))
                        .and(isNameSatisfiesFilter(nameFilteringValue))
                        .and(isAuthorLoginSatisfiesFilter(loginFilteringValue))
                        .and(isAuthorFioSatisfiesFilter(fioFilteringValue)), pageable);
        List<Map<String, Object>> result = new ArrayList<>();
        tests.forEach(t -> {
            Map<String, Object> testData = new HashMap<>();
            testData.put("id", t.getId());
            testData.put("name", t.getTest().getName());
            testData.put("short_description", t.getTest().getShortDescription());
            testData.put("creator_login", t.getTest().getAuthor().getLogin());
            testData.put("creator_fio", t.getTest().getAuthor().getFio());
            testData.put("passing_date", t.getTimeStampPassed());
            testData.put("duration", t.getDuration());
            testData.put("received_score", t.getScore());
            testData.put("max_score", t.getTest().getMaxScore());
            result.add(testData);
        });
        return result;
    }
}
