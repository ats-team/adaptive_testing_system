package ru.kpfu.itis.adaptivetest.services.implementations;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;
import ru.kpfu.itis.adaptivetest.exceptions.BadQuestionsException;
import ru.kpfu.itis.adaptivetest.forms.TestCreationForm;
import ru.kpfu.itis.adaptivetest.models.*;
import ru.kpfu.itis.adaptivetest.repositories.*;
import ru.kpfu.itis.adaptivetest.security.details.UserDetailsImpl;
import ru.kpfu.itis.adaptivetest.services.interfaces.TestServiceInterface;

import java.util.*;

import static org.springframework.data.jpa.domain.Specification.where;
import static ru.kpfu.itis.adaptivetest.specifications.AvailableTestSpecifications.*;
import static ru.kpfu.itis.adaptivetest.specifications.MyTestSpecifications.isUserAuthor;
import static ru.kpfu.itis.adaptivetest.specifications.TestSpecifications.*;

/**
 * @author Rustem Saitgareev
 * 11-602
 * 000
 */
@Service
public class TestServiceImpl implements TestServiceInterface {
    private TestRepository testRepository;
    private QuestionRepository questionRepository;
    private AvailableTestRepository availableTestRepository;
    private AdaptedQuestionRepository adaptedQuestionRepository;
    private PassedTestRepository passedTestRepository;

    @Autowired
    public void setAdaptedQuestionRepository(AdaptedQuestionRepository adaptedQuestionRepository) {
        this.adaptedQuestionRepository = adaptedQuestionRepository;
    }

    @Autowired
    public void setTestRepository(TestRepository testRepository) {
        this.testRepository = testRepository;
    }

    @Autowired
    public void setQuestionRepository(QuestionRepository questionRepository) {
        this.questionRepository = questionRepository;
    }

    @Autowired
    public void setAvailableTestRepository(AvailableTestRepository availableTestRepository) {
        this.availableTestRepository = availableTestRepository;
    }

    @Autowired
    public void setPassedTestRepository(PassedTestRepository passedTestRepository) {
        this.passedTestRepository = passedTestRepository;
    }

    @Override
    public Map<String, Object> getTestInfo(Long id) {
        Test test = testRepository.getOne(id);
        Map<String, Object> testInfo = new HashMap<>();
        testInfo.put("test_id", test.getId());
        testInfo.put("name", test.getName());
        testInfo.put("detailed_description", test.getDetailedDescription());
        testInfo.put("creator_login", test.getAuthor().getLogin());
        testInfo.put("creator_fio", test.getAuthor().getFio());
        testInfo.put("max_duration", test.getMaxDuration());
        testInfo.put("question_count", questionRepository.countAllByTest(test));
        testInfo.put("date_created", test.getTimeStampCreated());
        return testInfo;
    }

    @Override
    public Map<String, Object> getTestInfoForEditPage(Long id) {
        Test test = testRepository.getOne(id);
        Map<String, Object> testInfo = new HashMap<>();
        testInfo.put("test_id", test.getId());
        testInfo.put("test_name", test.getName());
        testInfo.put("short_description", test.getShortDescription());
        testInfo.put("detailed_description", test.getDetailedDescription());
        testInfo.put("max_duration", test.getMaxDuration());
        testInfo.put("is_private", test.getIsPrivate());
        testInfo.put("is_questions_details_available", test.getIsQuestionsDetailsAvailable());
        List<Map<String, Object>> questions = new LinkedList<>();
        for (Question question : test.getQuestions()) {
            HashMap<String, Object> questionMap = new HashMap<>();
            questionMap.put("question_id", question.getId());
            questionMap.put("question_name", question.getName());
            questionMap.put("level_count", question.getLevelCount());
            questions.add(questionMap);
        }
        testInfo.put("questions", questions);
        return testInfo;
    }

    @Override
    public List<Map<String, Object>> getAllPublicTests(String nameFilteringValue,
                                                       String loginFilteringValue,
                                                       String fioFilteringValue,
                                                       Integer pageNumber,
                                                       Integer pageSize) {
        Pageable pageable = PageRequest.of(pageNumber == null ? 0 : pageNumber, pageSize == null ? Integer.MAX_VALUE : pageSize);
        Page<Test> tests = testRepository.findAll(
                where(isAvailable())
                        .and(isPublic())
                        .and(isNameSatisfiesFilter(nameFilteringValue))
                        .and(isAuthorLoginSatisfiesFilter(loginFilteringValue))
                        .and(isAuthorFioSatisfiesFilter(fioFilteringValue)), pageable);

        List<Map<String, Object>> data = new ArrayList<>();

        tests.forEach(test -> {
            Map<String, Object> testData = new HashMap<>();
            testData.put("id", test.getId());
            testData.put("name", test.getName());
            testData.put("short_description", test.getShortDescription());
            testData.put("creator_login", test.getAuthor().getLogin());
            testData.put("creator_fio", test.getAuthor().getFio());
            testData.put("max_duration", test.getMaxDuration());
            testData.put("date_created", test.getTimeStampCreated());
            data.add(testData);
        });

        return data;
    }

    @Override
    public List<Map<String, Object>> getAvailableTests(Authentication authentication,
                                                       String nameFilteringValue,
                                                       String fioFilteringValue,
                                                       String loginFilteringValue,
                                                       Integer pageNumber,
                                                       Integer pageSize) {
        Pageable pageable = PageRequest.of(pageNumber == null ? 0 : pageNumber, pageSize == null ? Integer.MAX_VALUE : pageSize);
        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
        User user = userDetails.getUser();
        Page<AvailableTest> tests = availableTestRepository.findAll(
                where(isToUser(user))
                        .and(isNameSatisfiesFilterAT(nameFilteringValue))
                        .and(isAuthorLoginSatisfiesFilterAT(loginFilteringValue))
                        .and(isAuthorFioSatisfiesFilterAT(fioFilteringValue)), pageable);

        List<Map<String, Object>> result = new ArrayList<>();

        tests.forEach(t -> {
            Map<String, Object> testData = new HashMap<>();
            testData.put("id", t.getTest().getId());
            testData.put("name", t.getTest().getName());
            testData.put("short_description", t.getTest().getShortDescription());
            testData.put("creator_login", t.getTest().getAuthor().getLogin());
            testData.put("creator_fio", t.getTest().getAuthor().getFio());
            testData.put("max_duration", t.getTest().getMaxDuration());
            result.add(testData);
        });
        return result;
    }

    @Override
    public List<Map<String, Object>> getMyTests(Authentication authentication, String nameFilteringValue, Integer pageNumber, Integer pageSize) {
        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
        User user = userDetails.getUser();
        Pageable pageable = PageRequest.of(pageNumber == null ? 0 : pageNumber, pageSize == null ? Integer.MAX_VALUE : pageSize);
        Page<Test> tests = testRepository.findAll(
                where(isUserAuthor(user))
                        .and(isNameSatisfiesFilter(nameFilteringValue)), pageable);
        List<Map<String, Object>> data = new ArrayList<>();
        tests.forEach(test -> {
                    Map<String, Object> testData = new HashMap<>();
                    testData.put("id", test.getId());
                    testData.put("name", test.getName());
                    testData.put("short_description", test.getShortDescription());
                    testData.put("is_private", test.getIsPrivate());
                    testData.put("is_available", test.getIsAvailable());
                    testData.put("max_duration", test.getMaxDuration());
                    testData.put("max_score", test.getMaxScore());
                    testData.put("count_of_participants", test.getPassedTests().size());
                    testData.put("date_created", test.getTimeStampCreated());
                    data.add(testData);
                });
        return data;
    }

    @Override
    public Map<String, Object> create(TestCreationForm form, User user) {
        Test newTest = Test.builder()
                .name(form.getName())
                .shortDescription(form.getShortDescription())
                .detailedDescription(form.getDetailedDescription())
                .maxDuration(form.getMaxDuration())
                .isPrivate(form.getIsPrivate() != null && form.getIsPrivate().equals("isPrivate"))
                .isQuestionsDetailsAvailable(form.getIsQuestionsDetailsAvailable() != null && form.getIsQuestionsDetailsAvailable().equals("isQuestionsDetailsAvailable"))
                .dateCreated(new Date())
                .author(user)
                .isAvailable(false)
                .build();
        Test createdTest = testRepository.save(newTest);
        Map<String, Object> result = new HashMap<>();
        result.put("id", createdTest.getId());
        return result;
    }

    @Override
    public void edit(Test test, TestCreationForm form) {
        test.setName(form.getName());
        test.setShortDescription(form.getShortDescription());
        test.setDetailedDescription(form.getDetailedDescription());
        test.setMaxDuration(form.getMaxDuration());
        test.setIsPrivate(form.getIsPrivate() != null && form.getIsPrivate().equals("isPrivate"));
        test.setIsQuestionsDetailsAvailable(form.getIsQuestionsDetailsAvailable() != null && form.getIsQuestionsDetailsAvailable().equals("isQuestionsDetailsAvailable"));
        testRepository.save(test);
    }

    @Override
    public void makeAvailable(Test test) throws BadQuestionsException {
        int maxScore = 0;
        List<Question> questionList = test.getQuestions();
        if (questionList.size() == 0) {
            throw new BadQuestionsException();
        }
        for (Question question : test.getQuestions()) {
            List<AdaptedQuestion> adaptedQuestionSet = question.getAdaptedQuestions();
            if (adaptedQuestionSet.size() == 0) {
                throw new BadQuestionsException();
            } else {
               AdaptedQuestion adaptedQuestionThatHasHighestScore = adaptedQuestionSet
                       .stream()
                       .max(Comparator.comparingInt(AdaptedQuestion::getScore))
                       .get();
               maxScore += adaptedQuestionThatHasHighestScore.getScore();
            }
        }
        test.setIsAvailable(true);
        test.setMaxScore(maxScore);
        testRepository.save(test);
    }

    @Override
    public Optional<Test> getTestById(Long id) {
        return testRepository.findById(id);
    }


}
