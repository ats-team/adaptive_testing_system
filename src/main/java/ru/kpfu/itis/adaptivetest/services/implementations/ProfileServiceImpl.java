package ru.kpfu.itis.adaptivetest.services.implementations;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.kpfu.itis.adaptivetest.forms.UserForm;
import ru.kpfu.itis.adaptivetest.models.User;
import ru.kpfu.itis.adaptivetest.repositories.UserRepository;
import ru.kpfu.itis.adaptivetest.security.details.UserDetailsImpl;
import ru.kpfu.itis.adaptivetest.services.interfaces.ProfileServiceInterface;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/**
 * @author Bulat Giniyatullin
 * 03 Апрель 2018
 */

@Service
public class ProfileServiceImpl implements ProfileServiceInterface {

    private UserRepository userRepository;

    private PasswordEncoder passwordEncoder;

    @Autowired
    public ProfileServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Autowired
    public void setPasswordEncoder(PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public Map<String, Object> getUserByIdOrLogin(Long id, String login) {
        if (id == null && login == null) {
            User user = ((UserDetailsImpl) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUser();
            return prepareProfileData(Optional.of(user));
        } else if (id == null) {
            return prepareProfileData(userRepository.findOneByLogin(login));
        } else {
            return prepareProfileData(userRepository.findById(id));
        }
    }

    private Map<String, Object> prepareProfileData(Optional<User> optionalUser) {
        Map<String, Object> data = new HashMap<>();
        if (optionalUser.isPresent()) {
            User user = optionalUser.get();
            data.put("id", user.getId());
            data.put("login", user.getLogin());
            data.put("fio", user.getFio());
            data.put("email", user.getEmail());
        }
        return data;
    }

    @Override
    public void updateUserById(UserForm userForm) {
        User updatedUser = userRepository.findById(userForm.getId()).get();
        if (userForm.getLogin() != null && !"".equals(userForm.getLogin())) {
            updatedUser.setLogin(userForm.getLogin());
        }
        if (userForm.getFio() != null && !"".equals(userForm.getFio())) {
            updatedUser.setFio(userForm.getFio());
        }
        if (userForm.getEmail() != null && !"".equals(userForm.getEmail())) {
            updatedUser.setEmail(userForm.getEmail());
        }
        if (userForm.getPassword() != null && !"".equals(userForm.getPassword())) {
            updatedUser.setPassword(passwordEncoder.encode(userForm.getPassword()));
        }
        userRepository.save(updatedUser);
    }
}
