package ru.kpfu.itis.adaptivetest.services.interfaces;

import ru.kpfu.itis.adaptivetest.exceptions.NotFoundException;
import ru.kpfu.itis.adaptivetest.forms.QuestionCreationForm;
import ru.kpfu.itis.adaptivetest.models.User;

import java.util.Map;

/**
 * @author Bulat Giniyatullin
 * 11 Апрель 2018
 */

public interface QuestionServiceInterface {
    Map<String, Object> getQuestionWithLevelsById(Long questionId);

    void createQuestion(QuestionCreationForm questionCreationForm) throws NotFoundException;

    User getAuthorOfTestByQuestionId(Long questionId) throws NotFoundException;
}
