package ru.kpfu.itis.adaptivetest.services.interfaces;

import ru.kpfu.itis.adaptivetest.forms.UserForm;
import ru.kpfu.itis.adaptivetest.models.User;

import java.util.Map;

/**
 * @author Bulat Giniyatullin
 * 03 Апрель 2018
 */

public interface ProfileServiceInterface {
    Map<String, Object> getUserByIdOrLogin(Long id, String login);
    void updateUserById(UserForm userForm);
}
