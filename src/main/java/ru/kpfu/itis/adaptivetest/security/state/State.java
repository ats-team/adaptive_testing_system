package ru.kpfu.itis.adaptivetest.security.state;

/**
 * @author Rustem Saitgareev
 * 11-602
 * 000
 */
public enum State {
    CONFIRMED, NOT_CONFIRMED, BANNED, DELETED
}
