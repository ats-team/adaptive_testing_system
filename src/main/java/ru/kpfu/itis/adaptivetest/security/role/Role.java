package ru.kpfu.itis.adaptivetest.security.role;

/**
 * @author Rustem Saitgareev
 * 11-602
 * 000
 */
public enum Role {
    ADMIN, USER
}
