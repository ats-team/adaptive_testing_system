package ru.kpfu.itis.adaptivetest.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import ru.kpfu.itis.adaptivetest.services.interfaces.AuthServiceInterface;
import ru.kpfu.itis.adaptivetest.services.interfaces.TestServiceInterface;

import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/internal_api/tests")
public class TestInfoController {
    private TestServiceInterface testService;

    @Autowired
    public TestInfoController(TestServiceInterface testService) {
        this.testService = testService;
    }

    @GetMapping("/{id:[0-9]+}")
    public Map<String, Object> getTestInfo(@PathVariable("id") Long id) {
        return testService.getTestInfo(id);
    }

    @GetMapping("/all_public")
    public List<Map<String, Object>> getAllPublicTests(
            @RequestParam(name = "name", required = false) String nameFilteringValue,
            @RequestParam(name = "login", required = false) String loginFilteringValue,
            @RequestParam(name = "fio", required = false) String fioFilteringValue,
            @RequestParam(name = "pageNumber", required = false) Integer pageNumber,
            @RequestParam(name = "pageSize", required = false) Integer pageSize) {
        return testService.getAllPublicTests(nameFilteringValue, loginFilteringValue, fioFilteringValue, pageNumber, pageSize);
    }

    @GetMapping("/available")
    public List<Map<String, Object>> getAvailableTests(Authentication authentication,
            @RequestParam(name = "name", required = false) String nameFilteringValue,
            @RequestParam(name = "login", required = false) String loginFilteringValue,
            @RequestParam(name = "fio", required = false) String fioFilteringValue,
            @RequestParam(name = "pageNumber", required = false) Integer pageNumber,
            @RequestParam(name = "pageSize", required = false) Integer pageSize) {
        return testService.getAvailableTests(authentication, nameFilteringValue, fioFilteringValue, loginFilteringValue, pageNumber, pageSize);
    }

    @GetMapping("/my")
    public List<Map<String, Object>> getMyTests(@RequestParam(name = "name", required = false) String nameFilteringValue,
                                                @RequestParam(name = "pageNumber", required = false) Integer pageNumber,
                                                @RequestParam(name = "pageSize", required = false) Integer pageSize,
                                                HttpServletResponse resp) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null) {
            return testService.getMyTests(authentication, nameFilteringValue, pageNumber, pageSize);
        } else {
            resp.setStatus(401);
            return new LinkedList<>();
        }
    }
}
