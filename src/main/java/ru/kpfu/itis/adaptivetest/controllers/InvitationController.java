package ru.kpfu.itis.adaptivetest.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.kpfu.itis.adaptivetest.aspects.annotations.TestAuthorshipCheck;
import ru.kpfu.itis.adaptivetest.exceptions.NotFoundException;
import ru.kpfu.itis.adaptivetest.services.interfaces.InvitationsServiceInterface;

import javax.servlet.http.HttpServletResponse;

/**
 * @author Bulat Giniyatullin
 * 26 April 2018
 */

@RestController
@RequestMapping("/internal_api/invitations")
public class InvitationController {
    private final InvitationsServiceInterface invitationsService;

    @Autowired
    public InvitationController(InvitationsServiceInterface invitationsService) {
        this.invitationsService = invitationsService;
    }

    @TestAuthorshipCheck
    @PostMapping("/invite")
    public void invite(@RequestParam("test_id") Long testId,
                       @RequestParam("login") String login,
                       HttpServletResponse response) {
        try {
            invitationsService.invite(login, testId);
        } catch (NotFoundException e) {
            response.setStatus(HttpServletResponse.SC_NOT_FOUND);
        }
    }

    @TestAuthorshipCheck
    @PostMapping("/uninvite")
    public void uninvite(@RequestParam("test_id") Long testId,
                         @RequestParam("login") String login) {
        invitationsService.uninvite(login, testId);
    }
}
