package ru.kpfu.itis.adaptivetest.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.kpfu.itis.adaptivetest.aspects.annotations.TestAuthorshipCheck;
import ru.kpfu.itis.adaptivetest.services.implementations.ResultsOfTestCreatedByMeServiceImpl;

import java.util.Map;

/**
 * @author Bulat Giniyatullin
 * 18 April 2018
 */

@RestController
@RequestMapping("/internal_api/tests/my/{test_id}/results")
public class ResultsOfTestCreatedByMeController {
    private final ResultsOfTestCreatedByMeServiceImpl resultsOfTestCreatedByMeService;

    @Autowired
    public ResultsOfTestCreatedByMeController(ResultsOfTestCreatedByMeServiceImpl resultsOfTestCreatedByMeService) {
        this.resultsOfTestCreatedByMeService = resultsOfTestCreatedByMeService;
    }

    @TestAuthorshipCheck
    @GetMapping
    Map<String, Object> get(@PathVariable("test_id") Long testId) {
        return resultsOfTestCreatedByMeService.getTestResults(testId);
    }
}
