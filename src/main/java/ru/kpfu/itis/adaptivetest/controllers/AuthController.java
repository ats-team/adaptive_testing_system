package ru.kpfu.itis.adaptivetest.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestParam;
import ru.kpfu.itis.adaptivetest.models.User;
import ru.kpfu.itis.adaptivetest.security.role.Role;
import ru.kpfu.itis.adaptivetest.services.interfaces.AuthServiceInterface;

import javax.servlet.http.HttpServletRequest;

/**
 * @author Rustem Saitgareev
 * 11-602
 * 000
 */
@Controller
public class AuthController {
    @GetMapping("/login")
    public String login(@ModelAttribute("model") ModelMap model, Authentication authentication,
                        @RequestParam(required = false) String error) {
        if (authentication != null) {
            return "redirect:/";
        }
        model.addAttribute("error", error);
        return "login";
    }

    @GetMapping("/logout")
    public String logout(HttpServletRequest request, Authentication authentication) {
        if (authentication != null) {
            request.getSession().invalidate();
        }
        return "redirect:/login";
    }
}
