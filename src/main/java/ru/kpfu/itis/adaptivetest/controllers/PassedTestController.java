package ru.kpfu.itis.adaptivetest.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import ru.kpfu.itis.adaptivetest.exceptions.NotAccessException;
import ru.kpfu.itis.adaptivetest.exceptions.NotFoundException;
import ru.kpfu.itis.adaptivetest.models.User;
import ru.kpfu.itis.adaptivetest.security.details.UserDetailsImpl;
import ru.kpfu.itis.adaptivetest.services.interfaces.PassedTestServiceInterface;

import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Rustem Saitgareev
 * 11-602
 * 000
 */
@RestController
@RequestMapping("/internal_api/passed_tests")
public class PassedTestController {
    private PassedTestServiceInterface passedTestService;

    @Autowired
    public PassedTestController(PassedTestServiceInterface passedTestService) {
        this.passedTestService = passedTestService;
    }

    @GetMapping("/{id:[0-9]+}")
    public Map<String, Object> getPassedTestInfo(@PathVariable("id") Long id, HttpServletResponse resp) throws NotFoundException, NotAccessException {
        return passedTestService.getResultById(id);
    }

    @GetMapping("")
    public List<Map<String, Object>> getPassedTestList(@RequestParam(name = "name", required = false) String nameFilteringValue,
                                                       @RequestParam(name = "login", required = false) String loginFilteringValue,
                                                       @RequestParam(name = "fio", required = false) String fioFilteringValue,
                                                       @RequestParam(name = "pageNumber", required = false) Integer pageNumber,
                                                       @RequestParam(name = "pageSize", required = false) Integer pageSize) {
        return passedTestService.getPassedTestList(nameFilteringValue, loginFilteringValue, fioFilteringValue, pageNumber, pageSize);
    }
}
