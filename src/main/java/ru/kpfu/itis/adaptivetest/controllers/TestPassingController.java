package ru.kpfu.itis.adaptivetest.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import ru.kpfu.itis.adaptivetest.exceptions.AlreadyFinishedException;
import ru.kpfu.itis.adaptivetest.exceptions.AlreadyStartedException;
import ru.kpfu.itis.adaptivetest.exceptions.NotFoundException;
import ru.kpfu.itis.adaptivetest.services.interfaces.PassingTestService;

import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

@Controller
@RequestMapping("/internal_api/test")
public class TestPassingController {
    PassingTestService passingTestService;


    @Autowired
    public void setPassingTestService(PassingTestService passingTestService) {
        this.passingTestService = passingTestService;
    }

    @ResponseBody
    @PostMapping("/{testId:[0-9]+}/start")
    public Map<String, Object> startTest(@PathVariable("testId")Long testId, HttpServletResponse response) {

        try {
            Map<String, Object> result = passingTestService.startTest(testId);
            return result;
        } catch (NotFoundException e) {
            response.setStatus(400);
            response.setHeader("error_code", "not_found.test");
        } catch (AccessDeniedException e) {
            response.setStatus(403);
        } catch (AlreadyStartedException e) {
            response.setStatus(400);
            response.setHeader("error_code", "already_started");
        }
        return new HashMap<>();
    }

    @PostMapping("/finish")
    public Map<String, Object> finishTest(HttpServletResponse response) {
        try{
            return passingTestService.finishTest();
        }catch (AlreadyFinishedException e){
            response.setHeader("error_code", "already_finished");
            response.setStatus(400);
        } catch (AccessDeniedException e) {
            response.setStatus(403);
        }
        return new HashMap<>();
    }
}
