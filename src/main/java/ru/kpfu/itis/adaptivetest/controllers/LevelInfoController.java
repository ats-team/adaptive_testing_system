package ru.kpfu.itis.adaptivetest.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;
import ru.kpfu.itis.adaptivetest.exceptions.NotFoundException;
import ru.kpfu.itis.adaptivetest.models.User;
import ru.kpfu.itis.adaptivetest.security.details.UserDetailsImpl;
import ru.kpfu.itis.adaptivetest.services.interfaces.LevelServiceInterface;

import java.nio.file.AccessDeniedException;
import java.util.Map;

@RestController
@RequestMapping("internal_api/levels_creation_data")
public class LevelInfoController {
    private LevelServiceInterface levelService;

    @Autowired
    public void setLevelService(LevelServiceInterface levelService) {
        this.levelService = levelService;
    }

    @GetMapping("/{id:[0-9]+}")
    public Map<String, Object> getLevelInfo(Authentication authentication, @PathVariable Long id) throws Exception{
        System.out.println(id);
        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
        User user = userDetails.getUser();
        User authorOfTest = levelService.getAuthorOfTestByLevelId(id);
        if (authorOfTest.getId() == user.getId()) {
            return levelService.getLevelInfo(id);
        }
        else {
            throw new AccessDeniedException("403 forbidden");
        }
    }
}
