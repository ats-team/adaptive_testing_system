package ru.kpfu.itis.adaptivetest.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import ru.kpfu.itis.adaptivetest.exceptions.AlreadyFinishedException;
import ru.kpfu.itis.adaptivetest.exceptions.AlreadyPassedException;
import ru.kpfu.itis.adaptivetest.exceptions.NotFoundException;
import ru.kpfu.itis.adaptivetest.forms.AnswerForm;

import ru.kpfu.itis.adaptivetest.services.interfaces.PassingTestService;
import ru.kpfu.itis.adaptivetest.validators.AnswerFormValidator;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;

@Controller
@RequestMapping("/internal_api/questions")
public class PassingTestQuestionController {
    private AnswerFormValidator answerFormValidator;
    private PassingTestService passingTestService;

    @Autowired
    public void setAnswerFormValidator(AnswerFormValidator answerFormValidator) {
        this.answerFormValidator = answerFormValidator;
    }

    @Autowired
    public void setPassingTestService(PassingTestService passingTestService) {
        this.passingTestService = passingTestService;
    }

    @InitBinder("answer")
    public void initAdaptedQuestionFormValidator(WebDataBinder binder) {
        binder.addValidators(answerFormValidator);
    }


    @ResponseBody
    @PostMapping("/{id:[0-9]+}")
    public Map<String, Object> doAnswer(
            @Valid @ModelAttribute("answer") AnswerForm answerForm,
            BindingResult errors,
            @PathVariable("id") Long id,
            HttpServletResponse response) {
        if (errors.hasErrors()) {
            response.setHeader("error_code", errors.getAllErrors().get(0).getCode());
            response.setStatus(400);
        } else {
            try{
                Map<String, Object> result = passingTestService.processAnswer(answerForm);
                //for (String s: result.keySet()) {
                    //System.out.println(s + " " + result.get(s));
                //}
                return result;
            } catch (NotFoundException e) {
                response.setHeader("error_code", "not_found." + e.getaClass().getSimpleName());
                response.setStatus(400);
            } catch (AlreadyFinishedException e) {
                response.setStatus(400);
                response.setHeader("error_code", "already_finished");
            } catch (AlreadyPassedException e) {
                response.setStatus(400);
                response.setHeader("error_code", "already_passed");
            } catch (AccessDeniedException e) {
                response.setStatus(403);
            }
        }
        return new HashMap<>();
    }

    @ResponseBody
    @GetMapping("/{id:[0-9]+}")
    public Map<String, Object> getQuestion(
            @PathVariable("id") Long id, //id of question
            HttpServletResponse response){
        try{
            return passingTestService.getCurrentQuestion(id/*, passedTest*/);
        } catch (AlreadyPassedException e) {
            response.setStatus(400);
            response.setHeader("error_code", "already_passed");
        } catch (NotFoundException e) {
            response.setStatus(400);
            response.setHeader("error_code", "not_found." + e.getaClass().getSimpleName());
        } catch (AccessDeniedException e) {
            response.setStatus(403);
        }
        return new HashMap<>();
    }
}
