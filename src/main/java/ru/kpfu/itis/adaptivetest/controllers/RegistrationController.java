package ru.kpfu.itis.adaptivetest.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import ru.kpfu.itis.adaptivetest.forms.UserRegistrationForm;
import ru.kpfu.itis.adaptivetest.models.User;
import ru.kpfu.itis.adaptivetest.services.interfaces.RegistrationServiceInterface;
import ru.kpfu.itis.adaptivetest.validators.UserRegistrationFormValidator;

import javax.validation.Valid;

/**
 * @author Rustem Saitgareev
 * 11-602
 * 000
 */

@Controller
public class RegistrationController {
    private final RegistrationServiceInterface service;
    private final UserRegistrationFormValidator userRegistrationFormValidator;

    @Autowired
    public RegistrationController(RegistrationServiceInterface service, UserRegistrationFormValidator userRegistrationFormValidator) {
        this.service = service;
        this.userRegistrationFormValidator = userRegistrationFormValidator;
    }

    @InitBinder("user")
    public void initUserFormValidator(WebDataBinder binder) {
        binder.addValidators(userRegistrationFormValidator);
    }

    @PostMapping(value = "/registration")
    public String signUp(@Valid @ModelAttribute("user") UserRegistrationForm userRegistrationForm,
                         BindingResult errors, RedirectAttributes attributes) {
        if (errors.hasErrors()) {
            attributes.addFlashAttribute("error", errors.getAllErrors().get(0).getDefaultMessage());
            return "redirect:/registration";
        }
        service.register(userRegistrationForm);
        return "success_registration";
    }

    @GetMapping(value = "/registration")
    public String getSignUpPage() {
        return "registration";
    }
}
