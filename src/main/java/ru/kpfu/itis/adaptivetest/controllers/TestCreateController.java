package ru.kpfu.itis.adaptivetest.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import ru.kpfu.itis.adaptivetest.exceptions.BadQuestionsException;
import ru.kpfu.itis.adaptivetest.forms.TestCreationForm;
import ru.kpfu.itis.adaptivetest.models.Test;
import ru.kpfu.itis.adaptivetest.models.User;
import ru.kpfu.itis.adaptivetest.security.details.UserDetailsImpl;
import ru.kpfu.itis.adaptivetest.services.interfaces.TestServiceInterface;
import ru.kpfu.itis.adaptivetest.validators.TestCreationFormValidator;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/**
 * @author Rustem Saitgareev
 * 11-602
 * 000
 */

@RestController
@RequestMapping("/internal_api/tests")
public class TestCreateController {
    private TestServiceInterface testService;
    private final TestCreationFormValidator testCreationFormValidator;

    @Autowired
    public TestCreateController(TestServiceInterface testService, TestCreationFormValidator testCreationFormValidator) {
        this.testService = testService;
        this.testCreationFormValidator = testCreationFormValidator;
    }

    @InitBinder("test")
    public void initUserFormValidator(WebDataBinder binder) {
        binder.addValidators(testCreationFormValidator);
    }

    @PostMapping("/create")
    public Map<String, Object> create(@Valid @ModelAttribute("test") TestCreationForm form,
                       BindingResult errors, HttpServletResponse resp) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null) {
            if (errors.hasErrors()) {
                resp.setCharacterEncoding("utf-8");
                resp.setHeader("error_code", errors.getAllErrors().get(0).getCode());
                resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                return new HashMap<>();
            } else {
                //User user = authService.getUserByAuthentication(authentication);
                User user = ((UserDetailsImpl) authentication.getPrincipal()).getUser();
                return testService.create(form, user);
            }
        } else {
            resp.setStatus(401);
            return new HashMap<>();
        }
    }

    @GetMapping("/{id:[0-9]+}/edit")
    public Map<String, Object> edit(@PathVariable("id") Long id, HttpServletResponse resp) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null) {
            User user = ((UserDetailsImpl) authentication.getPrincipal()).getUser();
            Optional<Test> dbTest = testService.getTestById(id);
            if (dbTest.isPresent()) {
                Test test = dbTest.get();
                if (test.getAuthor().getId() == user.getId()) {
                    if (!test.getIsAvailable()) {
                        return testService.getTestInfoForEditPage(id);
                    } else {
                        resp.setStatus(406);
                        return new HashMap<>();
                    }
                } else {
                    resp.setStatus(403);
                    return new HashMap<>();
                }
            } else {
                resp.setStatus(404);
                return new HashMap<>();
            }
        } else {
            resp.setStatus(401);
            return new HashMap<>();
        }
    }

    @PostMapping("/{id:[0-9]+}/edit")
    public void edit(@Valid @ModelAttribute("test") TestCreationForm form, BindingResult errors,
                     @PathVariable("id") Long id, HttpServletResponse resp) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null) {
            User user = ((UserDetailsImpl) authentication.getPrincipal()).getUser();
            Optional<Test> dbTest = testService.getTestById(id);
            if (dbTest.isPresent()) {
                Test test = dbTest.get();
                if (test.getAuthor().getId() == user.getId()) {
                    if (errors.hasErrors()) {
                        resp.setCharacterEncoding("utf-8");
                        resp.setHeader("error_code", errors.getAllErrors().get(0).getCode());
                        resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                    } else {
                        if (!test.getIsAvailable()) {
                            testService.edit(test, form);
                        } else {
                            resp.setStatus(406);
                        }
                    }
                } else {
                    resp.setStatus(403);
                }
            } else {
                resp.setStatus(404);
            }
        } else {
            resp.setStatus(401);
        }
    }

    @PostMapping("/{id:[0-9]+}/make_available")
    public void makeAvailable(@PathVariable("id") Long id, HttpServletResponse resp) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null) {
            User user = ((UserDetailsImpl) authentication.getPrincipal()).getUser();
            Optional<Test> dbTest = testService.getTestById(id);
            if (dbTest.isPresent()) {
                Test test = dbTest.get();
                if (test.getAuthor().getId() == user.getId()) {
                        if (!test.getIsAvailable()) {
                            try {
                                testService.makeAvailable(test);
                            } catch (BadQuestionsException ex) {
                                resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                            }
                        } else {
                            resp.setStatus(406);
                        }
                } else {
                    resp.setStatus(403);
                }
            } else {
                resp.setStatus(404);
            }
        } else {
            resp.setStatus(401);
        }
    }
}
