package ru.kpfu.itis.adaptivetest.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import ru.kpfu.itis.adaptivetest.forms.UserForm;
import ru.kpfu.itis.adaptivetest.models.User;
import ru.kpfu.itis.adaptivetest.security.details.UserDetailsImpl;
import ru.kpfu.itis.adaptivetest.services.interfaces.ProfileServiceInterface;
import ru.kpfu.itis.adaptivetest.validators.ProfileFormValidator;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.Map;

/**
 * @author Bulat Giniyatullin
 * 03 Апрель 2018
 */

@RestController
@RequestMapping("/internal_api/profile")
public class ProfileController {
    private ProfileServiceInterface profileService;
    private ProfileFormValidator profileFormValidator;

    @Autowired
    public ProfileController(ProfileServiceInterface profileService, ProfileFormValidator profileFormValidator) {
        this.profileService = profileService;
        this.profileFormValidator = profileFormValidator;
    }

    @InitBinder
    public void initProfileFormValidator(WebDataBinder binder) {
        binder.addValidators(profileFormValidator);
    }

    @GetMapping({
            "/{id:[0-9]+}",
            "/{login:.+}",
            ""})
    public Map<String, Object> getUserProfile(@PathVariable(required = false) Long id,
                                              @PathVariable(required = false) String login) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        User user = ((UserDetailsImpl) authentication.getPrincipal()).getUser();

        if (id != null && id == user.getId() || user.getLogin().equals(login) || id == null && login == null) {
            return profileService.getUserByIdOrLogin(id, login);
        } else {
            throw new AccessDeniedException("403 Forbidden");
        }
    }

    @PostMapping("/{id:[0-9+]}")
    public void updateUser(@PathVariable Long id,
                             @Valid @ModelAttribute UserForm userForm,
                             BindingResult errors,
                             HttpServletResponse response) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        User user = ((UserDetailsImpl) authentication.getPrincipal()).getUser();

        if (id != null && id == user.getId()) {
            if (errors.hasErrors()) {
                response.setCharacterEncoding("utf-8");
                response.setHeader("error_code", errors.getAllErrors().get(0).getCode());
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            } else {
                profileService.updateUserById(userForm);
            }
        } else {
            throw new AccessDeniedException("403 Forbidden");
        }
    }
}