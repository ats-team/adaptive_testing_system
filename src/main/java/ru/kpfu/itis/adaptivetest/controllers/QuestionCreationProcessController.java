package ru.kpfu.itis.adaptivetest.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.kpfu.itis.adaptivetest.services.implementations.QuestionServiceImpl;

import java.util.Map;

/**
 * @author Bulat Giniyatullin
 * 11 April 2018
 */

@RestController
@RequestMapping("/internal_api")
public class QuestionCreationProcessController {

    private final QuestionServiceImpl questionService;

    @Autowired
    public QuestionCreationProcessController(QuestionServiceImpl questionService) {
        this.questionService = questionService;
    }

    @GetMapping("/question_creation_data/{question_id}")
    public Map<String, Object> getQuestionInformation(
            @PathVariable("question_id") Long questionId) {
        return questionService.getQuestionWithLevelsById(questionId);
    }
}
