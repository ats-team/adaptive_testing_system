package ru.kpfu.itis.adaptivetest.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.kpfu.itis.adaptivetest.aspects.annotations.TestAuthorshipCheck;
import ru.kpfu.itis.adaptivetest.exceptions.NotFoundException;
import ru.kpfu.itis.adaptivetest.forms.QuestionCreationForm;
import ru.kpfu.itis.adaptivetest.models.Test;
import ru.kpfu.itis.adaptivetest.services.implementations.QuestionServiceImpl;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

/**
 * @author Bulat Giniyatullin
 * 11 Апрель 2018
         */

@RestController
@RequestMapping("/internal_api/questions")
public class QuestionController {

    private QuestionServiceImpl questionService;

    @Autowired
    public QuestionController(QuestionServiceImpl questionService) {
        this.questionService = questionService;
    }

    @TestAuthorshipCheck
    @PostMapping("/create")
    public void create(@Valid @ModelAttribute("question") QuestionCreationForm questionCreationForm,
                       HttpServletResponse resp, BindingResult errors) {
        try {
            questionService.createQuestion(questionCreationForm);
            if (errors.hasErrors()) {
                resp.setCharacterEncoding("utf-8");
                resp.setHeader("error_code", errors.getAllErrors().get(0).getCode());
                resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            }
        } catch (NotFoundException e) {
            if (e.getaClass().equals(Test.class)) {
                resp.setHeader("error_code", "not_found.test");
                resp.setStatus(HttpServletResponse.SC_NOT_FOUND);
            }
        }
    }
}
