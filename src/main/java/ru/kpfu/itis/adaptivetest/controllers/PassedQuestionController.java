package ru.kpfu.itis.adaptivetest.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.kpfu.itis.adaptivetest.services.interfaces.PassedQuestionServiceInterface;

import java.util.Map;

/**
 * @author Bulat Giniyatullin
 * 24 April 2018
 */

@RestController
@RequestMapping("/internal_api/passed_questions")
public class PassedQuestionController {
    private final PassedQuestionServiceInterface passedQuestionService;

    @Autowired
    public PassedQuestionController(PassedQuestionServiceInterface passedQuestionService) {
        this.passedQuestionService = passedQuestionService;
    }

    @GetMapping("{id}")
    public Map<String, Object> get(@PathVariable("id") Long questionId) {
        return passedQuestionService.getPassedQuestionById(questionId);
    }
}
