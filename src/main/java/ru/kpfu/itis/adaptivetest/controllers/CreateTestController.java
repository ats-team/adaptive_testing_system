package ru.kpfu.itis.adaptivetest.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import ru.kpfu.itis.adaptivetest.forms.AdaptedQuestionForm;
import ru.kpfu.itis.adaptivetest.models.User;
import ru.kpfu.itis.adaptivetest.security.details.UserDetailsImpl;
import ru.kpfu.itis.adaptivetest.services.interfaces.TestServiceInterface;
import ru.kpfu.itis.adaptivetest.validators.AdaptedQuestionValidator;

import javax.persistence.EntityNotFoundException;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.nio.file.AccessDeniedException;
import java.util.Map;

@Controller
@RequestMapping("/internal_api/tests/create")
public class CreateTestController {

    private TestServiceInterface testService;

    @Autowired
    public CreateTestController(TestServiceInterface testService) {
        this.testService = testService;
    }


    @GetMapping("/question/{id:[0-9]+}/levels/{level:[0-9]+}/testForm")
    public String adaptedQuestionForm(@PathVariable int level, @PathVariable long id, ModelMap map) {
        map.put("level", level);
        map.put("number", id);
        return "adaptedQuestionForm";
    }

}
