package ru.kpfu.itis.adaptivetest.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author Bulat Giniyatullin
 * 13 Март 2018
 */

@Controller
public class IndexController {
    @RequestMapping(value = "/")
    public String emptyPath() {
        return "redirect:/ats";
    }

    @RequestMapping(value = "/ats/**", produces = "text/html")
    public String index() {
        return "index";
    }
}
