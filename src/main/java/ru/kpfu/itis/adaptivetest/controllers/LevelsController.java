package ru.kpfu.itis.adaptivetest.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import ru.kpfu.itis.adaptivetest.exceptions.NotFoundException;
import ru.kpfu.itis.adaptivetest.forms.AdaptedQuestionForm;
import ru.kpfu.itis.adaptivetest.models.Question;
import ru.kpfu.itis.adaptivetest.models.User;
import ru.kpfu.itis.adaptivetest.security.details.UserDetailsImpl;
import ru.kpfu.itis.adaptivetest.services.implementations.LevelServiceImpl;
import ru.kpfu.itis.adaptivetest.services.implementations.QuestionServiceImpl;
import ru.kpfu.itis.adaptivetest.validators.AdaptedQuestionValidator;

import javax.persistence.EntityNotFoundException;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

/**
 * @author Bulat Giniyatullin
 * 12 April 2018
 */

@RestController
@RequestMapping("/internal_api/levels")
public class LevelsController {
    private final LevelServiceImpl levelService;
    private QuestionServiceImpl questionService;
    private AdaptedQuestionValidator adaptedQuestionValidator;

    @Autowired
    public LevelsController(LevelServiceImpl levelService, AdaptedQuestionValidator adaptedQuestionValidator) {
        this.levelService = levelService;
        this.adaptedQuestionValidator = adaptedQuestionValidator;
    }

    //@Autowired
    public void setAdaptedQuestionValidator(AdaptedQuestionValidator adaptedQuestionValidator) {
        this.adaptedQuestionValidator = adaptedQuestionValidator;
    }

    @Autowired
    public void setQuestionService(QuestionServiceImpl questionService) {
        this.questionService = questionService;
    }

    @DeleteMapping
    public void delete(@RequestParam("level_id_to_delete") Long levelId) {
        levelService.deleteLevelById(levelId);
    }

    @InitBinder("adaptedQuestion")
    public void initAdaptedQuestionFormValidator(WebDataBinder binder) {
        binder.addValidators(adaptedQuestionValidator);
    }


    @PostMapping()
    public void createAdaptedQuestion(
            @Valid @ModelAttribute("adaptedQuestion") AdaptedQuestionForm question,
            BindingResult errors,
            HttpServletResponse response) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        System.out.println(errors.hasErrors());
        if (errors.hasErrors()) {
            response.setCharacterEncoding("utf-8");
            response.setHeader("error_code", errors.getAllErrors().get(0).getCode());
            response.setStatus(400);
        } else {
            try{
                UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
                User user = userDetails.getUser();
                User authorOfTest = questionService.getAuthorOfTestByQuestionId(question.getQuestion_id());
                if (authorOfTest.getId() == user.getId()) {
                    levelService.createLevel(question);
                }
                else {
                    response.setStatus(403);
                }
            } catch (Exception e) {
                e.printStackTrace();
                response.setStatus(500);
            }

        }
    }
}
