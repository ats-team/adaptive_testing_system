package ru.kpfu.itis.adaptivetest.exceptions;

/**
 * @author Bulat Giniyatullin
 * 11 April 2018
 */

public class NotFoundException extends Exception {
    private Class aClass;

    public NotFoundException(Class aClass) {
        this.aClass = aClass;
    }

    public Class getaClass() {
        return aClass;
    }
}
