package ru.kpfu.itis.adaptivetest.specifications;

import org.springframework.data.jpa.domain.Specification;
import ru.kpfu.itis.adaptivetest.models.Test;
import ru.kpfu.itis.adaptivetest.models.User;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 * @author Rustem Saitgareev
 * 11-602
 * 000
 */
public class MyTestSpecifications {
    public static Specification<Test> isUserAuthor(User user) {
        return (Root<Test> test, CriteriaQuery<?> query, CriteriaBuilder builder) ->
                builder.equal(test.<User> get("author").<Integer> get("id"), user.getId());
    }
}
