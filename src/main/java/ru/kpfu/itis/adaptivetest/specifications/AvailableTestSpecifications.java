package ru.kpfu.itis.adaptivetest.specifications;

import org.springframework.data.jpa.domain.Specification;
import ru.kpfu.itis.adaptivetest.models.AvailableTest;
import ru.kpfu.itis.adaptivetest.models.Test;
import ru.kpfu.itis.adaptivetest.models.User;
import static ru.kpfu.itis.adaptivetest.specifications.TestSpecifications.*;
import javax.persistence.criteria.*;

public class AvailableTestSpecifications {
    public static Specification<AvailableTest> isToUser(User user) {
        return (Root<AvailableTest> root, CriteriaQuery<?> query, CriteriaBuilder builder) ->
                builder.equal(root.<Long> get("user"), user);
    }

    public static Specification<AvailableTest> isAuthorLoginSatisfiesFilterAT(String loginFilter) {
        return new Specification<AvailableTest>() {
            @Override
            public Predicate toPredicate(Root<AvailableTest> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
                return getFilteringValueIsNullOrContainedEntirelyPredicate(
                        root.get("test").get("author").<String> get("login"),
                        loginFilter,
                        criteriaBuilder
                );
            }
        };
    }

    public static Specification<AvailableTest> isNameSatisfiesFilterAT(String nameFilter) {
        return (Root<AvailableTest> root, CriteriaQuery<?> query, CriteriaBuilder builder) ->
            getFilteringValueIsNullOrContainedInDifferentCombinationsPredicate(
                    root.get("test").<String> get("name"),
                    nameFilter,
                    builder);


    }

    public static Specification<AvailableTest> isAuthorFioSatisfiesFilterAT(String fioFilter) {
        return (Root<AvailableTest> root, CriteriaQuery<?> query, CriteriaBuilder builder) ->
            getFilteringValueIsNullOrContainedInDifferentCombinationsPredicate(
                    root.get("test").get("author").<String> get("fio"),
                    fioFilter,
                    builder);


    }
}
