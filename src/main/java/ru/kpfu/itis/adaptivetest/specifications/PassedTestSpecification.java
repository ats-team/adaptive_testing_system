package ru.kpfu.itis.adaptivetest.specifications;

import org.springframework.data.jpa.domain.Specification;
import ru.kpfu.itis.adaptivetest.models.PassedTest;
import ru.kpfu.itis.adaptivetest.models.Test;
import ru.kpfu.itis.adaptivetest.models.User;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import static ru.kpfu.itis.adaptivetest.specifications.TestSpecifications.getFilteringValueIsNullOrContainedEntirelyPredicate;
import static ru.kpfu.itis.adaptivetest.specifications.TestSpecifications.getFilteringValueIsNullOrContainedInDifferentCombinationsPredicate;

/**
 * @author Rustem Saitgareev
 * 11-602
 * 000
 */
public class PassedTestSpecification {
    public static Specification<PassedTest> isToUser(User user) {
        return (Root<PassedTest> root, CriteriaQuery<?> query, CriteriaBuilder builder) ->
                builder.equal(root.<Long> get("user"), user);
    }

    public static Specification<PassedTest> isNameSatisfiesFilter(String nameFilter) {
        return (Root<PassedTest> root, CriteriaQuery<?> query, CriteriaBuilder builder) ->
                getFilteringValueIsNullOrContainedInDifferentCombinationsPredicate(
                        root.get("test").<String> get("name"),
                        nameFilter,
                        builder);
    }

    public static Specification<PassedTest> isAuthorFioSatisfiesFilter(String authorFioFilter) {
        return (Root<PassedTest> root, CriteriaQuery<?> query, CriteriaBuilder builder) ->
                getFilteringValueIsNullOrContainedInDifferentCombinationsPredicate(
                        root.get("test").get("author").<String> get("fio"),
                        authorFioFilter,
                        builder);
    }

    public static Specification<PassedTest> isAuthorLoginSatisfiesFilter(String authorLoginFilter) {
        return (Root<PassedTest> root, CriteriaQuery<?> query, CriteriaBuilder builder) ->
                getFilteringValueIsNullOrContainedEntirelyPredicate(
                        root.get("test").get("author").<String> get("login"),
                        authorLoginFilter,
                        builder
                );
    }
}
