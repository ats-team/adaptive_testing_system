package ru.kpfu.itis.adaptivetest.specifications;

import org.springframework.data.jpa.domain.Specification;
import ru.kpfu.itis.adaptivetest.models.Test;

import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

/**
 * @author Bulat Giniyatullin
 * 21 Март 2018
 */

public class TestSpecifications {
    public static Specification<Test> isAvailable() {
        return (Root<Test> test, CriteriaQuery<?> query, CriteriaBuilder builder) ->
                builder.isTrue(test.<Boolean> get("isAvailable"));
    }

    public static Specification<Test> isPublic() {
        return (Root<Test> test, CriteriaQuery<?> query, CriteriaBuilder builder) ->
                builder.isFalse(test.<Boolean> get("isPrivate"));
    }

    public static Specification<Test> isNameSatisfiesFilter(String nameFilter) {
        return (Root<Test> test, CriteriaQuery<?> query, CriteriaBuilder builder) ->
                getFilteringValueIsNullOrContainedInDifferentCombinationsPredicate(
                        test.<String> get("name"),
                        nameFilter,
                        builder);
    }

    public static Specification<Test> isAuthorFioSatisfiesFilter(String authorFioFilter) {
        return (Root<Test> test, CriteriaQuery<?> query, CriteriaBuilder builder) ->
                getFilteringValueIsNullOrContainedInDifferentCombinationsPredicate(
                        test.get("author").<String> get("fio"),
                        authorFioFilter,
                        builder);
    }

    public static Specification<Test> isAuthorLoginSatisfiesFilter(String authorLoginFilter) {
        return (Root<Test> test, CriteriaQuery<?> query, CriteriaBuilder builder) ->
                getFilteringValueIsNullOrContainedEntirelyPredicate(
                        test.get("author").<String> get("login"),
                        authorLoginFilter,
                        builder
                );
    }

    public static Predicate getFilteringValueIsNullOrContainedInDifferentCombinationsPredicate(Expression<String> verifying,
                                                                                                String filter,
                                                                                                CriteriaBuilder builder) {
        List<Predicate> predicates = new ArrayList<>();
        if (filter == null || Pattern.compile("^\\s*$").matcher(filter).matches()) {
            // always return true
            return builder.and();
        }
        for (String part : filter.split(" ")) {
            predicates.add(builder.like(builder.lower(verifying), "%" + part.toLowerCase() + "%"));
        }
        return builder.and(predicates.toArray(new Predicate[predicates.size()]));
    }

    public static Predicate getFilteringValueIsNullOrContainedEntirelyPredicate(Expression<String> verifying,
                                                                               String filter,
                                                                               CriteriaBuilder builder) {
        if (filter == null || Pattern.compile("^\\s*$").matcher(filter).matches()) {
            // always return true
            return builder.and();
        } else {
            return builder.like(builder.lower(verifying), filter.toLowerCase());
        }
    }
}
