package ru.kpfu.itis.adaptivetest.forms;

import lombok.Data;

/**
 * @author Rustem Saitgareev
 * 11-602
 * 000
 */
@Data
public class UserRegistrationForm {
    private String login;
    private String email;
    private String fio;
    private String password;
    private String repeatPassword;
}
