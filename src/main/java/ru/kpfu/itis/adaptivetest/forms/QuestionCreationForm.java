package ru.kpfu.itis.adaptivetest.forms;

import lombok.*;

/**
 * @author Rustem Saitgareev
 * 11-602
 * 000
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class QuestionCreationForm {
    private long testId;
    private String name;
}
