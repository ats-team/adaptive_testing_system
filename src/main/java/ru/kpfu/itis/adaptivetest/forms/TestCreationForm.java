package ru.kpfu.itis.adaptivetest.forms;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Rustem Saitgareev
 * 11-602
 * 000
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TestCreationForm {
    private String name;
    private String shortDescription;
    private String detailedDescription;
    private int maxDuration;
    private String isPrivate;
    private String isQuestionsDetailsAvailable;
}
