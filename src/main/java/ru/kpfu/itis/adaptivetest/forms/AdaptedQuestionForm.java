package ru.kpfu.itis.adaptivetest.forms;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class AdaptedQuestionForm {
    private long question_id;
    private String text;
    private String right_answer;
    private String photo;
    private String file;
    private int score;
}
