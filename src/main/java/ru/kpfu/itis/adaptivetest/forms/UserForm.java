package ru.kpfu.itis.adaptivetest.forms;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Bulat Giniyatullin
 * 18 May 2018
 */

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserForm {
    private Long id;
    private String email;
    private String login;
    private String fio;
    private String password;
    private String password_repeat;
}
