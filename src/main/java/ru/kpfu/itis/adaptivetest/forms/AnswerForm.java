package ru.kpfu.itis.adaptivetest.forms;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AnswerForm {
    private long level_id;
    private String answer;
}
