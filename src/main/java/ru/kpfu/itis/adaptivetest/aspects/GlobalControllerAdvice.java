package ru.kpfu.itis.adaptivetest.aspects;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import ru.kpfu.itis.adaptivetest.exceptions.NotAccessException;
import ru.kpfu.itis.adaptivetest.exceptions.NotAuthenticatedException;
import ru.kpfu.itis.adaptivetest.exceptions.NotFoundException;

import javax.servlet.http.HttpServletResponse;

/**
 * @author Rustem Saitgareev
 * 11-602
 * 000
 */
@ControllerAdvice(basePackages = {"ru.kpfu.itis.adaptivetest.controllers"})
public class GlobalControllerAdvice {
    @ExceptionHandler(NotFoundException.class)
    public void notFoundExceptionHandler(HttpServletResponse resp) {
        resp.setStatus(HttpServletResponse.SC_NOT_FOUND);
        resp.setContentType("application/json;charset=UTF-8");
    }

    @ExceptionHandler(NotAccessException.class)
    public void notAccessExceptionHandler(HttpServletResponse resp) {
        resp.setStatus(HttpServletResponse.SC_FORBIDDEN);
        resp.setContentType("application/json;charset=UTF-8");
    }

    @ExceptionHandler(NotAuthenticatedException.class)
    public void notAuthenticatedExceptionHandler(HttpServletResponse resp) {
        resp.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        resp.setContentType("application/json;charset=UTF-8");
    }
}
