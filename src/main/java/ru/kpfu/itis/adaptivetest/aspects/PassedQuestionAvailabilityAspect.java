package ru.kpfu.itis.adaptivetest.aspects;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import ru.kpfu.itis.adaptivetest.models.PassedQuestion;
import ru.kpfu.itis.adaptivetest.models.Test;
import ru.kpfu.itis.adaptivetest.models.User;
import ru.kpfu.itis.adaptivetest.repositories.PassedQuestionRepository;
import ru.kpfu.itis.adaptivetest.security.details.UserDetailsImpl;

import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Optional;

/**
 * @author Bulat Giniyatullin
 * 24 April 2018
 */

@Aspect
@Component
public class PassedQuestionAvailabilityAspect {
    private final PassedQuestionRepository passedQuestionRepository;

    @Autowired
    public PassedQuestionAvailabilityAspect(PassedQuestionRepository passedQuestionRepository) {
        this.passedQuestionRepository = passedQuestionRepository;
    }

    @Pointcut("execution(* ru.kpfu.itis.adaptivetest.controllers.PassedQuestionController.get(Long))")
    private void passedQuestionControllersGetMethod() {}

    @Around("passedQuestionControllersGetMethod() && args(questionId)")
    public Object checkAvailability(ProceedingJoinPoint proceedingJoinPoint, Long questionId) throws Throwable {
        Optional<PassedQuestion> optionalPassedQuestion = passedQuestionRepository.findById(questionId);
        if (optionalPassedQuestion.isPresent()) {
            User currentUser = ((UserDetailsImpl) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUser();
            PassedQuestion passedQuestion = optionalPassedQuestion.get();
            boolean isAuthor = passedQuestion.getPassedTest().getTest().getAuthor().getId() == currentUser.getId();
            boolean isUserWhichPassedTest = passedQuestion.getPassedTest().getUser().getId() == currentUser.getId();
            boolean isQuestionsDetailsAvailable = passedQuestion.getPassedTest().getTest().getIsQuestionsDetailsAvailable();
            if (isAuthor || (isUserWhichPassedTest && isQuestionsDetailsAvailable)) {
                return proceedingJoinPoint.proceed();
            } else {
                HttpServletResponse response = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getResponse();
                response.setStatus(HttpServletResponse.SC_FORBIDDEN);
                return null;
            }
        } else {
            HttpServletResponse response = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getResponse();
            response.setStatus(HttpServletResponse.SC_NOT_FOUND);
            return null;
        }
    }
}