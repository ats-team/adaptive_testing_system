package ru.kpfu.itis.adaptivetest.aspects;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import ru.kpfu.itis.adaptivetest.models.Test;
import ru.kpfu.itis.adaptivetest.models.User;
import ru.kpfu.itis.adaptivetest.repositories.TestRepository;
import ru.kpfu.itis.adaptivetest.security.details.UserDetailsImpl;

import javax.servlet.http.HttpServletResponse;
import java.util.Optional;

/**
 * @author Bulat Giniyatullin
 * 18 April 2018
 */

@Aspect
@Component
public class TestAuthorshipCheckingAspect {
    private final TestRepository testRepository;

    @Autowired
    public TestAuthorshipCheckingAspect(TestRepository testRepository) {
        this.testRepository = testRepository;
    }

    @Pointcut("@annotation(ru.kpfu.itis.adaptivetest.aspects.annotations.TestAuthorshipCheck)")
    public void annotatedForAuthorshipChecking() {}

    @Pointcut("execution(* *.*(..))")
    public void method() {}

    /**
     * Аспект проверяет, является ли пользователь, отправивший запрос автором теста,
     * id которого передается в параметры аннотированного метода(контроллера)
     *
     * Вызывает сам метод, если проверка успешно пройдена,
     *  Возвращает response с кодом 403 Forbidden, если проверка не пролйдена
     *  Возвращает response с кодом 404 Not Found, если тест, авторство которого проверяется, не найден в базе
     */
    @Around("annotatedForAuthorshipChecking() && method() && args(testId, ..)")
    public Object checkAuthorship(ProceedingJoinPoint proceedingJoinPoint, Long testId) throws Throwable {
        Optional<Test> optionalTest = testRepository.findById(testId);
        User user = ((UserDetailsImpl)SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUser();
        if (optionalTest.isPresent()) {
            if (optionalTest.get().getAuthor().getId() == user.getId()) {
                return proceedingJoinPoint.proceed();
            } else {
                HttpServletResponse response = ((ServletRequestAttributes) RequestContextHolder
                        .getRequestAttributes())
                        .getResponse();
                response.setStatus(HttpServletResponse.SC_FORBIDDEN);
                return null;
            }
        } else {
            HttpServletResponse response = ((ServletRequestAttributes) RequestContextHolder
                    .getRequestAttributes())
                    .getResponse();
            response.setHeader("error_code", "not_found.test");
            response.setStatus(HttpServletResponse.SC_NOT_FOUND);
            return null;
        }
    }
}
