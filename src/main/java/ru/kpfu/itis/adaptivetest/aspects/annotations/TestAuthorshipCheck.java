package ru.kpfu.itis.adaptivetest.aspects.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Target;

/**
 * @author Bulat Giniyatullin
 * 18 April 2018
 *
 * annotated method should contain 'testId' parameter
 */
@Target(ElementType.METHOD)
public @interface TestAuthorshipCheck {
}
