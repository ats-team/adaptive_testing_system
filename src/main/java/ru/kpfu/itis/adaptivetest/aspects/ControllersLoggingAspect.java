package ru.kpfu.itis.adaptivetest.aspects;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import ru.kpfu.itis.adaptivetest.FileLogger;

import javax.servlet.http.HttpServletRequest;

/**
 * @author Bulat Giniyatullin
 * 13 Март 2018
 */

@Aspect
@Component
public class ControllersLoggingAspect {
    private FileLogger logger;

    @Autowired
    public ControllersLoggingAspect(FileLogger logger) {
        this.logger = logger;
    }

    @Pointcut("within(@org.springframework.stereotype.Controller *)")
    public void controller() {}

    @Pointcut("within(@org.springframework.web.bind.annotation.RestController *)")
    public void restController() {}

    @Pointcut("execution(* *.*(..))")
    public void method() {}

    @Around("(controller() || restController()) && method()")
    public Object doLogging(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder
                .currentRequestAttributes())
                .getRequest();

        Object value = proceedingJoinPoint.proceed();

        logger.info(String.format("%s %s from %s",
                request.getMethod(),
                request.getRequestURL(),
                request.getRemoteAddr()));

        return value;
    }
}
