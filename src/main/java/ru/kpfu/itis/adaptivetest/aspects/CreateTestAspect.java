package ru.kpfu.itis.adaptivetest.aspects;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;
import org.springframework.validation.BindingResult;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import ru.kpfu.itis.adaptivetest.models.Test;
import ru.kpfu.itis.adaptivetest.models.User;
import ru.kpfu.itis.adaptivetest.services.interfaces.AuthServiceInterface;
import ru.kpfu.itis.adaptivetest.services.interfaces.TestServiceInterface;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Optional;

/**
 * @author Rustem Saitgareev
 * 11-602
 * 000
 */

@Aspect
@Component
public class CreateTestAspect {
    private TestServiceInterface testService;
    private final AuthServiceInterface authService;

    @Autowired
    public CreateTestAspect(TestServiceInterface testService, AuthServiceInterface authService) {
        this.testService = testService;
        this.authService = authService;
    }


    /*@Around("execution(void ru.kpfu.itis.adaptivetest.controllers.TestCreateController.edit(..))")
    public Object doLogging(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
        Authentication authentication = (Authentication) proceedingJoinPoint.getArgs()[0];
        BindingResult errors = (BindingResult) proceedingJoinPoint.getArgs()[2];
        Long id = (Long) proceedingJoinPoint.getArgs()[3];
        HttpServletResponse resp = (HttpServletResponse) proceedingJoinPoint.getArgs()[4];
        RedirectAttributes attributes = (RedirectAttributes) proceedingJoinPoint.getArgs()[5];
        // TODO рассмотреть вариант, когда не доходит до proceed()
        if (authentication != null) {
            User user = authService.getUserByAuthentication(authentication);
            Optional<Test> dbTest = testService.getTestById(id);
            if (dbTest.isPresent()) {
                if (dbTest.get().getAuthor().getId() == user.getId()) {
                    if (errors.hasErrors()) {
                        attributes.addFlashAttribute("error", errors.getAllErrors().get(0).getDefaultMessage());
                    }
                    proceedingJoinPoint.proceed();
                } else {
                    throw new AccessDeniedException("403 Forbidden");
                }
            } else {
                resp.setStatus(404);
            }
        } else {
            resp.setStatus(401);
        }
        return null;
    }*/
}
