package ru.kpfu.itis.adaptivetest.aspects;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.context.annotation.Profile;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import ru.kpfu.itis.adaptivetest.exceptions.NotAuthenticatedException;

/**
 * @author Rustem Saitgareev
 * 11-602
 * 000
 */

@Aspect
@Component
public class AuthenticationCheckingAspect {
    @Around("execution(* ru.kpfu.itis.adaptivetest.controllers.PassedTestController.getPassedTestInfo(..))")
    public Object throwExceptionIfNotAuthenticated(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
        System.out.println("==========================================================");
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null) {
            return proceedingJoinPoint.proceed();
        }
        throw new NotAuthenticatedException();
    }
}
