package ru.kpfu.itis.adaptivetest.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;
import ru.kpfu.itis.adaptivetest.models.PassedTest;
import ru.kpfu.itis.adaptivetest.models.Test;
import ru.kpfu.itis.adaptivetest.models.User;

import java.util.Optional;

@Repository
public interface PassedTestRepository extends JpaRepository<PassedTest, Long>, JpaSpecificationExecutor<PassedTest> {
    Optional<PassedTest> findByUserAndAndTest(User user, Test test);
}
