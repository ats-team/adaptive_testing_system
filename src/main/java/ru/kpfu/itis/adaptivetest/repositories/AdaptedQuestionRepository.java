package ru.kpfu.itis.adaptivetest.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.kpfu.itis.adaptivetest.models.AdaptedQuestion;
import ru.kpfu.itis.adaptivetest.models.Question;

import java.util.List;
/**
 * @author Bulat Giniyatullin
 * 12 April 2018
 */
@Repository
public interface AdaptedQuestionRepository extends JpaRepository<AdaptedQuestion, Long>{
    List<AdaptedQuestion> findAllByQuestionIdAndLevel(Long id, Integer level);
    void deleteAdaptedQuestionById(Long id);
    int countAdaptedQuestionByQuestionId(Long id);
}
