package ru.kpfu.itis.adaptivetest.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;
import ru.kpfu.itis.adaptivetest.models.Test;

import java.util.List;

/**
 * @author Rustem Saitgareev
 * 11-602
 * 000
 */

@Repository
public interface TestRepository extends JpaRepository<Test, Long>, JpaSpecificationExecutor<Test> {
}
