package ru.kpfu.itis.adaptivetest.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.kpfu.itis.adaptivetest.models.PassedTest;
import ru.kpfu.itis.adaptivetest.models.Question;
import ru.kpfu.itis.adaptivetest.models.Test;

import java.util.List;

@Repository
public interface QuestionRepository extends JpaRepository<Question, Long> {
    int countAllByTest(Test test);
    @Query("select q from Question q where q.id not in " +
            "(select pq.adaptedQuestion.question.id from PassedQuestion pq where pq.passedTest=:passedTest)" +
            " and q.id <> :currentQuestionId and q.test = :test")
    List<Question> getNonPassedQuestions(@Param("passedTest") PassedTest passedTest, @Param("currentQuestionId") Long currentQuestionId, @Param("test") Test test);
}
