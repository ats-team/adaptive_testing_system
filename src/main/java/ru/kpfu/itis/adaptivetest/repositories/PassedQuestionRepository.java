package ru.kpfu.itis.adaptivetest.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.kpfu.itis.adaptivetest.models.AdaptedQuestion;
import ru.kpfu.itis.adaptivetest.models.PassedQuestion;
import ru.kpfu.itis.adaptivetest.models.PassedTest;
import ru.kpfu.itis.adaptivetest.models.Question;

import java.util.List;
import java.util.Optional;

@Repository
public interface PassedQuestionRepository extends JpaRepository<PassedQuestion, Long>{
    int countAllByPassedTest(PassedTest passedTest);
    Optional<PassedQuestion> findByPassedTestAndAdaptedQuestion(PassedTest passedTest, AdaptedQuestion adaptedQuestion);
    Optional<PassedQuestion> findByPassedTestAndAdaptedQuestion_Question(PassedTest passedTest, Question question);
    @Query("select pq, max(aq1.score) from PassedQuestion pq" +
            " join pq.adaptedQuestion aq" +
            " join aq.question q" +
            " join q.adaptedQuestions aq1" +
            " where pq.passedTest.id=:#{#passedTest.getId()}" +
            " group by pq.id")
    List<Object[]> findPassedQuestionWithMaxScoreByPassedTest(@Param("passedTest") PassedTest passedTest);
}
