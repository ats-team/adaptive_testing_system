package ru.kpfu.itis.adaptivetest.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.kpfu.itis.adaptivetest.models.User;

import java.util.Optional;

/**
 * @author Rustem Saitgareev
 * 11-602
 * 000
 */
public interface UserRepository extends JpaRepository<User, Long> {
    Optional<User> findOneByLogin(String login);
    Optional<User> findOneByEmail(String email);
}
