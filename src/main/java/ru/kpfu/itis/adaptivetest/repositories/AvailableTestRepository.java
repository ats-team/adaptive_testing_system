package ru.kpfu.itis.adaptivetest.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import ru.kpfu.itis.adaptivetest.models.AvailableTest;
import ru.kpfu.itis.adaptivetest.models.Test;
import ru.kpfu.itis.adaptivetest.models.User;

import java.util.List;
import java.util.Optional;

public interface AvailableTestRepository extends JpaRepository<AvailableTest, Long>, JpaSpecificationExecutor<AvailableTest>{
    List<AvailableTest> findByUser(User user);

    List<AvailableTest> findAllByUserAndTest(User user, Test test);

    void deleteAvailableTestByTestIdAndUserLogin(Long testId, String userLogin);
    Optional<AvailableTest> findOneByTestIdAndUser(Long testId, User user);
    Optional<AvailableTest> findOneByTestIdAndUserLogin(Long testId, String userLogin);

}
