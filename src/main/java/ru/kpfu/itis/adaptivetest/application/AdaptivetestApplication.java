package ru.kpfu.itis.adaptivetest.application;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@ComponentScan("ru.kpfu.itis.adaptivetest")
@EnableJpaRepositories(basePackages = "ru.kpfu.itis.adaptivetest.repositories")
@EntityScan(basePackages = "ru.kpfu.itis.adaptivetest.models")
public class AdaptivetestApplication {
	public static void main(String[] args) {
		SpringApplication.run(AdaptivetestApplication.class, args);
	}
}
