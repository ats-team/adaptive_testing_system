package ru.kpfu.itis.adaptivetest.utils;

import org.springframework.stereotype.Component;

import java.time.*;
import java.util.Date;
@Component
public class DateTimeUtil {
    public  Date addSeconds(Date date, int seconds) {
        Instant instant = date.toInstant();
        ZonedDateTime zonedDateTime = instant.atZone(ZoneId.of("Europe/Moscow"));
        LocalDateTime localDateTime = zonedDateTime.toLocalDateTime();
        System.out.println("initial local date: " + localDateTime);
        localDateTime = localDateTime.plusSeconds(seconds);
        System.out.println("new local date: " + localDateTime);
        instant = localDateTime.toInstant(ZoneOffset.of("+03:00:00"));
        return java.util.Date.from( instant );
    }

    public  int compare(Date date1, Date date2) {
        return Long.compare(date1.getTime(), date2.getTime());
    }

    public  long getDifferenceInSeconds(Date date1, Date date2) {
        return (date1.getTime() - date2.getTime()) / 1000;
    }
}
