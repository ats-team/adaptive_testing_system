package ru.kpfu.itis.adaptivetest.models;

import lombok.*;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "questions")
public class Question {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "name", nullable = false)
    private String name;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "test_id")
    private Test test;

    @Column(name = "level_count", nullable = false)
    private int levelCount = 0;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "question", orphanRemoval = true)
    private List<AdaptedQuestion> adaptedQuestions;
}
