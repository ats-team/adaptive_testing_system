package ru.kpfu.itis.adaptivetest.models;

import lombok.*;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "adapted_questions")
public class AdaptedQuestion {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "text", nullable = false)
    private String text;

    @Column(name = "right_answer", nullable = false)
    private String rightAnswer;

    @Column(name = "photo_path")
    private String photoPath;

    @Column(name = "file_path")
    private String filePath;

    @Column(name = "score", nullable = false)
    private int score;

    @Column(name = "level", nullable = false)
    private int level;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "question_id")
    private Question question;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "passedTest", orphanRemoval = true)
    private List<PassedQuestion> passedQuestions;
}
