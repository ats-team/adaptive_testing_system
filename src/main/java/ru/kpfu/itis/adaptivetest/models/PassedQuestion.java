package ru.kpfu.itis.adaptivetest.models;

import lombok.*;

import javax.persistence.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "passed_questions")
public class PassedQuestion {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "answer", nullable = false)
    private String answer;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "passed_test_id")
    private PassedTest passedTest;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "adapted_question_id")
    private AdaptedQuestion adaptedQuestion;
}
