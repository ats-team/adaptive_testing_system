package ru.kpfu.itis.adaptivetest.models;

import lombok.*;
import ru.kpfu.itis.adaptivetest.security.role.Role;
import ru.kpfu.itis.adaptivetest.security.state.State;

import javax.persistence.*;
import java.util.List;

/**
 * @author Rustem Saitgareev
 * 11-602
 * 000
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@EqualsAndHashCode(of = "id")
@Entity
@Table(name = "users", indexes = {
        @Index(columnList = "email", name = "user_email_index"),
        @Index(columnList = "login", name = "user_login_index")})
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "email", nullable = false, unique = true)
    private String email;

    @Column(name = "login", nullable = false, unique = true)
    private String login;

    @Column(name = "password", nullable = false)
    private String password;

    @Column(name = "fio")
    private String fio;

    @Enumerated(EnumType.STRING)
    private Role role;

    @Enumerated(EnumType.STRING)
    private State state;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "author", orphanRemoval = true)
    private List<Test> myTests;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "user", orphanRemoval = true)
    private List<AvailableTest> availableTests;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "user", orphanRemoval = true)
    private List<PassedTest> passedTests;
}