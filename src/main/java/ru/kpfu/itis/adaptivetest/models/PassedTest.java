package ru.kpfu.itis.adaptivetest.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Getter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "passed_tests")
public class PassedTest {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "score")
    private int score;

    @Column(name = "date")
    private Date date; //время старта

    @Column(name = "duration")
    private int duration;

    @Column(name = "complexity")
    private int complexity;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id")
    private User user;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "test_id")
    private Test test;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "passedTest", orphanRemoval = true)
    private List<PassedQuestion> passedQuestions;

    public void setScore(int score) {
        this.score = score;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public void setComplexity(int complexity) {
        if (complexity < 0) {
            complexity = 0;
        } else if (complexity > 100) {
            complexity = 100;
        }
        this.complexity = complexity;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void setTest(Test test) {
        this.test = test;
    }

    public void setPassedQuestions(List<PassedQuestion> passedQuestions) {
        this.passedQuestions = passedQuestions;
    }

    public long getTimeStampPassed() {
        return date.getTime() / 1000;
    }
}
