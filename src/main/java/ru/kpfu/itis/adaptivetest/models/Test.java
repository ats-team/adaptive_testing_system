package ru.kpfu.itis.adaptivetest.models;

import lombok.*;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * @author Rustem Saitgareev
 * 11-602
 * 000
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "tests")
public class Test {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "short_description")
    private String shortDescription;

    @Column(name = "detailed_description")
    private String detailedDescription;

    @Column(name = "is_private", nullable = false)
    @Type(type = "boolean")
    private Boolean isPrivate = false;

    @Column(name = "is_available", nullable = false)
    @Type(type = "boolean")
    private Boolean isAvailable = false;

    @Column(name = "is_questions_details_available", nullable = false)
    @Type(type = "boolean")
    private Boolean isQuestionsDetailsAvailable = false;

    @Column(name = "max_duration")
    private int maxDuration;

    @Column(name = "max_score")
    private int maxScore;

    @Column(name = "date_created")
    private Date dateCreated;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "author_id")
    private User author;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "test", orphanRemoval = true)
    private List<AvailableTest> invitations;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "test", orphanRemoval = true)
    private List<Question> questions;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "test", orphanRemoval = true)
    private List<PassedTest> passedTests;

    public long getTimeStampCreated() {
        return dateCreated.getTime() / 1000;
    }
}
