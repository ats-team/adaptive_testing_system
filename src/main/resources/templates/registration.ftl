<#if error??>
<div class="alert alert-danger" role="alert">${error}</div>
</#if>
<form class="form-horizontal" action="/registration" method="post">
    <input name="login" placeholder="Логин">
    <input name="email" placeholder="E-mail">
    <input name="fio" placeholder="ФИО">
    <input name="password" placeholder="Пароль">
    <input name="repeatPassword" placeholder="Повторите пароль">
    <input type="submit">
</form>