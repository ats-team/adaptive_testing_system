<#if model.error??>
<div class="alert alert-danger" role="alert">Логин или пароль введены неверно</div>
</#if>
<div class="content-block">
    <form class="form-horizontal" action="/login" method="post">
        <input name="login" placeholder="Логин"><br>
        <input type="password" name="password" placeholder="Пароль"><br>
        <label for="remember-me">Запомнить меня</label>
        <input type="checkbox" id="remember-me" name="remember-me">
        <input type="submit">
    </form>
</div>