insert into users (email, fio, login, password, role, state)
  values ('email1@example.com', 'fio1', 'author', 'pass', 'USER', 'CONFIRMED');
insert into users (email, fio, login, password, role, state)
  values ('email2@example.com', 'fio2', 'student1', 'pass', 'USER', 'CONFIRMED');
insert into users (email, fio, login, password, role, state)
  values ('email3@example.com', 'fio2', 'student2', 'pass', 'USER', 'CONFIRMED');

INSERT INTO tests (name, short_description, detailed_description, is_private, is_available, max_duration, max_score, date_created, is_questions_details_available, author_id)
  VALUES ('Название теста1', 'Short description', 'Detailed description', 'f', 't', 600, 10, to_timestamp(1521922433), 'f', 1);
insert into passed_tests (complexity, date, duration, score, test_id, user_id)
  VALUES (3, to_timestamp(1), 120, 5, 1, 2);
insert into questions (name, level_count, test_id)
  values ('name', 1, 1), ('name', 1, 1);
insert into adapted_questions (file_path, level, photo_path, right_answer, score, text, question_id)
  values ('file1', 1, 'photo1', 'rightAnswer1', '100', 'text1', 1),
  ('file2', 1, 'photo2', 'rightAnswer2', '200', 'text2', 2);
insert into passed_questions (answer, adapted_question_id, passed_test_id)
  values ('answer1', 1, 1), ('answer2', 2, 1);

INSERT INTO tests (name, short_description, detailed_description, is_private, is_available, max_duration, max_score, date_created, is_questions_details_available, author_id)
  VALUES ('Name of test2', 'Short description', 'Detailed description', 'f', 't', 600, 10, to_timestamp(1521922433), 't', 1);
insert into passed_tests (complexity, date, duration, score, test_id, user_id)
  VALUES (3, to_timestamp(1), 120, 5, 2, 2);
insert into questions (name, level_count, test_id)
  values ('name', 1, 2), ('name', 1, 2);
insert into adapted_questions (file_path, level, photo_path, right_answer, score, text, question_id)
  values ('file1', 1, 'photo1', 'rightAnswer1', '100', 'text1', 3),
  ('file2', 1, 'photo2', 'rightAnswer2', '200', 'text2', 4);
insert into passed_questions (answer, adapted_question_id, passed_test_id)
  values ('answer1', 3, 2), ('answer2', 4, 2);