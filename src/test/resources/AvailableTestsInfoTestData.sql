INSERT INTO users (id, email, login, password, fio) VALUES (1, 'teacher1@example.com', 'teacher1', 'password', 'Surname Name');
INSERT INTO users (id, email, login, password, fio) VALUES (2, 'teacher2@example.com', 'teacher2', 'password', 'LastName Name');
INSERT INTO users (id, email, login, password, fio) VALUES (3, 'student@example.com', 'student', 'password', 'Surname Name');
INSERT INTO tests (id, name, short_description, detailed_description, is_private, is_available, max_duration, max_score, is_questions_details_available, author_id)
  VALUES (1, 'Name of test','Short description', 'Detailed description', 't', 't', 10, 10, 't', 1);
INSERT INTO tests (id, name, short_description, detailed_description, is_private, is_available, max_duration, max_score, is_questions_details_available, author_id)
  VALUES (2, 'Test 2','Short description', 'Detailed description', 't', 't', 10, 10, 't', 2);
INSERT INTO tests (id, name, short_description, detailed_description, is_private, is_available, max_duration, max_score, is_questions_details_available, author_id)
  VALUES (3, 'Name test 3','Short description', 'Detailed description', 'f', 't', 10, 10, 't', 1);
INSERT INTO questions (id, name, test_id, level_count) VALUES (1, 'name', 1, 3);
INSERT INTO available_tests (id, test_id, user_id) VALUES (1, 1, 3);
INSERT INTO available_tests (id, test_id, user_id) VALUES (2, 2, 3);