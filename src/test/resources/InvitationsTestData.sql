insert into users (email, fio, login, password, role, state)
  VALUES ('example@email1.com', 'fio1', 'login1', 'password', 'USER', 'CONFIRMED'),
         ('example@email2.com', 'fio2', 'login2', 'password', 'USER', 'CONFIRMED');

insert into tests (detailed_description, is_available, is_private, max_duration, max_score, name, short_description, author_id, date_created, is_questions_details_available)
  values ('dd', true, true, 100, 100, 'name', 'sd', 1, to_timestamp(1), true),
         ('dd', true, true, 100, 100, 'name', 'sd', 1, to_timestamp(1), true),
         ('dd', true, true, 100, 100, 'name', 'sd', 1, to_timestamp(1), true);

insert into available_tests (test_id, user_id)
  VALUES (1, 2),
         (2, 2);