insert into users (email, fio, login, password, role, state)
  values ('email@example.com', 'fio', 'login', 'pass', 'USER', 'CONFIRMED');
INSERT INTO tests (name, short_description, detailed_description, is_private, is_available, max_duration, max_score, date_created, is_questions_details_available, author_id)
  VALUES ('Name of test','Short description', 'Detailed description', 'f', 'f', 600, 10, to_timestamp(1521922433), true, 1);
insert into questions (name, level_count, test_id)
  values ('name', 1, 1);
insert into questions (name, level_count, test_id)
  values ('name', 1, 1);
insert into  adapted_questions (text, right_answer, score, level, question_id)
  values ('1', 'aa', 2, 0, 1);
insert into  adapted_questions (text, right_answer, score, level, question_id)
  values ('1', 'aa', 5, 0, 1);