insert into users (id, email, fio, login, password, role, state)
    VALUES (1, 'example@email1.com', 'userFio', 'userLogin', 'password', 'USER', 'CONFIRMED'),
           (2, 'example@email2.com', 'userFio2', 'userLogin2', 'password', 'USER', 'CONFIRMED');

insert into tests (id, detailed_description, is_available, is_private, max_duration, max_score, name, short_description, author_id, date_created, is_questions_details_available)
    values (1, 'Detailed Description', true, false, 120, 10, 'testName', 'short desc.', 1, to_timestamp(1), true);

insert into passed_tests (id, complexity, date, duration, score, test_id, user_id)
    VALUES (1, 3, to_timestamp(1), 120, 5, 1, 1),
           (2, 4, to_timestamp(1), 120, 6, 1, 2);

insert into questions (id, name, level_count, test_id)
    VALUES (1, 'name', 1, 1);