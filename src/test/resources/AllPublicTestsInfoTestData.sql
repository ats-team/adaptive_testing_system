INSERT INTO users (id, email, fio, login, password)
  VALUES
    (1, 'email1', 'Surname1 Name1', 'login1', 'password'),
    (2, 'email2', 'Surname2 Name2', 'login2', 'password');

INSERT INTO tests (id, detailed_description, is_available, is_private, max_duration, max_score, name, short_description, date_created, is_questions_details_available, author_id)
    VALUES
      (1, 'detailed description', TRUE, FALSE, 42, 100, 'test1', 'short desc1', to_timestamp(1521922433), 't', 1),
      (2, 'detailed description', TRUE, FALSE, 42, 100, 'test2', 'short desc2', to_timestamp(1521922433), 't', 2),
      (3, 'detailed description', TRUE, TRUE, 42, 100, 'test1', 'short desc2', to_timestamp(1521922433), 't', 2);