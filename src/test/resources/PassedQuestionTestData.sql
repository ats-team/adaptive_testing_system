insert into users (id, email, fio, login, password, role, state)
    values (1, 'example@email1.com', 'fio1', 'login1', 'password1', 'USER', 'CONFIRMED'),
        (2, 'example@email2.com', 'fio2', 'login2', 'password2', 'USER', 'CONFIRMED'),
        (3, 'example@email3.com', 'fio3', 'login3', 'password3', 'USER', 'CONFIRMED');

insert into tests (id, detailed_description, is_available, is_private, max_duration, max_score, name, short_description, author_id, date_created, is_questions_details_available)
    values (1, 'dd1', true, false, 100, 100, 'name1', 'sd1', 1, to_timestamp(1), false),
        (2, 'dd2', true, false, 200, 200, 'name2', 'sd2', 1, to_timestamp(1), true);

insert into passed_tests (id, complexity, date, duration, score, test_id, user_id)
    values (1, 1, to_timestamp(1), 50, 50, 1, 2),
        (2, 1, to_timestamp(1), 100, 100, 2, 2);

insert into questions (id, name, level_count, test_id)
    values (1, 'name', 1, 1),
        (2, 'name', 1, 2);

insert into adapted_questions (id, file_path, level, photo_path, right_answer, score, text, question_id)
    values (1, 'file1', 1, 'photo1', 'rightAnswer1', '100', 'text1', 1),
        (2, 'file2', 1, 'photo2', 'rightAnswer2', '200', 'text2', 2);

insert into passed_questions (id, answer, adapted_question_id, passed_test_id)
    values (1, 'answer1', 1, 1),
        (2, 'answer2', 2, 2);