INSERT INTO users (email, fio, login, password, role, state) VALUES
 ('email@example.com', 'Surname Name', 'login1', '123456', 'USER', 'CONFIRMED');
INSERT INTO tests (detailed_description, is_available, is_private, max_duration, max_score, name, short_description, author_id, date_created, is_questions_details_available)
 VALUES ('Detailed description', TRUE , FALSE , 1200, 9, 'test 1', 'description', 1, to_timestamp(1), true),
        ('Detailed description', TRUE , TRUE, 1200, 9, 'test 2', 'description', 1, to_timestamp(1), true),
        ('Detailed description', TRUE , TRUE , 1200, 9, 'test 3', 'description', 1, to_timestamp(1), true);

INSERT INTO available_tests (id, test_id, user_id) VALUES (1, 3, 1);
INSERT INTO questions (level_count, test_id) VALUES
  (3, 1),
  (3, 1),
  (3, 1),
  (3, 3),
  (3, 3),
  (3, 3);
INSERT INTO adapted_questions (level, right_answer, score, text, question_id) VALUES
(1, 'answer', 3, 'question 1 level 1', 1),
(2, 'answer', 3, 'question 1 level 2', 1),
(3, 'answer', 3, 'question 1 level 3', 1),
(1, 'answer', 3, 'question 2 level 1', 2),
(2, 'answer', 3, 'question 2 level 2', 2),
(3, 'answer', 3, 'question 2 level 3', 2),
(1, 'answer', 3, 'question 3 level 1', 3),
(2, 'answer', 3, 'question 3 level 2', 3),
(3, 'answer', 3, 'question 3 level 3', 3),
( 1, 'answer1', 3, 'question 1 level 1', 4),
( 2, 'answer1', 3, 'question 1 level 2', 4),
( 3, 'answer1', 3, 'question 1 level 3', 4),
( 1, 'answer1', 3, 'question 2 level 1', 5),
( 2, 'answer1', 3, 'question 2 level 2', 5),
( 3, 'answer1', 3, 'question 2 level 3', 5),
( 1, 'answer1', 3, 'question 3 level 1', 6),
( 2, 'answer1', 3, 'question 3 level 2', 6),
( 3, 'answer1', 3, 'question 3 level 3', 6);

INSERT INTO passed_tests (complexity, "date", duration, score, test_id, user_id) VALUES
 (50, current_timestamp - INTERVAL '60 second',1200, 3, 3, 1);

INSERT INTO passed_questions (answer, adapted_question_id, passed_test_id) VALUES
 ('answer', 10, 1);


