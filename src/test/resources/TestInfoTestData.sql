INSERT INTO users (id, email, login, password, fio) VALUES (1, 'email@example.com', 'login1', 'password', 'Surname Name');
INSERT INTO tests (id, name, short_description, detailed_description, is_private, is_available, max_duration, max_score, date_created, is_questions_details_available, author_id)
  VALUES (1, 'Name of test','Short description', 'Detailed description', 'f', 't', 600, 10, to_timestamp(1521922433), true, 1);
INSERT INTO questions (id, name, test_id, level_count) VALUES (1, 'name', 1, 3);