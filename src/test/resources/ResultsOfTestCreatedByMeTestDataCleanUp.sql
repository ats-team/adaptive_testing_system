delete from questions;
delete from passed_tests;
delete from tests;
delete from users;

ALTER SEQUENCE passed_tests_id_seq RESTART WITH 1;
ALTER SEQUENCE tests_id_seq RESTART WITH 1;
ALTER SEQUENCE users_id_seq RESTART WITH 1;
ALTER SEQUENCE questions_id_seq RESTART WITH 1;