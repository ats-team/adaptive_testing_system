delete from adapted_questions;
delete from questions;
delete from tests;
delete from users;
ALTER SEQUENCE tests_id_seq RESTART WITH 1;
ALTER SEQUENCE users_id_seq RESTART WITH 1;
ALTER SEQUENCE questions_id_seq RESTART WITH 1;
ALTER SEQUENCE adapted_questions_id_seq RESTART WITH 1;