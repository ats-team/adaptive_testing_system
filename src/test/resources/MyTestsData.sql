insert into users (email, fio, login, password, role, state)
  values ('email1@example.com', 'fio1', 'login1', 'pass', 'USER', 'CONFIRMED');
insert into users (email, fio, login, password, role, state)
  values ('email2@example.com', 'fio2', 'login2', 'pass', 'USER', 'CONFIRMED');
INSERT INTO tests (name, short_description, detailed_description, is_private, is_available, max_duration, max_score, date_created, is_questions_details_available, author_id)
  VALUES ('Name of test1','Short description', 'Detailed description', 'f', 't', 600, 10, to_timestamp(1521922433), 't', 1);
INSERT INTO tests (name, short_description, detailed_description, is_private, is_available, max_duration, max_score, date_created, is_questions_details_available, author_id)
  VALUES ('Name of test2','Short description', 'Detailed description', 'f', 't', 600, 10, to_timestamp(1521922433), 't', 1);
INSERT INTO tests (name, short_description, detailed_description, is_private, is_available, max_duration, max_score, date_created, is_questions_details_available, author_id)
  VALUES ('Name of test3','Short description', 'Detailed description', 'f', 't', 600, 10, to_timestamp(1521922433), 't', 2);
insert into passed_tests (complexity, date, duration, score, test_id, user_id)
  VALUES (3, to_timestamp(1), 120, 5, 1, 1),
         (4, to_timestamp(1), 120, 6, 1, 2);