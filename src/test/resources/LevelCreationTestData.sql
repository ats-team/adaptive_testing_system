INSERT INTO users (id, email, fio, login, password, role, state) VALUES
  (1, 'email@example.com', 'FIO', 'login1', 'password1', 'USER', 'CONFIRMED');
INSERT INTO users (id, email, fio, login, password, role, state) VALUES
  (2, 'email2@example.com', 'FIO2', 'login2', 'password2', 'USER', 'CONFIRMED');
INSERT INTO tests (id, detailed_description, is_available, is_private, max_duration, max_score, name, short_description, is_questions_details_available, author_id)
  VALUES (1, 'detailed description', 't', 't', 10, 10, 'test name', 'short desc', 't', 1);
INSERT INTO questions (id, name, level_count, test_id) VALUES
  (1, 'name', 2, 1);
INSERT INTO adapted_questions (id, file_path, level, photo_path, right_answer, score, text, question_id)
  VALUES (1, '', 1, '', 'answer', 5, 'text', 1);