delete from available_tests;
delete from tests;
delete from users;

ALTER SEQUENCE tests_id_seq RESTART WITH 1;
ALTER SEQUENCE users_id_seq RESTART WITH 1;
ALTER SEQUENCE available_tests_id_seq RESTART WITH 1;