insert into users (id, email, fio, login, password, role, state)
    values (1, 'email@example.com', 'fio', 'login', 'pass', 'USER', 'CONFIRMED');

insert into tests (id, is_available, is_private, name, author_id, max_duration, max_score, is_questions_details_available)
    values
        (1, true, false, 'name', 1, 5, 10, 't');

insert into questions (id, name, level_count, test_id)
    values (1, 'name', 1, 1);

insert into adapted_questions
          (id, level, right_answer, score, text, question_id)
      VALUES
          (1, 1, 'right_answer', 5, 'text', 1),
          (2, 1, 'right_answer', 5, 'text', 1);