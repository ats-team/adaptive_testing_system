delete from questions;
delete from tests;
delete from users;

ALTER SEQUENCE tests_id_seq RESTART WITH 1;
ALTER SEQUENCE users_id_seq RESTART WITH 1;
ALTER SEQUENCE questions_id_seq RESTART WITH 1;