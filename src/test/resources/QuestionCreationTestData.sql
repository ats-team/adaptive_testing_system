insert into users (id, email, fio, login, password, role, state)
values (1, 'email@example.com', 'fio', 'login', 'pass', 'USER', 'CONFIRMED');

insert into tests (id, is_available, is_private, name, author_id, max_duration, max_score, is_questions_details_available)
values
  (1, true, false, 'testName', 1, 5, 10, true);