package ru.kpfu.itis.adaptivetest.testInfoTests;

import ru.kpfu.itis.adaptivetest.models.Test;
import ru.kpfu.itis.adaptivetest.models.User;
import ru.kpfu.itis.adaptivetest.repositories.QuestionRepository;
import ru.kpfu.itis.adaptivetest.repositories.TestRepository;
import ru.kpfu.itis.adaptivetest.services.implementations.TestServiceImpl;

import java.time.Duration;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestInfoSefviceTest {
    @org.junit.Test
    public void shouldReturnRightMap() throws Exception{
        TestRepository testRepository = mock(TestRepository.class);
        QuestionRepository questionRepository = mock(QuestionRepository.class);

        User author = new User();
        author.setLogin("login_example");
        author.setFio("Surname Name");

        Test test = new Test();
        test.setId(1);
        test.setName("Name of test");
        test.setDetailedDescription("Detailed description");
        test.setAuthor(author);
        test.setMaxDuration(600);
        test.setDateCreated(new Date(1521922433000L));

        when(testRepository.getOne(1L)).thenReturn(test);
        when(questionRepository.countAllByTest(test)).thenReturn(1);

        TestServiceImpl testService = new TestServiceImpl();
        testService.setTestRepository(testRepository);
        testService.setQuestionRepository(questionRepository);

        Map<String, Object> expected = new HashMap<>();
        expected.put("test_id", 1L);
        expected.put("name", "Name of test");
        expected.put("detailed_description", "Detailed description");
        expected.put("creator_login", "login_example");
        expected.put("creator_fio", "Surname Name");
        expected.put("max_duration", 600);
        expected.put("question_count", 1);
        expected.put("date_created", 1521922433L);

        Map<String, Object> actual = testService.getTestInfo(1L);
        assertEquals(expected, actual);
    }
}
