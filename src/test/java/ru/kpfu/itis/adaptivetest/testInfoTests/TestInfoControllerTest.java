package ru.kpfu.itis.adaptivetest.testInfoTests;

import org.springframework.test.web.servlet.MockMvc;
import ru.kpfu.itis.adaptivetest.controllers.TestInfoController;
import ru.kpfu.itis.adaptivetest.services.implementations.TestServiceImpl;

import java.util.HashMap;
import java.util.Map;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.hamcrest.Matchers.*;

public class TestInfoControllerTest {
    @org.junit.Test
    public void shouldReturnRightJSON() throws Exception{
        Map<String, Object> testMap = new HashMap<>();
        testMap.put("test_id", 1L);
        testMap.put("name", "Name of test");
        testMap.put("detailed_description", "Detailed description");
        testMap.put("creator_login", "login_example");
        testMap.put("creator_fio", "Surname Name");
        testMap.put("max_duration", 10);
        testMap.put("question_count", 1);
        testMap.put("date_created", 1521922433);


        TestServiceImpl testService = mock(TestServiceImpl.class);
        when(testService.getTestInfo(1L)).thenReturn(testMap);
        TestInfoController controller = new TestInfoController(testService);

        MockMvc mockMvc = standaloneSetup(controller).build();
        mockMvc.perform(get("/internal_api/tests/1"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.test_id", is(1)))
                .andExpect(jsonPath("$.name", is("Name of test")))
                .andExpect(jsonPath("$.detailed_description", is("Detailed description")))
                .andExpect(jsonPath("$.creator_login", is("login_example")))
                .andExpect(jsonPath("$.question_count", is(1)))
                .andExpect(jsonPath("$.creator_fio", is("Surname Name")))
                .andExpect(jsonPath("$.max_duration", is(10)))
                .andExpect(jsonPath("$.date_created", is(1521922433)))
        ;
    }
}
