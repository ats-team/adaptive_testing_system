package ru.kpfu.itis.adaptivetest.resultsOfTestCreatedByMeTests;

import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import ru.kpfu.itis.adaptivetest.controllers.ResultsOfTestCreatedByMeController;
import ru.kpfu.itis.adaptivetest.services.implementations.ResultsOfTestCreatedByMeServiceImpl;

import java.time.Instant;
import java.util.*;

import static org.hamcrest.Matchers.*;
import static org.mockito.AdditionalMatchers.not;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

/**
 * @author Bulat Giniyatullin
 * 19 April 2018
 */

public class ResultsOfTestCreatedByMeControllerTest {
    private static MockMvc mockMvc;

    @BeforeClass
    public static void prepareMockMvc() {
        Map<String, Object> test = new HashMap<>();
        test.put("test_id", 0L);
        test.put("name", "testName");
        test.put("detailed_description", "Detailed Description");
        test.put("max_duration", 120);
        test.put("date_created", new Date(5));
        test.put("question_count", 0);
        test.put("count_of_participants", 2);
        List<Map<String, Object>> passedTestsList = new ArrayList<>();
        Map<String, Object> passedTest1 = new HashMap<>();
        passedTest1.put("passed_test_id", 1L);
        passedTest1.put("login", "userLogin");
        passedTest1.put("fio", "userFio");
        passedTest1.put("score", 5);
        passedTest1.put("complexity", 3);
        passedTest1.put("duration", 120);
        passedTest1.put("date", new Date(5));
        passedTestsList.add(passedTest1);
        Map<String, Object> passedTest2 = new HashMap<>();
        passedTest2.put("passed_test_id", 2L);
        passedTest2.put("login", "userLogin2");
        passedTest2.put("fio", "userFio2");
        passedTest2.put("score", 6);
        passedTest2.put("complexity", 4);
        passedTest2.put("duration", 120);
        passedTest2.put("date", new Date(5));
        passedTestsList.add(passedTest2);
        test.put("participants", passedTestsList);

        ResultsOfTestCreatedByMeServiceImpl resultsOfTestCreatedByMeService =
                mock(ResultsOfTestCreatedByMeServiceImpl.class);

        doReturn(test).when(resultsOfTestCreatedByMeService).getTestResults(0L);
        doReturn(new HashMap<>()).when(resultsOfTestCreatedByMeService)
                .getTestResults(not(eq(0L)));

        ResultsOfTestCreatedByMeController resultsOfTestCreatedByMeController =
                new ResultsOfTestCreatedByMeController(resultsOfTestCreatedByMeService);

        mockMvc = standaloneSetup(resultsOfTestCreatedByMeController).build();
    }

    @Test
    public void shouldReturnCorrectJson() throws Exception {
        mockMvc.perform(get("/internal_api/tests/my/0/results"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.test_id", is(0)))
                .andExpect(jsonPath("$.name", is("testName")))
                .andExpect(jsonPath("$.detailed_description", is("Detailed Description")))
                .andExpect(jsonPath("$.max_duration", is(120)))
                .andExpect(jsonPath("$.count_of_participants", is(2)))
                .andExpect(jsonPath("$.question_count", is(0)))
                .andExpect(jsonPath("$.date_created", is((int)new Date(5).getTime())))
                .andExpect(jsonPath("$.participants[*].passed_test_id", contains(1, 2)))
                .andExpect(jsonPath("$.participants[*].login", contains("userLogin", "userLogin2")))
                .andExpect(jsonPath("$.participants[*].fio", contains("userFio", "userFio2")))
                .andExpect(jsonPath("$.participants[*].score", contains(5, 6)))
                .andExpect(jsonPath("$.participants[*].complexity", contains(3, 4)))
                .andExpect(jsonPath("$.participants[*].duration", contains(120, 120)))
                .andExpect(jsonPath("$.participants[*].date", contains((int)new Date(5).getTime(), (int)new Date(5).getTime())));
    }

    @Test
    public void shouldReturnEmptyResponseWith404StatusIfTestIdIsIncorrect() throws Exception {
        mockMvc.perform(get("/internal_api/question_creation_data/1"))
                .andExpect(status().isNotFound())
                .andExpect(content().string(isEmptyString()));
    }
}
