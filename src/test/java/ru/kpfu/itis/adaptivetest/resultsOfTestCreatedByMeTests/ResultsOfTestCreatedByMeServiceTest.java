package ru.kpfu.itis.adaptivetest.resultsOfTestCreatedByMeTests;

import org.junit.BeforeClass;
import ru.kpfu.itis.adaptivetest.models.PassedTest;
import ru.kpfu.itis.adaptivetest.models.Test;
import ru.kpfu.itis.adaptivetest.models.User;
import ru.kpfu.itis.adaptivetest.repositories.TestRepository;
import ru.kpfu.itis.adaptivetest.services.implementations.ResultsOfTestCreatedByMeServiceImpl;

import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.mockito.AdditionalMatchers.not;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;

/**
 * @author Bulat Giniyatullin
 * 19 April 2018
 */

public class ResultsOfTestCreatedByMeServiceTest {
    private static ResultsOfTestCreatedByMeServiceImpl resultsOfTestCreatedByMeService;

    @BeforeClass
    public static void prepareService() {
        User user1 = User.builder()
                .login("userLogin")
                .fio("userFio")
                .build();
        User user2 = User.builder()
                .login("userLogin2")
                .fio("userFio2")
                .build();
        PassedTest passedTest1 = PassedTest.builder()
                .id(1)
                .user(user1)
                .score(5)
                .complexity(3)
                .duration(120)
                .date(new Date(1000))
                .build();
        PassedTest passedTest2 = PassedTest.builder()
                .id(2)
                .user(user2)
                .score(6)
                .complexity(4)
                .duration(120)
                .date(new Date(1000))
                .build();
        List<PassedTest> passedTests = new LinkedList<>();
        passedTests.add(passedTest1);
        passedTests.add(passedTest2);
        Test test = Test.builder()
                .id(0)
                .name("testName")
                .detailedDescription("Detailed Description")
                .maxDuration(120)
                .dateCreated(new Date(1000))
                .passedTests(passedTests)
                .build();

        TestRepository testRepository = mock(TestRepository.class);
        doReturn(Optional.of(test)).when(testRepository).findById(0L);
        doReturn(Optional.empty()).when(testRepository).findById(not(eq(0L)));

        resultsOfTestCreatedByMeService = new ResultsOfTestCreatedByMeServiceImpl(testRepository);
    }

    @org.junit.Test
    public void shouldReturnCorrectData() {
        Map<String, Object> expected = new HashMap<>();
        expected.put("test_id", 0L);
        expected.put("name", "testName");
        expected.put("detailed_description", "Detailed Description");
        expected.put("max_duration", 120);
        expected.put("date_created", 1L);
        expected.put("question_count", 0);
        expected.put("count_of_participants", 2);
        List<Map<String, Object>> passedTestsList = new LinkedList<>();
        Map<String, Object> passedTest1 = new HashMap<>();
        passedTest1.put("passed_test_id", 1L);
        passedTest1.put("login", "userLogin");
        passedTest1.put("fio", "userFio");
        passedTest1.put("score", 5);
        passedTest1.put("complexity", 3);
        passedTest1.put("duration", 120);
        passedTest1.put("date", 1L);
        passedTestsList.add(passedTest1);
        Map<String, Object> passedTest2 = new HashMap<>();
        passedTest2.put("passed_test_id", 2L);
        passedTest2.put("login", "userLogin2");
        passedTest2.put("fio", "userFio2");
        passedTest2.put("score", 6);
        passedTest2.put("complexity", 4);
        passedTest2.put("duration", 120);
        passedTest2.put("date", 1L);
        passedTestsList.add(passedTest2);
        expected.put("participants", passedTestsList);

        Map<String, Object> received = resultsOfTestCreatedByMeService.getTestResults(0L);

        assertEquals(expected, received);
    }

    @org.junit.Test
    public void shouldReturnEmptyMapIfTestIdIsIncorrect() {
        Map<String, Object> received = resultsOfTestCreatedByMeService.getTestResults(1L);

        assertThat(received)
                .isEmpty();
    }
}
