/*
package ru.kpfu.itis.adaptivetest.passingTestProcessTest;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import ru.kpfu.itis.adaptivetest.application.AdaptivetestApplication;
import ru.kpfu.itis.adaptivetest.controllers.TestPassingController;
import ru.kpfu.itis.adaptivetest.exceptions.AlreadyFinishedException;
import ru.kpfu.itis.adaptivetest.exceptions.AlreadyStartedException;
import ru.kpfu.itis.adaptivetest.exceptions.NotFoundException;
import ru.kpfu.itis.adaptivetest.models.PassedTest;
import ru.kpfu.itis.adaptivetest.models.Question;
import ru.kpfu.itis.adaptivetest.services.implementations.PassingTestServiceImpl;
import ru.kpfu.itis.adaptivetest.services.interfaces.PassingTestService;

import java.util.HashMap;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;


@RunWith(SpringRunner.class)
@SpringBootTest(
        classes = {AdaptivetestApplication.class}
)
public class TestPassingControllerTest {
    @Test
    @WithUserDetails(value = "testLogin", userDetailsServiceBeanName = "userDetailsService1")
    public void shouldReturn403WhenServerThrowsAccessDeniedException() throws Exception{
        PassingTestService passingTestService = mock(PassingTestServiceImpl.class);
        doThrow(new AccessDeniedException("403")).when(passingTestService).startTest(any());
        TestPassingController testPassingController = new TestPassingController();
        testPassingController.setPassingTestService(passingTestService);

        MockMvc mockMvc = standaloneSetup(testPassingController).build();

        mockMvc.perform(post("/internal_api/test/1/start"))
                .andExpect(status().isForbidden());
    }

    @Test
    @WithUserDetails(value = "testLogin", userDetailsServiceBeanName = "userDetailsService1")
    public void shouldReturn400WhenServerThrowsAlreadyStartedException() throws Exception{
        PassingTestService passingTestService = mock(PassingTestServiceImpl.class);
        doThrow(new AlreadyStartedException()).when(passingTestService).startTest(any());
        TestPassingController testPassingController = new TestPassingController();
        testPassingController.setPassingTestService(passingTestService);

        MockMvc mockMvc = standaloneSetup(testPassingController).build();

        mockMvc.perform(post("/internal_api/test/1/start"))
                .andExpect(status().isBadRequest())
                .andExpect(header().string("error_code", "already_started"))
        ;
    }

    @Test
    @WithUserDetails(value = "testLogin", userDetailsServiceBeanName = "userDetailsService1")
    public void shouldReturn400WhenServerThrowsNotFoundException() throws Exception{
        PassingTestService passingTestService = mock(PassingTestServiceImpl.class);
        doThrow(new NotFoundException(ru.kpfu.itis.adaptivetest.models.Test.class)).when(passingTestService).startTest(any());
        TestPassingController testPassingController = new TestPassingController();
        testPassingController.setPassingTestService(passingTestService);

        MockMvc mockMvc = standaloneSetup(testPassingController).build();

        mockMvc.perform(post("/internal_api/test/1/start"))
                .andExpect(status().isBadRequest())
                .andExpect(header().string("error_code", "not_found.test"))
        ;
    }

    @Test
    @WithUserDetails(value = "testLogin", userDetailsServiceBeanName = "userDetailsService1")
    public void shouldReturnJsonWithNextQuestionIdAndAddPassedTestIntoSessionWhenStartingTest() throws Exception{
        Map<String, Object> map = new HashMap<>();
        map.put("next_question_id", 1L);
        PassingTestService passingTestService = mock(PassingTestServiceImpl.class);
        doReturn(map).when(passingTestService).startTest(any());
        doReturn(Question.builder().id(1L).build()).when(passingTestService).getNextQuestion(any(), any());
        TestPassingController testPassingController = new TestPassingController();
        testPassingController.setPassingTestService(passingTestService);

        MockMvc mockMvc = standaloneSetup(testPassingController).build();

        mockMvc.perform(post("/internal_api/test/1/start"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.next_question_id", is(1)));

    }

    @Test
    @WithUserDetails(value = "testLogin", userDetailsServiceBeanName = "userDetailsService1")
    public void shouldReturn403WhenServiceThrowsAccessDeniedException() throws Exception{
        PassingTestServiceImpl passingTestService = mock(PassingTestServiceImpl.class);
        doThrow(new AccessDeniedException("403")).when(passingTestService).finishTest();
        TestPassingController controller = new TestPassingController();
        controller.setPassingTestService(passingTestService);
        MockMvc mockMvc = standaloneSetup(controller).build();

        mockMvc.perform(post("/internal_api/test/finish"))
                .andExpect(status().isForbidden());
    }

    @Test
    @WithUserDetails(value = "testLogin", userDetailsServiceBeanName = "userDetailsService1")
    public void shouldReturn400WhenServiceThrowsAlreadyFinishedException() throws Exception{
        PassingTestServiceImpl passingTestService = mock(PassingTestServiceImpl.class);
        doThrow(new AlreadyFinishedException()).when(passingTestService).finishTest();
        TestPassingController controller = new TestPassingController();
        controller.setPassingTestService(passingTestService);

        MockMvc mockMvc = standaloneSetup(controller).build();

        mockMvc.perform(post("/internal_api/test/finish"))
                .andExpect(status().isBadRequest())
                .andExpect(header().string("error_code", "already_finished"));
    }

    @Test
    @WithUserDetails(value = "testLogin", userDetailsServiceBeanName = "userDetailsService1")
    public void shouldCallFinishTestMethodOfService() throws Exception{
        PassingTestServiceImpl passingTestService = mock(PassingTestServiceImpl.class);
        doNothing().when(passingTestService).finishTest();

        TestPassingController controller = new TestPassingController();
        controller.setPassingTestService(passingTestService);

        MockMvc mockMvc = standaloneSetup(controller).build();

        mockMvc.perform(post("/internal_api/test/finish"))
                .andExpect(status().isOk());
        verify(passingTestService, times(1)).finishTest();
    }

}
*/
