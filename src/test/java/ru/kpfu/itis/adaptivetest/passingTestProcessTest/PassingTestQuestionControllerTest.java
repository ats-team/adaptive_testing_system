/*
package ru.kpfu.itis.adaptivetest.passingTestProcessTest;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.validation.Errors;
import ru.kpfu.itis.adaptivetest.application.AdaptivetestApplication;
import ru.kpfu.itis.adaptivetest.controllers.PassingTestQuestionController;
import ru.kpfu.itis.adaptivetest.exceptions.AlreadyFinishedException;
import ru.kpfu.itis.adaptivetest.exceptions.AlreadyPassedException;
import ru.kpfu.itis.adaptivetest.exceptions.NotFoundException;
import ru.kpfu.itis.adaptivetest.models.AdaptedQuestion;
import ru.kpfu.itis.adaptivetest.models.PassedTest;
import ru.kpfu.itis.adaptivetest.models.User;
import ru.kpfu.itis.adaptivetest.services.implementations.PassingTestServiceImpl;
import ru.kpfu.itis.adaptivetest.services.interfaces.PassingTestService;
import ru.kpfu.itis.adaptivetest.validators.AnswerFormValidator;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

@RunWith(SpringRunner.class)
@SpringBootTest(
        classes = {AdaptivetestApplication.class}
)
public class PassingTestQuestionControllerTest {

    public PassingTestQuestionController prepareController(Exception serviceException) throws Exception{
        AnswerFormValidator answerFormValidator = mock(AnswerFormValidator.class);
        doReturn(true).when(answerFormValidator).supports(any());
        PassingTestService passingTestService = mock(PassingTestServiceImpl.class);
        if (serviceException != null){
            doThrow(serviceException).when(passingTestService).processAnswer(any());
        }else {
            doNothing().when(passingTestService).processAnswer(any());
        }

        PassingTestQuestionController controller = new PassingTestQuestionController();
        controller.setAnswerFormValidator(answerFormValidator);
        controller.setPassingTestService(passingTestService);

        return controller;
    }

    @Test
    @WithUserDetails(value = "testLogin", userDetailsServiceBeanName = "userDetailsService1")
    public void shouldReturn400WhenAnswerIsNotValid() throws Exception{
        AnswerFormValidator answerFormValidator = mock(AnswerFormValidator.class);
        doReturn(true).when(answerFormValidator).supports(any());
        doAnswer(invocationOnMock -> {
            Errors errors = invocationOnMock.getArgument(1);
            errors.reject("not_found.level", "Уровня не существует");
            return null;
        }).when(answerFormValidator).validate(any(), any());
        PassingTestQuestionController controller = new PassingTestQuestionController();
        controller.setAnswerFormValidator(answerFormValidator);

        MockMvc mockMvc = standaloneSetup(controller).build();
        mockMvc.perform(post("/internal_api/questions/1"))
                .andExpect(status().isBadRequest())
                .andExpect(header().string("error_code", "not_found.level"));

    }

    @Test
    @WithUserDetails(value = "testLogin", userDetailsServiceBeanName = "userDetailsService1")
    public void shouldReturn403WhenServerThrowsAccessDeniedException() throws Exception{

        MockMvc mockMvc = standaloneSetup(prepareController(new AccessDeniedException("403"))).build();
        mockMvc.perform(post("/internal_api/questions/1"))
                .andExpect(status().isForbidden());

    }

    @Test
    @WithUserDetails(value = "testLogin", userDetailsServiceBeanName = "userDetailsService1")
    public void shouldReturn400WhenSmthNotFound() throws Exception{

        MockMvc mockMvc = standaloneSetup(prepareController(new NotFoundException(AdaptedQuestion.class))).build();
        mockMvc.perform(post("/internal_api/questions/1"))
                .andExpect(status().isBadRequest())
                .andExpect(header().string("error_code", "not_found.AdaptedQuestion"));

    }

    @Test
    @WithUserDetails(value = "testLogin", userDetailsServiceBeanName = "userDetailsService1")
    public void shouldReturn400WhenQuestionAlreadyPassed() throws Exception{

        MockMvc mockMvc = standaloneSetup(prepareController(new AlreadyPassedException())).build();

        mockMvc.perform(post("/internal_api/questions/1"))
                .andExpect(status().isBadRequest())
                .andExpect(header().string("error_code", "already_passed"));

    }

    @WithUserDetails(value = "testLogin", userDetailsServiceBeanName = "userDetailsService1")
    public void shouldReturn400WhenTestAlreadyFinished() throws Exception{

        MockMvc mockMvc = standaloneSetup(prepareController(new AlreadyFinishedException())).build();

        mockMvc.perform(post("/internal_api/questions/1"))
                .andExpect(status().isBadRequest())
                .andExpect(header().string("error_code", "already_finished"));

    }

    @Test
    @WithUserDetails(value = "testLogin", userDetailsServiceBeanName = "userDetailsService1")
    public void shouldReturn200WhenAnsweringQuestion() throws Exception{
        PassingTestQuestionController controller = prepareController(null);
        MockMvc mockMvc = standaloneSetup(controller).build();
        mockMvc.perform(post("/internal_api/questions/1"))
                .andExpect(status().isOk());

    }


    @Test
    @WithUserDetails(value = "testLogin", userDetailsServiceBeanName = "userDetailsService1")
    public void shouldReturn403WhenServiceThrowsAccessDeniedException() throws Exception{
        PassingTestQuestionController controller = new PassingTestQuestionController();
        PassingTestService passingTestService = mock(PassingTestServiceImpl.class);
        controller.setPassingTestService(passingTestService);
        doThrow(new AccessDeniedException("403")).when(passingTestService).getCurrentQuestion(any());
        MockMvc mockMvc = standaloneSetup(controller).build();
        mockMvc.perform(get("/internal_api/questions/1"))
                .andExpect(status().isForbidden());

    }


    @Test
    @WithUserDetails(value = "testLogin", userDetailsServiceBeanName = "userDetailsService1")
    public void shouldReturnRightJsonWhenGettingCurrentQuestion() throws Exception{
        Date date = new Date();
        Map<String, Object> data = new HashMap<>();
        data.put("question_id", 1L);
        data.put("level_id", 2L);
        data.put("test_name", "Test");
        data.put("picture_path", null);
        data.put("file_path", null);
        data.put("text", "text of question");
        data.put("answered_questions_count", 3);
        data.put("question_count", 5);
        data.put("end_time", date);
        data.put("next_question_id", 4L);
        PassingTestService passingTestService = mock(PassingTestServiceImpl.class);
        doReturn(data).when(passingTestService).getCurrentQuestion(any());

        PassingTestQuestionController controller = new PassingTestQuestionController();
        controller.setPassingTestService(passingTestService);

        MockMvc mockMvc = standaloneSetup(controller).build();
        mockMvc.perform(get("/internal_api/questions/1"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.question_id", is(1)))
                .andExpect(jsonPath("$.level_id", is(2)))
                .andExpect(jsonPath("$.test_name", is("Test")))
                .andExpect(jsonPath("$.text", is("text of question")))
                .andExpect(jsonPath("$.answered_questions_count", is(3)))
                .andExpect(jsonPath( "$.question_count", is(5)))
                .andExpect(jsonPath("$.next_question_id", is(4)))
                .andExpect(jsonPath("$.end_time", is(date.getTime())));
    }
}
*/
