package ru.kpfu.itis.adaptivetest.passingTestProcessTest;

import org.mockito.ArgumentCaptor;
import org.springframework.security.access.AccessDeniedException;
import ru.kpfu.itis.adaptivetest.exceptions.AlreadyFinishedException;
import ru.kpfu.itis.adaptivetest.exceptions.AlreadyPassedException;
import ru.kpfu.itis.adaptivetest.exceptions.AlreadyStartedException;
import ru.kpfu.itis.adaptivetest.exceptions.NotFoundException;
import ru.kpfu.itis.adaptivetest.forms.AnswerForm;
import ru.kpfu.itis.adaptivetest.models.*;
import ru.kpfu.itis.adaptivetest.repositories.*;
import ru.kpfu.itis.adaptivetest.services.implementations.PassingTestServiceImpl;
import ru.kpfu.itis.adaptivetest.utils.DateTimeUtil;

import javax.servlet.http.HttpSession;
import java.time.Instant;
import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;


public class PassingTestServiceTest {
    /*@org.junit.Test(expected = AccessDeniedException.class)
    public void shouldThrowAccessDeniedExceptionOnStartingNotAvailableTest() throws Exception{
        AvailableTestRepository availableTestRepository = mock(AvailableTestRepository.class);
        doReturn(new ArrayList<AvailableTest>()).when(availableTestRepository).findAllByUserAndTest(any(), any());
        TestRepository testRepository = mock(TestRepository.class);
        doReturn(Optional.of(new Test())).when(testRepository).findById(any());

        PassingTestServiceImpl passingTestService = mock(PassingTestServiceImpl.class);
        doCallRealMethod().when(passingTestService).setAvailableTestRepository(any());
        doCallRealMethod().when(passingTestService).setTestRepository(any());
        doCallRealMethod().when(passingTestService).startTest(any());
        doReturn(new User()).when(passingTestService).getUser();
        passingTestService.setAvailableTestRepository(availableTestRepository);
        passingTestService.setTestRepository(testRepository);

        passingTestService.startTest(1L);
    }

    @org.junit.Test(expected = AlreadyStartedException.class)
    public void shouldThrowExceptionOnStartingAlreadyPassedTest() throws Exception{
        AvailableTestRepository availableTestRepository = mock(AvailableTestRepository.class);
        List<AvailableTest> availableTests = new ArrayList<>();
        availableTests.add(new AvailableTest());
        doReturn(availableTests).when(availableTestRepository).findAllByUserAndTest(any(), any());
        TestRepository testRepository = mock(TestRepository.class);
        doReturn(Optional.of(new Test())).when(testRepository).findById(any());
        PassedTestRepository passedTestRepository = mock(PassedTestRepository.class);
        doReturn(Optional.of(new PassedTest())).when(passedTestRepository).findByUserAndAndTest(any(), any());

        PassingTestServiceImpl passingTestService = mock(PassingTestServiceImpl.class);
        doCallRealMethod().when(passingTestService).setAvailableTestRepository(any());
        doCallRealMethod().when(passingTestService).setTestRepository(any());
        doCallRealMethod().when(passingTestService).setPassedTestRepository(any());
        passingTestService.setAvailableTestRepository(availableTestRepository);
        passingTestService.setTestRepository(testRepository);
        passingTestService.setPassedTestRepository(passedTestRepository);
        doCallRealMethod().when(passingTestService).startTest(any());
        doReturn(new User()).when(passingTestService).getUser();
        passingTestService.startTest(1L);

    }

    @org.junit.Test
    public void shouldCreatePassedTestWithRightAttributesAndSaveItInSessionWhenTestIsStarted() throws Exception{
        User user = User.builder().id(1).build();
        Test test = new Test();
        AvailableTestRepository availableTestRepository = mock(AvailableTestRepository.class);
        List<AvailableTest> availableTests = new ArrayList<>();
        availableTests.add(new AvailableTest());
        doReturn(availableTests).when(availableTestRepository).findAllByUserAndTest(any(), any());
        TestRepository testRepository = mock(TestRepository.class);
        doReturn(Optional.of(test)).when(testRepository).findById(any());
        PassedTestRepository passedTestRepository = mock(PassedTestRepository.class);
        doReturn(Optional.empty()).when(passedTestRepository).findByUserAndAndTest(any(), any());

        ArgumentCaptor<PassedTest> passedTestArgumentCaptor = ArgumentCaptor.forClass(PassedTest.class);
        doReturn(null).when(passedTestRepository).save(passedTestArgumentCaptor.capture());

        PassingTestServiceImpl passingTestService = mock(PassingTestServiceImpl.class);
        doCallRealMethod().when(passingTestService).setAvailableTestRepository(any());
        doCallRealMethod().when(passingTestService).setTestRepository(any());
        doCallRealMethod().when(passingTestService).setPassedTestRepository(any());
        passingTestService.setAvailableTestRepository(availableTestRepository);
        passingTestService.setTestRepository(testRepository);
        passingTestService.setPassedTestRepository(passedTestRepository);

        doReturn(user).when(passingTestService).getUser();
        HttpSession session = mock(HttpSession.class);

        ArgumentCaptor<String> attrName = ArgumentCaptor.forClass(String.class);
        ArgumentCaptor<PassedTest> attrValue = ArgumentCaptor.forClass(PassedTest.class);
        doNothing().when(session).setAttribute(attrName.capture(), attrValue.capture());
        doReturn(session).when(passingTestService).session();
        doReturn(Question.builder().id(2L).build()).when(passingTestService).getNextQuestion(any(), any());
        doCallRealMethod().when(passingTestService).startTest(any());
        Map<String, Object> result = passingTestService.startTest(1L);

        assertThat(passedTestArgumentCaptor.getValue())
            .hasFieldOrPropertyWithValue("test", test)
            .hasFieldOrPropertyWithValue("complexity", 50)
            .hasFieldOrPropertyWithValue("score", 0)
            .hasFieldOrPropertyWithValue("user", user);
        assertEquals("passed_test", attrName.getValue());
        assertThat(attrValue.getValue())
                .hasFieldOrPropertyWithValue("test", test)
                .hasFieldOrPropertyWithValue("complexity", 50)
                .hasFieldOrPropertyWithValue("score", 0)
                .hasFieldOrPropertyWithValue("user", user);
        assertThat(result)
                .hasSize(1)
                .containsOnly(new AbstractMap.SimpleEntry<String, Object>("next_question_id", 2L));
    }

    @org.junit.Test(expected = AlreadyFinishedException.class)
    public void shouldThrowExceptionOnFinishingAlreadyPassedTest()throws Exception {
        Test test = Test.builder()
                .maxDuration(0)
                .build();
        PassedTest passedTest = PassedTest.builder()
                .date(new Date())
                .test(test)
                .build();
        PassedTestRepository passedTestRepository = mock(PassedTestRepository.class);
        doReturn(null).when(passedTestRepository).save(any());

        HttpSession session = mock(HttpSession.class);
        doReturn(passedTest).when(session).getAttribute("passed_test");

        PassingTestServiceImpl passingTestService = mock(PassingTestServiceImpl.class);
        doReturn(true).when(passingTestService).isFinished(any());
        doReturn(session).when(passingTestService).session();
        doCallRealMethod().when(passingTestService).finishTest();
        doCallRealMethod().when(passingTestService).setPassedTestRepository(passedTestRepository);

        passingTestService.finishTest();
    }


    @org.junit.Test
    public void shouldUpdateDurationAndRemovePassedTestFromSessionWhenFinishTest() throws Exception{
        Date date = new Date();
        DateTimeUtil dateTimeUtil = mock(DateTimeUtil.class);
        when(dateTimeUtil.getDifferenceInSeconds(any(), any())).thenReturn(300L);
        PassedTest passedTest = PassedTest.builder()
                .date(date)
                .duration(500)
                .build();
        PassedTestRepository passedTestRepository = mock(PassedTestRepository.class);
        ArgumentCaptor<PassedTest> passedTestArgumentCaptor = ArgumentCaptor.forClass(PassedTest.class);
        doReturn(null).when(passedTestRepository).save(passedTestArgumentCaptor.capture());

        PassingTestServiceImpl passingTestService = mock(PassingTestServiceImpl.class);
        doReturn(false).when(passingTestService).isFinished(any());
        doCallRealMethod().when(passingTestService).finishTest();
        doCallRealMethod().when(passingTestService).setPassedTestRepository(passedTestRepository);
        doCallRealMethod().when(passingTestService).setDateTimeUtil(dateTimeUtil);

        HttpSession session = mock(HttpSession.class);

        ArgumentCaptor<String> attrName = ArgumentCaptor.forClass(String.class);
        doNothing().when(session).removeAttribute(attrName.capture());
        doReturn(passedTest).when(session).getAttribute("passed_test");
        doReturn(session).when(passingTestService).session();

        passingTestService.setPassedTestRepository(passedTestRepository);
        passingTestService.setDateTimeUtil(dateTimeUtil);
        passingTestService.finishTest();

        assertThat(passedTestArgumentCaptor.getValue())
                .hasFieldOrPropertyWithValue("duration", 300);
        assertEquals("passed_test", attrName.getValue());
    }

    @org.junit.Test(expected = AccessDeniedException.class)
    public void shouldThrowAccessDeniedExceptionWhenFinishPassedTestThatIsNotInSession() throws Exception{
        PassingTestServiceImpl passingTestService = mock(PassingTestServiceImpl.class);
        doCallRealMethod().when(passingTestService).finishTest();
        HttpSession session = mock(HttpSession.class);
        doReturn(session).when(passingTestService).session();
        passingTestService.finishTest();
    }

    @org.junit.Test(expected = AccessDeniedException.class)
    public void shouldThrowAccessDeniedExceptionWhenAnsweringOnQuestionOFTestWhatIsNotInSession() throws Exception {
        User user = User.builder().id(1).build();
        AnswerForm answerForm = mock(AnswerForm.class);

        PassingTestServiceImpl passingTestService = mock(PassingTestServiceImpl.class);
        doReturn(true).when(passingTestService).isFinished(any());
        HttpSession session = mock(HttpSession.class);
        doReturn(session).when(passingTestService).session();
        doReturn(user).when(passingTestService).getUser();
        doCallRealMethod().when(passingTestService).processAnswer(any());

        passingTestService.processAnswer(answerForm);

    }

    @org.junit.Test(expected = AccessDeniedException.class)
    public void shouldThrowAccessDeniedExceptionWhenPassedTestInSessionIsNotForCurrentUser() throws Exception {
        User user = User.builder().id(1).build();
        User user1 = User.builder().id(2).build();
        PassedTest passedTest = PassedTest.builder().user(user1).build();
        AnswerForm answerForm = mock(AnswerForm.class);

        PassingTestServiceImpl passingTestService = mock(PassingTestServiceImpl.class);
        doReturn(true).when(passingTestService).isFinished(any());
        HttpSession session = mock(HttpSession.class);
        doReturn(passedTest).when(session).getAttribute("passed_test");
        doReturn(session).when(passingTestService).session();
        doReturn(user).when(passingTestService).getUser();
        doCallRealMethod().when(passingTestService).processAnswer(any());

        passingTestService.processAnswer(answerForm);

    }

    @org.junit.Test(expected = AlreadyFinishedException.class)
    public void shouldThrowExceptionOnAnsweringOnAlreadyPassedTest() throws Exception {
        User user = User.builder().id(1).build();
        PassedTest passedTest = PassedTest.builder().user(user).build();
        AnswerForm answerForm = mock(AnswerForm.class);

        PassingTestServiceImpl passingTestService = mock(PassingTestServiceImpl.class);
        doReturn(true).when(passingTestService).isFinished(any());
        HttpSession session = mock(HttpSession.class);
        doReturn(passedTest).when(session).getAttribute("passed_test");
        doReturn(session).when(passingTestService).session();
        doReturn(user).when(passingTestService).getUser();
        doCallRealMethod().when(passingTestService).processAnswer(any());

        passingTestService.processAnswer(answerForm);

    }

    @org.junit.Test(expected = NotFoundException.class)
    public void shouldThrowNotFoundExceptionWhenAvailableQuestionDoesNotExist() throws Exception{
        AdaptedQuestionRepository adaptedQuestionRepository = mock(AdaptedQuestionRepository.class);
        doReturn(Optional.empty()).when(adaptedQuestionRepository).findById(any());
        User user = User.builder().id(1L).build();
        PassedTest passedTest = PassedTest.builder().user(user).build();
        PassingTestServiceImpl passingTestService = mock(PassingTestServiceImpl.class);
        HttpSession session = mock(HttpSession.class);
        doReturn(passedTest).when(session).getAttribute("passed_test");
        doReturn(session).when(passingTestService).session();
        doReturn(user).when(passingTestService).getUser();
        doReturn(false).when(passingTestService).isFinished(any());
        doCallRealMethod().when(passingTestService).processAnswer(any());
        doCallRealMethod().when(passingTestService).setAdaptedQuestionRepository(adaptedQuestionRepository);
        passingTestService.setAdaptedQuestionRepository(adaptedQuestionRepository);
        AnswerForm answerForm = new AnswerForm();
        answerForm.setLevel_id(1);
        passingTestService.processAnswer(answerForm);
    }

    @org.junit.Test(expected = AlreadyPassedException.class)
    public void shouldThrowExceptionWhenAnswerOnAlreadyPassedQuestion() throws Exception{
        AdaptedQuestionRepository adaptedQuestionRepository = mock(AdaptedQuestionRepository.class);
        doReturn(Optional.of(new AdaptedQuestion())).when(adaptedQuestionRepository).findById(any());
        PassedQuestionRepository passedQuestionRepository = mock(PassedQuestionRepository.class);
        doReturn(Optional.of(new PassedQuestion())).when(passedQuestionRepository).findByPassedTestAndAdaptedQuestion_Question(any(), any());

        User user = User.builder().id(1L).build();
        PassedTest passedTest = PassedTest.builder().user(user).build();

        PassingTestServiceImpl passingTestService = mock(PassingTestServiceImpl.class);
        doReturn(false).when(passingTestService).isFinished(any());

        HttpSession session = mock(HttpSession.class);
        doReturn(passedTest).when(session).getAttribute("passed_test");
        doReturn(session).when(passingTestService).session();
        doReturn(user).when(passingTestService).getUser();

        doCallRealMethod().when(passingTestService).processAnswer(any());
        doCallRealMethod().when(passingTestService).setAdaptedQuestionRepository(adaptedQuestionRepository);
        doCallRealMethod().when(passingTestService).setPassedQuestionRepository(passedQuestionRepository);
        doCallRealMethod().when(passingTestService).isPassed(any(), any());

        passingTestService.setAdaptedQuestionRepository(adaptedQuestionRepository);
        passingTestService.setPassedQuestionRepository(passedQuestionRepository);

        AnswerForm answerForm = new AnswerForm();
        answerForm.setLevel_id(1);

        passingTestService.processAnswer(answerForm);

    }

    @org.junit.Test
    public void complexityShouldChangeProperly() throws Exception{
        Test test = new Test();
        User user = User.builder().id(1).build();
        PassedTest passedTest = PassedTest.builder()
                .complexity(50)
                .test(test)
                .score(0)
                .user(user)
                .passedQuestions(new LinkedList<>())
                .build();

        AdaptedQuestion adaptedQuestion = AdaptedQuestion.builder()
                .rightAnswer("1")
                .build();
        AdaptedQuestionRepository adaptedQuestionRepository = mock(AdaptedQuestionRepository.class);
        doReturn(Optional.of(adaptedQuestion)).when(adaptedQuestionRepository).findById(any());

        PassedQuestionRepository passedQuestionRepository = mock(PassedQuestionRepository.class);
        doReturn(Optional.empty()).when(passedQuestionRepository).findByPassedTestAndAdaptedQuestion_Question(any(), any());

        QuestionRepository questionRepository = mock(QuestionRepository.class);
        doReturn(4).when(questionRepository).countAllByTest(any());

        PassedTestRepository passedTestRepository = mock(PassedTestRepository.class);
        doReturn(null).when(passedTestRepository).save(any());

        PassingTestServiceImpl passingTestService = mock(PassingTestServiceImpl.class);
        doReturn(false).when(passingTestService).isFinished(any());

        HttpSession session = mock(HttpSession.class);
        doReturn(passedTest).when(session).getAttribute("passed_test");
        doReturn(session).when(passingTestService).session();
        doReturn(user).when(passingTestService).getUser();

        doCallRealMethod().when(passingTestService).processAnswer(any());
        doCallRealMethod().when(passingTestService).setAdaptedQuestionRepository(adaptedQuestionRepository);
        doCallRealMethod().when(passingTestService).setPassedQuestionRepository(passedQuestionRepository);
        doCallRealMethod().when(passingTestService).setQuestionRepository(questionRepository);
        doCallRealMethod().when(passingTestService).setPassedTestRepository(passedTestRepository);
        doCallRealMethod().when(passingTestService).isPassed(any(), any());

        passingTestService.setAdaptedQuestionRepository(adaptedQuestionRepository);
        passingTestService.setPassedQuestionRepository(passedQuestionRepository);
        passingTestService.setQuestionRepository(questionRepository);
        passingTestService.setPassedTestRepository(passedTestRepository);

        AnswerForm answerForm = new AnswerForm();
        answerForm.setLevel_id(1);
        answerForm.setAnswer("2");

        passingTestService.processAnswer(answerForm);

        assertThat(passedTest)
                .hasFieldOrPropertyWithValue("complexity", 25);
    }

    @org.junit.Test(expected = NotFoundException.class)
    public void shouldThrowExceptionOnGettingInfoAboutNonExistingQuestion() throws Exception{
        QuestionRepository questionRepository = mock(QuestionRepository.class);
        doReturn(Optional.empty()).when(questionRepository).findById(any());
        User user = User.builder().id(1L).build();
        PassedTest passedTest = PassedTest.builder().user(user).build();

        PassingTestServiceImpl passingTestService = mock(PassingTestServiceImpl.class);

        HttpSession session = mock(HttpSession.class);
        doReturn(passedTest).when(session).getAttribute("passed_test");
        doReturn(session).when(passingTestService).session();
        doReturn(user).when(passingTestService).getUser();

        doCallRealMethod().when(passingTestService).setQuestionRepository(any());
        passingTestService.setQuestionRepository(questionRepository);

        doCallRealMethod().when(passingTestService).getCurrentQuestion(any());
        passingTestService.getCurrentQuestion(1L);
    }

    @org.junit.Test(expected = AlreadyPassedException.class)
    public void shouldThrowExceptionOnGettingInfoAboutPassedQuestion() throws Exception{
        PassedQuestionRepository passedQuestionRepository = mock(PassedQuestionRepository.class);
        doReturn(Optional.of(new PassedQuestion())).when(passedQuestionRepository).findByPassedTestAndAdaptedQuestion_Question(any(), any());
        QuestionRepository questionRepository = mock(QuestionRepository.class);
        doReturn(Optional.of(new Question())).when(questionRepository).findById(any());

        PassingTestServiceImpl passingTestService = mock(PassingTestServiceImpl.class);

        User user = User.builder().id(1L).build();
        PassedTest passedTest = PassedTest.builder().user(user).build();
        HttpSession session = mock(HttpSession.class);
        doReturn(passedTest).when(session).getAttribute("passed_test");
        doReturn(session).when(passingTestService).session();
        doReturn(user).when(passingTestService).getUser();

        doCallRealMethod().when(passingTestService).setQuestionRepository(any());
        doCallRealMethod().when(passingTestService).setPassedQuestionRepository(any());
        passingTestService.setQuestionRepository(questionRepository);
        passingTestService.setPassedQuestionRepository(passedQuestionRepository);
        doCallRealMethod().when(passingTestService).getCurrentQuestion(any());
        doCallRealMethod().when(passingTestService).isPassed(any(), any());
        passingTestService.getCurrentQuestion(1L);

    }

    @org.junit.Test
    public void shouldGiveRightDataWithNextQuestion() throws Exception{
        PassedQuestionRepository passedQuestionRepository = mock(PassedQuestionRepository.class);
        doReturn(Optional.empty()).when(passedQuestionRepository).findByPassedTestAndAdaptedQuestion_Question(any(), any());
        doReturn(2).when(passedQuestionRepository).countAllByPassedTest(any());
        QuestionRepository questionRepository = mock(QuestionRepository.class);
        doReturn(Optional.of(new Question())).when(questionRepository).findById(any());
        doReturn(5).when(questionRepository).countAllByTest(any());
        AdaptedQuestionRepository adaptedQuestionRepository = mock(AdaptedQuestionRepository.class);
        doReturn(4).when(adaptedQuestionRepository).countAdaptedQuestionByQuestionId(any());
        DateTimeUtil dateTimeUtil = mock(DateTimeUtil.class);
        Date date = Date.from(Instant.now());
        doReturn(date).when(dateTimeUtil).addSeconds(any(), anyInt());
        AdaptedQuestion adaptedQuestion = AdaptedQuestion.builder()
                .score(10)
                .text("text of question")
                .rightAnswer("right answer")
                .level(3)
                .id(1)
                .build();
        List<AdaptedQuestion> adaptedQuestions = new ArrayList<>();
        adaptedQuestions.add(adaptedQuestion);
        doReturn(adaptedQuestions).when(adaptedQuestionRepository).findAllByQuestionIdAndLevel(any(), any());
        User user = User.builder().id(1L).build();

        Test test = Test.builder()
                .name("Test")
                .build();

        PassedTest passedTest = PassedTest.builder()
                .complexity(50)
                .test(test)
                .user(user)
                .build();

        Question nextQuestion = Question.builder()
                .id(2)
                .build();
        List<Question> nextQuestions = new ArrayList<>();
        nextQuestions.add(nextQuestion);
        doReturn(nextQuestions).when(questionRepository).getNonPassedQuestions(any(), any(), any());

        PassingTestServiceImpl passingTestService = mock(PassingTestServiceImpl.class);
        doCallRealMethod().when(passingTestService).setPassedQuestionRepository(any());
        doCallRealMethod().when(passingTestService).setQuestionRepository(any());
        doCallRealMethod().when(passingTestService).setAdaptedQuestionRepository(any());
        doCallRealMethod().when(passingTestService).setDateTimeUtil(any());

        passingTestService.setPassedQuestionRepository(passedQuestionRepository);
        passingTestService.setQuestionRepository(questionRepository);
        passingTestService.setAdaptedQuestionRepository(adaptedQuestionRepository);
        passingTestService.setDateTimeUtil(dateTimeUtil);

        HttpSession session = mock(HttpSession.class);
        doReturn(passedTest).when(session).getAttribute("passed_test");
        doReturn(session).when(passingTestService).session();
        doReturn(user).when(passingTestService).getUser();

        doCallRealMethod().when(passingTestService).isPassed(any(), any());
        doCallRealMethod().when(passingTestService).getNextQuestion(any(), any());
        doCallRealMethod().when(passingTestService).getCurrentQuestion(any());

        Map<String, Object> result = passingTestService.getCurrentQuestion(1l);

        Map<String, Object> expected = new HashMap<>();
        expected.put("question_id", 1L);
        expected.put("level_id", 1L);
        expected.put("test_name", "Test");
        expected.put("picture_path", null);
        expected.put("file_path", null);
        expected.put("text", "text of question");
        expected.put("answered_questions_count", 2);
        expected.put("question_count", 5);
        expected.put("end_time", date);
        expected.put("next_question_id", 2L);
        assertThat(result)
                .hasSameSizeAs(expected)
                .containsAllEntriesOf(expected);
    }*/
}
