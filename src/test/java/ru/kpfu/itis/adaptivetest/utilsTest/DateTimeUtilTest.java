package ru.kpfu.itis.adaptivetest.utilsTest;

import org.junit.Test;
import ru.kpfu.itis.adaptivetest.utils.DateTimeUtil;

import java.util.Date;

import static junit.framework.TestCase.assertEquals;


public class DateTimeUtilTest {
    @Test
    public void addSecondsTest() {
        DateTimeUtil dateTimeUtil = new DateTimeUtil();
        int seconds = 600;
        Date date = new Date();
        Date newDate = dateTimeUtil.addSeconds(date, seconds);
        assertEquals(date.getTime() + seconds * 1000, newDate.getTime());
    }

    @Test
    public void compareTest() {
        DateTimeUtil dateTimeUtil = new DateTimeUtil();
        Date date1 = new Date();
        try{
            Thread.sleep(100);
        } catch (Exception e) {
            //ignore
        }
        Date date2 = new Date();
        assertEquals(dateTimeUtil.compare(date1, date2), -1);
    }
}
