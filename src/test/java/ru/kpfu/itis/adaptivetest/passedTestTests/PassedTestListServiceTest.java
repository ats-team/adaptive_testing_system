package ru.kpfu.itis.adaptivetest.passedTestTests;

import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.junit4.SpringRunner;
import ru.kpfu.itis.adaptivetest.application.AdaptivetestApplication;
import ru.kpfu.itis.adaptivetest.models.*;
import ru.kpfu.itis.adaptivetest.repositories.PassedQuestionRepository;
import ru.kpfu.itis.adaptivetest.repositories.PassedTestRepository;
import ru.kpfu.itis.adaptivetest.services.implementations.PassedTestServiceImpl;

import java.util.*;

import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @author Rustem Saitgareev
 * 11-602
 * 000
 */

@RunWith(SpringRunner.class)
@SpringBootTest(
        classes = {AdaptivetestApplication.class}
)
public class PassedTestListServiceTest {
    private Test test;
    private PassedTestServiceImpl passedTestService;
    private List<Map<String, Object>> expected = new LinkedList<>();

    @Before
    public void setUp() {
        User author = User.builder()
                .id(1)
                .login("Login")
                .fio("FIO")
                .build();
        User student1 = User.builder()
                .id(2)
                .build();
        test = Test.builder()
                .name("Name")
                .shortDescription("Short descr")
                .maxScore(10)
                .isQuestionsDetailsAvailable(false)
                .author(author)
                .build();
        PassedTest passedTest = PassedTest.builder()
                .id(1)
                .user(student1)
                .test(test)
                .duration(20)
                .score(5)
                .date(new Date(100L))
                .build();
        AdaptedQuestion adaptedQuestion = AdaptedQuestion.builder().build();
        PassedQuestion passedQuestion = PassedQuestion.builder()
                .id(1)
                .adaptedQuestion(adaptedQuestion)
                .passedTest(passedTest)
                .build();
        List<PassedQuestion> passedQuestionSet = new LinkedList<>();
        passedQuestionSet.add(passedQuestion);
        passedTest.setPassedQuestions(passedQuestionSet);

        List<PassedTest> passedTestList = new LinkedList<>();
        passedTestList.add(passedTest);

        PassedTestRepository passedTestRepository = mock(PassedTestRepository.class);
        when(passedTestRepository.findById(1L)).thenReturn(Optional.of(passedTest));
        when(passedTestRepository.findById(2L)).thenReturn(Optional.empty());
        Specification<PassedTest> passedTestSpecification = any();
        when(passedTestRepository.findAll(passedTestSpecification, any(Pageable.class))).thenReturn(new PageImpl<>(passedTestList));
        passedTestService = new PassedTestServiceImpl(passedTestRepository, mock(PassedQuestionRepository.class));

        Map<String, Object> expected = new HashMap<>();
        expected.put("id", passedTest.getId());
        expected.put("name", passedTest.getTest().getName());
        expected.put("short_description", passedTest.getTest().getShortDescription());
        expected.put("creator_login", passedTest.getTest().getAuthor().getLogin());
        expected.put("creator_fio", passedTest.getTest().getAuthor().getFio());
        expected.put("passing_date", passedTest.getTimeStampPassed());
        expected.put("duration", passedTest.getDuration());
        expected.put("received_score", passedTest.getScore());
        expected.put("max_score", passedTest.getTest().getMaxScore());
        this.expected.add(expected);
    }

    @org.junit.Test
    @WithUserDetails(value = "testLogin", userDetailsServiceBeanName = "userDetailsService1")
    public void shouldReturnCorrectList() throws Exception {
        List<Map<String, Object>> actual = passedTestService.getPassedTestList(null, null, null, null, null);
        assertThat(actual)
                .hasSameSizeAs(expected)
                .containsAll(expected);
    }
}
