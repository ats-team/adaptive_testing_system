package ru.kpfu.itis.adaptivetest.passedTestTests;

import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.junit4.SpringRunner;
import ru.kpfu.itis.adaptivetest.application.AdaptivetestApplication;
import ru.kpfu.itis.adaptivetest.exceptions.NotAccessException;
import ru.kpfu.itis.adaptivetest.exceptions.NotFoundException;
import ru.kpfu.itis.adaptivetest.models.*;
import ru.kpfu.itis.adaptivetest.repositories.PassedQuestionRepository;
import ru.kpfu.itis.adaptivetest.repositories.PassedTestRepository;
import ru.kpfu.itis.adaptivetest.services.implementations.PassedTestServiceImpl;

import java.util.*;

import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @author Rustem Saitgareev
 * 11-602
 * 000
 */

@RunWith(SpringRunner.class)
@SpringBootTest(
        classes = {AdaptivetestApplication.class}
)
public class PassedTestResultServiceTest {
    private Test test;
    private PassedTestServiceImpl passedTestService;
    private Map<String, Object> expected;

    @Before
    public void setUp() {
        User author = User.builder()
                .id(1)
                .login("Login")
                .fio("FIO")
                .build();
        User student1 = User.builder()
                .id(2)
                .build();
        test = Test.builder()
                .name("Name")
                .detailedDescription("Det descr")
                .maxScore(10)
                .isQuestionsDetailsAvailable(false)
                .author(author)
                .build();
        AdaptedQuestion adaptedQuestion1 = AdaptedQuestion.builder()
                .question(Question.builder().name("name1").build())
                .rightAnswer("111")
                .score(5)
                .build();
        PassedTest passedTest = PassedTest.builder()
                .id(1)
                .user(student1)
                .test(test)
                .duration(20)
                .score(5)
                .date(new Date(100L))
                .build();
        PassedQuestion passedQuestion = PassedQuestion.builder()
                .id(1)
                .answer("111")
                .adaptedQuestion(adaptedQuestion1)
                .passedTest(passedTest)
                .build();
        List<PassedQuestion> passedQuestionSet = new LinkedList<>();
        passedQuestionSet.add(passedQuestion);
        passedTest.setPassedQuestions(passedQuestionSet);

        PassedTestRepository passedTestRepository = mock(PassedTestRepository.class);
        when(passedTestRepository.findById(1L)).thenReturn(Optional.of(passedTest));
        when(passedTestRepository.findById(2L)).thenReturn(Optional.empty());

        PassedQuestionRepository passedQuestionRepository = mock(PassedQuestionRepository.class);
        List<Object[]> passedQuestionList = new LinkedList<>();
        passedQuestionList.add(new Object[]{passedQuestion, 10});
        when(passedQuestionRepository.findPassedQuestionWithMaxScoreByPassedTest(any())).thenReturn(passedQuestionList);

        passedTestService = new PassedTestServiceImpl(passedTestRepository, passedQuestionRepository);

        expected = new HashMap<>();
        expected.put("passed_test_id", passedTest.getId());
        expected.put("name", passedTest.getTest().getName());
        expected.put("detailed_description", passedTest.getTest().getDetailedDescription());
        expected.put("creator_login", passedTest.getTest().getAuthor().getLogin());
        expected.put("creator_fio", passedTest.getTest().getAuthor().getFio());
        expected.put("passing_date", passedTest.getTimeStampPassed());
        expected.put("duration", passedTest.getDuration());
        expected.put("received_score", passedTest.getScore());
        expected.put("max_score", passedTest.getTest().getMaxScore());
        List<Map<String, Object>> passedQuestionsList = new LinkedList<>();
        for (PassedQuestion pq : passedTest.getPassedQuestions()) {
            Map<String, Object> element = new HashMap<>();
            element.put("passed_question_id", pq.getId());
            element.put("question_name", pq.getAdaptedQuestion().getQuestion().getName());
            element.put("received_score", pq
                    .getAnswer()
                    .equals(pq.getAdaptedQuestion().getRightAnswer()) ?
                                pq.getAdaptedQuestion().getScore() : 0);
            element.put("max_score", 10);
            passedQuestionsList.add(element);
        }
        expected.put("passed_questions_list", passedQuestionsList);
    }

    @org.junit.Test
    @WithUserDetails(value = "testLogin", userDetailsServiceBeanName = "userDetailsService1")
    public void shouldReturnCorrectDataIfTestExistsAndUserIsAuthor() throws Exception {
        Map<String, Object> actual = passedTestService.getResultById(1L);
        assertThat(actual)
                .hasSameSizeAs(expected)
                .containsAllEntriesOf(expected);
    }

    @org.junit.Test
    @WithUserDetails(value = "testLogin", userDetailsServiceBeanName = "userDetailsService2")
    public void shouldReturnDataWithoutPassedQuestionsIfIsQuestionsDetailsAvailableAttributeIsFalse() throws Exception {
        expected.remove("passed_questions_list");
        Map<String, Object> actual = passedTestService.getResultById(1L);
        assertThat(actual)
                .hasSameSizeAs(expected)
                .containsAllEntriesOf(expected);
    }

    @org.junit.Test
    @WithUserDetails(value = "testLogin", userDetailsServiceBeanName = "userDetailsService2")
    public void shouldReturnDataWithPassedQuestionsIfIsQuestionsDetailsAvailableAttributeIsTrue() throws Exception {
        test.setIsQuestionsDetailsAvailable(true);
        Map<String, Object> actual = passedTestService.getResultById(1L);
        assertThat(actual)
                .hasSameSizeAs(expected)
                .containsAllEntriesOf(expected);
    }

    @org.junit.Test(expected = NotFoundException.class)
    @WithUserDetails(value = "testLogin", userDetailsServiceBeanName = "userDetailsService1")
    public void shouldThrowNotFoundExceptionIfTestDoesNotExist() throws Exception {
        passedTestService.getResultById(2L);
    }

    @org.junit.Test(expected = NotAccessException.class)
    @WithUserDetails(value = "testLogin", userDetailsServiceBeanName = "userDetailsService3")
    public void shouldThrowNotAccessExceptionIfUserHasNoPermission() throws Exception {
        passedTestService.getResultById(1L);
    }
}
