package ru.kpfu.itis.adaptivetest.passedTestTests;

import org.junit.Before;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import ru.kpfu.itis.adaptivetest.controllers.PassedTestController;
import ru.kpfu.itis.adaptivetest.services.interfaces.PassedTestServiceInterface;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

/**
 * @author Rustem Saitgareev
 * 11-602
 * 000
 */
public class PassedTestListControllerTest {
    private PassedTestController passedTestController;
    private MockMvc mockMvc;
    private List<Map<String, Object>> result = new LinkedList<>();

    @Before
    public void init() {
        Map<String, Object> entity = new HashMap<>();
        entity.put("answer", "OK");
        result.add(entity);

        PassedTestServiceInterface passedTestServiceInterface = mock(PassedTestServiceInterface.class);
        when(passedTestServiceInterface.getPassedTestList(any(), any(), any(), any(), any())).thenReturn(result);

        passedTestController = new PassedTestController(passedTestServiceInterface);
        mockMvc = standaloneSetup(passedTestController).build();
    }

    @org.junit.Test
    public void shouldReturnOkIfControllerCallsService() throws Exception {
        mockMvc.perform(get("/internal_api/passed_tests"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].answer", is("OK")));
    }
}
