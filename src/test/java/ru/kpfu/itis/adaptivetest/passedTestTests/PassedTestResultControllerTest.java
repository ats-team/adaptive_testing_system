package ru.kpfu.itis.adaptivetest.passedTestTests;

import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import ru.kpfu.itis.adaptivetest.application.AdaptivetestApplication;
import ru.kpfu.itis.adaptivetest.services.interfaces.PassedTestServiceInterface;

import java.util.HashMap;
import java.util.Map;

import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

/**
 * @author Rustem Saitgareev
 * 11-602
 * 000
 */
@RunWith(SpringRunner.class)
@SpringBootTest(
        classes = {AdaptivetestApplication.class}
)
public class PassedTestResultControllerTest {
    private ru.kpfu.itis.adaptivetest.controllers.PassedTestController passedTestController;
    private MockMvc mockMvc;
    private Map<String, Object> result;

    @Before
    public void init() throws Exception {
        result = new HashMap<>();
        result.put("answer", "OK");

        PassedTestServiceInterface passedTestServiceInterface = mock(PassedTestServiceInterface.class);
        when(passedTestServiceInterface.getResultById(any())).thenReturn(result);

        passedTestController = new ru.kpfu.itis.adaptivetest.controllers.PassedTestController(passedTestServiceInterface);
        mockMvc = standaloneSetup(passedTestController).build();
    }

    @org.junit.Test
    public void shouldReturnOkIfControllerCallsService() throws Exception {
        mockMvc.perform(get("/internal_api/passed_tests/1"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.answer", is("OK")));
    }
}
