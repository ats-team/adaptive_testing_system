package ru.kpfu.itis.adaptivetest.levelDeletingTests;

import org.junit.Test;
import org.mockito.ArgumentCaptor;
import ru.kpfu.itis.adaptivetest.repositories.AdaptedQuestionRepository;
import ru.kpfu.itis.adaptivetest.services.implementations.LevelServiceImpl;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;

/**
 * @author Bulat Giniyatullin
 * 12 April 2018
 */

public class LevelDeletingServiceTest {
    private LevelServiceImpl levelService;

    @Test
    public void shouldInvokeDeleteMethodOfRepository() {
        AdaptedQuestionRepository adaptedQuestionRepository = mock(AdaptedQuestionRepository.class);
        ArgumentCaptor<Long> argumentCaptor = ArgumentCaptor.forClass(Long.class);
        doNothing().when(adaptedQuestionRepository).deleteAdaptedQuestionById(argumentCaptor.capture());

        levelService = new LevelServiceImpl(adaptedQuestionRepository);

        Long expected = 1L;

        levelService.deleteLevelById(expected);

        assertThat(argumentCaptor.getValue())
                .isEqualTo(expected);
    }
}
