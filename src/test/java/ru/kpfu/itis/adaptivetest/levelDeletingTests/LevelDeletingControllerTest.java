package ru.kpfu.itis.adaptivetest.levelDeletingTests;

import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.springframework.test.web.servlet.MockMvc;
import ru.kpfu.itis.adaptivetest.controllers.LevelsController;
import ru.kpfu.itis.adaptivetest.models.AdaptedQuestion;
import ru.kpfu.itis.adaptivetest.services.implementations.LevelServiceImpl;
import ru.kpfu.itis.adaptivetest.validators.AdaptedQuestionValidator;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

/**
 * @author Bulat Giniyatullin
 * 12 April 2018
 */

public class LevelDeletingControllerTest {
    private static MockMvc mockMvc;
    private static LevelsController levelsController;
    private static ArgumentCaptor<Long> serviceDeleteMethodArgumentCaptor;

    @BeforeClass
    public static void prepareServiceMockAndMockMvc() {
        LevelServiceImpl levelService = mock(LevelServiceImpl.class);
        serviceDeleteMethodArgumentCaptor = ArgumentCaptor.forClass(Long.class);
        doNothing().when(levelService).deleteLevelById(serviceDeleteMethodArgumentCaptor.capture());
        AdaptedQuestionValidator validator = mock(AdaptedQuestionValidator.class);
        levelsController = new LevelsController(levelService, validator);

        mockMvc = standaloneSetup(levelsController).build();
    }

    @Test
    public void shouldReturnOkStatus() throws Exception {
        mockMvc.perform(delete("/internal_api/levels")
                .param("level_id_to_delete", "1"))
                .andExpect(status().isOk());
    }

    @Test
    public void shouldInvokeServiceMethod() throws Exception {
        Long expected = 1L;
        mockMvc.perform(delete("/internal_api/levels")
                .param("level_id_to_delete", String.valueOf(expected)));

        assertThat(serviceDeleteMethodArgumentCaptor.getValue())
                .isEqualTo(expected);
    }
}
