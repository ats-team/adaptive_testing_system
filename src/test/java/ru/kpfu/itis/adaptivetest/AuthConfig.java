package ru.kpfu.itis.adaptivetest;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import ru.kpfu.itis.adaptivetest.models.User;
import ru.kpfu.itis.adaptivetest.security.details.UserDetailsImpl;
import ru.kpfu.itis.adaptivetest.security.role.Role;
import ru.kpfu.itis.adaptivetest.security.state.State;

import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;

/**
 * @author Rustem Saitgareev
 * 11-602
 * 000
 */
@Configuration
public class AuthConfig {
    @Bean("userDetailsService1")
    public UserDetailsService userDetailsService() {
        User user = User.builder()
                .id(1)
                .login("testLogin")
                .fio("Surname Name")
                .email("test@email.com")
                .role(Role.USER)
                .state(State.CONFIRMED)
                .build();
        UserDetails userDetails = new UserDetailsImpl(user);
        UserDetailsService userDetailsService = mock(UserDetailsService.class);
        doReturn(userDetails).when(userDetailsService).loadUserByUsername("testLogin");
        return userDetailsService;
    }

    @Bean("userDetailsService2")
    public UserDetailsService userDetailsService2() {
        User user = User.builder()
                .id(2)
                .login("testLogin")
                .fio("Surname Name")
                .email("test@email.com")
                .role(Role.USER)
                .state(State.CONFIRMED)
                .build();
        UserDetails userDetails = new UserDetailsImpl(user);
        UserDetailsService userDetailsService = mock(UserDetailsService.class);
        doReturn(userDetails).when(userDetailsService).loadUserByUsername("testLogin");
        return userDetailsService;
    }

    @Bean("userDetailsService3")
    public UserDetailsService userDetailsService3() {
        User user = User.builder()
                .id(3)
                .login("testLogin")
                .fio("Surname Name")
                .email("test@email.com")
                .role(Role.USER)
                .state(State.CONFIRMED)
                .build();
        UserDetails userDetails = new UserDetailsImpl(user);
        UserDetailsService userDetailsService = mock(UserDetailsService.class);
        doReturn(userDetails).when(userDetailsService).loadUserByUsername("testLogin");
        return userDetailsService;
    }
}
