package ru.kpfu.itis.adaptivetest.invitationsTests;

import org.junit.Test;
import org.mockito.ArgumentCaptor;
import ru.kpfu.itis.adaptivetest.repositories.AvailableTestRepository;
import ru.kpfu.itis.adaptivetest.repositories.UserRepository;
import ru.kpfu.itis.adaptivetest.services.implementations.InvitationsServiceImpl;
import ru.kpfu.itis.adaptivetest.services.interfaces.InvitationsServiceInterface;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;

/**
 * @author Bulat Giniyatullin
 * 27 April 2018
 */

public class UninvitationServiceTest {
    @Test
    public void uninviteMethodMustInvokeDeleteMethodOfRepository() {
        AvailableTestRepository availableTestRepository = mock(AvailableTestRepository.class);
        ArgumentCaptor<Long> testIdArgumentCaptor = ArgumentCaptor.forClass(Long.class);
        ArgumentCaptor<String> loginArgumentCaptor = ArgumentCaptor.forClass(String.class);
        doNothing().when(availableTestRepository)
                .deleteAvailableTestByTestIdAndUserLogin(
                        testIdArgumentCaptor.capture(),
                        loginArgumentCaptor.capture());
        InvitationsServiceInterface invitationsService =
                new InvitationsServiceImpl(mock(UserRepository.class), availableTestRepository);


        String expectedLogin = "login";
        Long expectedTestId = 1L;

        invitationsService.uninvite(expectedLogin, expectedTestId);

        assertThat(testIdArgumentCaptor.getValue())
                .isEqualTo(expectedTestId);

        assertThat(loginArgumentCaptor.getValue())
                .isEqualTo(expectedLogin);
    }
}
