package ru.kpfu.itis.adaptivetest.invitationsTests;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import ru.kpfu.itis.adaptivetest.exceptions.NotFoundException;
import ru.kpfu.itis.adaptivetest.models.AvailableTest;
import ru.kpfu.itis.adaptivetest.models.User;
import ru.kpfu.itis.adaptivetest.repositories.AvailableTestRepository;
import ru.kpfu.itis.adaptivetest.repositories.UserRepository;
import ru.kpfu.itis.adaptivetest.services.implementations.InvitationsServiceImpl;
import ru.kpfu.itis.adaptivetest.services.interfaces.InvitationsServiceInterface;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.AdditionalMatchers.not;
import static org.mockito.Mockito.*;
import static org.mockito.internal.configuration.GlobalConfiguration.validate;

/**
 * @author Bulat Giniyatullin
 * 27 April 2018
 */

public class InvitationServiceTest {
    private static InvitationsServiceInterface invitationsService;
    private static AvailableTestRepository mockedAvailableTestRepository;

    @Before
    public void prepareService() {
        UserRepository userRepository = mock(UserRepository.class);
        User user = User.builder().id(1).login("login").build();
        doReturn(Optional.of(user)).when(userRepository).findOneByLogin("login");
        doReturn(Optional.empty()).when(userRepository).findOneByLogin(not(eq("login")));

        mockedAvailableTestRepository = mock(AvailableTestRepository.class);
        AvailableTest availableTest = AvailableTest.builder().id(1).build();
        doReturn(Optional.of(availableTest)).when(mockedAvailableTestRepository).findOneByTestIdAndUser(eq(1L), any(User.class));
        doReturn(Optional.empty()).when(mockedAvailableTestRepository).findOneByTestIdAndUser(not(eq(1L)), any(User.class));

        invitationsService = new InvitationsServiceImpl(userRepository, mockedAvailableTestRepository);
    }

    @Test
    public void inviteMethodMustInvokeCreateMethodOfRepository() throws NotFoundException {
        invitationsService.invite("login", 2L);

        AvailableTest expectedAvailableTest = AvailableTest.builder()
                .test(ru.kpfu.itis.adaptivetest.models.Test.builder().id(2L).build())
                .user(User.builder().id(1).login("login").build())
                .build();

        ArgumentCaptor<AvailableTest> availableTestCaptor = ArgumentCaptor.forClass(AvailableTest.class);

        verify(mockedAvailableTestRepository).save(availableTestCaptor.capture());

        assertThat(availableTestCaptor.getValue())
                .isEqualToComparingFieldByFieldRecursively(expectedAvailableTest);
    }

    @Test
    public void inviteMethodMustDoNothingIfAvailableTestIsAlreadyPresent() throws NotFoundException {
        invitationsService.invite("login", 1L);

        verify(mockedAvailableTestRepository, times(0)).save(any());
    }

    @Test
    public void inviteMethodMustThrowNotFoundExceptionIfUserDoesNotExist() {
        try {
            invitationsService.invite("unknownLogin", 1L);
            fail();
        } catch (NotFoundException e) {
            assertEquals(e.getaClass(), User.class);
        }
    }
}
