package ru.kpfu.itis.adaptivetest.invitationsTests;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.springframework.test.web.servlet.MockMvc;
import ru.kpfu.itis.adaptivetest.controllers.InvitationController;
import ru.kpfu.itis.adaptivetest.exceptions.NotFoundException;
import ru.kpfu.itis.adaptivetest.models.User;
import ru.kpfu.itis.adaptivetest.services.implementations.InvitationsServiceImpl;
import ru.kpfu.itis.adaptivetest.services.interfaces.InvitationsServiceInterface;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.AdditionalMatchers.not;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

/**
 * @author Bulat Giniyatullin
 * 27 April 2018
 */

public class InvitationControllerTest {
    private MockMvc mockMvc;
    private InvitationsServiceInterface mockedInvitationsService;

    @Before
    public void prepareMockMvcAndMockedService(){
        mockedInvitationsService = mock(InvitationsServiceImpl.class);
        try {
            doThrow(new NotFoundException(User.class))
                    .when(mockedInvitationsService).invite(not(eq("login")), any(Long.class));
        } catch (NotFoundException ignored) {}

        InvitationController invitationController = new InvitationController(mockedInvitationsService);

        mockMvc = standaloneSetup(invitationController).build();
    }

    @Test
    public void mustInvokeInviteMethodOfService() throws Exception {
        mockMvc.perform(post("/internal_api/invitations/invite")
                .param("test_id", "1")
                .param("login", "login"))
                .andExpect(status().isOk());

        ArgumentCaptor<Long> testIdArgumentCaptor = ArgumentCaptor.forClass(Long.class);
        ArgumentCaptor<String> loginArgumentCaptor = ArgumentCaptor.forClass(String.class);

        verify(mockedInvitationsService)
                .invite(loginArgumentCaptor.capture(), testIdArgumentCaptor.capture());

        assertThat(loginArgumentCaptor.getValue()).isEqualTo("login");
        assertThat(testIdArgumentCaptor.getValue()).isEqualTo(1L);
    }

    @Test
    public void mustReturnNotFoundIfUserLoginUnknown() throws Exception {
        mockMvc.perform(post("/internal_api/invitations/invite")
                .param("test_id", "1")
                .param("login", "unknownLogin"))
                .andExpect(status().isNotFound());
    }
}
