package ru.kpfu.itis.adaptivetest.myTestsTest;

import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.security.core.Authentication;
import org.springframework.test.context.junit4.SpringRunner;
import ru.kpfu.itis.adaptivetest.application.AdaptivetestApplication;
import ru.kpfu.itis.adaptivetest.models.Test;
import ru.kpfu.itis.adaptivetest.models.User;
import ru.kpfu.itis.adaptivetest.repositories.TestRepository;
import ru.kpfu.itis.adaptivetest.security.details.UserDetailsImpl;
import ru.kpfu.itis.adaptivetest.services.implementations.TestServiceImpl;

import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @author Rustem Saitgareev
 * 11-602
 * 000
 */
@RunWith(SpringRunner.class)
@SpringBootTest(
        classes = {AdaptivetestApplication.class}
)
public class ServiceTest {
    private static List<Test> initialData;

    @BeforeClass
    public static void prepareData() {
        initialData = new LinkedList<>();
        User author = User.builder()
                .id(1)
                .fio("author")
                .login("author")
                .build();
        Test test1 = Test.builder()
                .id(1)
                .author(author)
                .maxDuration(10)
                .name("Test name")
                .detailedDescription("Detailed description")
                .shortDescription("Short description")
                .maxScore(20)
                .passedTests(new LinkedList<>())
                .isAvailable(true)
                .isPrivate(false)
                .dateCreated(new Date(1521922433000L))
                .build();
        initialData.add(test1);
    }

    @org.junit.Test
    public void shouldReturnCorrectDataSet() {
        JpaSpecificationExecutor<Test> testRepository = mock(TestRepository.class);
        Specification<Test> testSpecification = any();
        when(testRepository.findAll(testSpecification, any(Pageable.class)))
                .thenReturn(new PageImpl<>(initialData));

        TestServiceImpl testService = new TestServiceImpl();
        testService.setTestRepository((TestRepository) testRepository);
        User user = new User();
        UserDetailsImpl userDetails = new UserDetailsImpl(user);
        Authentication auth = mock(Authentication.class);
        when(auth.getPrincipal()).thenReturn(userDetails);
        List<Map<String, Object>> result = testService.getMyTests(auth, null, null, null);
        List<Map<String, Object>> expectedResult = new ArrayList<>();

        Map<String, Object> test = new HashMap<>();
        test.put("id", initialData.get(0).getId());
        test.put("name", initialData.get(0).getName());
        test.put("short_description", initialData.get(0).getShortDescription());
        test.put("is_private", initialData.get(0).getIsPrivate());
        test.put("is_available", initialData.get(0).getIsAvailable());
        test.put("max_duration", initialData.get(0).getMaxDuration());
        test.put("max_score", initialData.get(0).getMaxScore());
        test.put("count_of_participants", initialData.get(0).getPassedTests().size());
        test.put("date_created", initialData.get(0).getTimeStampCreated());

        expectedResult.add(test);

        assertThat(result)
                .hasSameSizeAs(expectedResult)
                .containsAll(expectedResult);
    }
}
