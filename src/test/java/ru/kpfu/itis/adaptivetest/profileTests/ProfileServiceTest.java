package ru.kpfu.itis.adaptivetest.profileTests;

import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import ru.kpfu.itis.adaptivetest.forms.UserForm;
import ru.kpfu.itis.adaptivetest.models.User;
import ru.kpfu.itis.adaptivetest.repositories.UserRepository;
import ru.kpfu.itis.adaptivetest.services.implementations.ProfileServiceImpl;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

/**
 * @author Bulat Giniyatullin
 * 08 Апрель 2018
 */

public class ProfileServiceTest {
    private static ProfileServiceImpl profileService;

    private static void prepareRepositoryData() {
        User user = User.builder()
                .id(1)
                .login("testLogin")
                .fio("Surname Name")
                .email("test@email.com")
                .build();

        User user2 = User.builder()
                .id(2)
                .login("testLogin2")
                .fio("Surname Name")
                .email("test@email.com")
                .build();

        UserRepository userRepository = mock(UserRepository.class);
        doReturn(Optional.of(user)).when(userRepository).findById(any());
        doReturn(Optional.of(user2)).when(userRepository).findOneByLogin(any());

        profileService = new ProfileServiceImpl(userRepository);
    }

    @Test
    public void shouldReturnCorrectDataById() {
        prepareRepositoryData();
        Map<String, Object> expected = new HashMap<>();
        expected.put("id", 1L);
        expected.put("login", "testLogin");
        expected.put("fio", "Surname Name");
        expected.put("email", "test@email.com");

        //when
        Map<String, Object> data = profileService.getUserByIdOrLogin(1L, null);

        //expect
        assertThat(data)
                .hasSameSizeAs(expected)
                .containsAllEntriesOf(expected);
    }

    @Test
    public void shouldReturnCorrectDataByLogin() {
        prepareRepositoryData();
        Map<String, Object> expected = new HashMap<>();
        expected.put("id", 2L);
        expected.put("login", "testLogin2");
        expected.put("fio", "Surname Name");
        expected.put("email", "test@email.com");

        //when
        Map<String, Object> data = profileService.getUserByIdOrLogin(null, "testLogin2");

        //expect
        assertThat(data)
                .hasSameSizeAs(expected)
                .containsAllEntriesOf(expected);
    }

    @Test
    public void shouldInvokeSaveMethodOfRepoWithSendCorrectUserData() {
        UserRepository userRepository = mock(UserRepository.class);
        ArgumentCaptor<User> argumentCaptor = ArgumentCaptor.forClass(User.class);
        doReturn(null).when(userRepository).save(argumentCaptor.capture());

        User oldUser = User.builder()
                .id(1)
                .login("oldLogin")
                .fio("oldFio")
                .email("oldEmail")
                .password("oldPassword")
                .build();

        doReturn(Optional.of(oldUser)).when(userRepository).findById(any());

        profileService = new ProfileServiceImpl(userRepository);

        BCryptPasswordEncoder passwordEncoder = mock(BCryptPasswordEncoder.class);
        doAnswer(invocationOnMock -> invocationOnMock.<String>getArgument(0))
                .when(passwordEncoder).encode(anyString());

        profileService.setPasswordEncoder(passwordEncoder);

        UserForm user = UserForm.builder()
                .id(1L)
                .login("newLogin")
                .fio("newFio")
                .email("newEmail")
                .password("newPassword")
                .build();

        profileService.updateUserById(user);

        User capturedUser = argumentCaptor.getValue();

        assertThat(capturedUser)
                .hasFieldOrPropertyWithValue("id", 1L)
                .hasFieldOrPropertyWithValue("login", "newLogin")
                .hasFieldOrPropertyWithValue("fio", "newFio")
                .hasFieldOrPropertyWithValue("email", "newEmail")
                .hasFieldOrPropertyWithValue("password","newPassword");
    }

    @Test
    public void shouldInvokeSaveMethodOfRepoWithSendPartialDataIfSomeFieldsEmpty() {
        UserRepository userRepository = mock(UserRepository.class);
        ArgumentCaptor<User> argumentCaptor = ArgumentCaptor.forClass(User.class);
        doReturn(null).when(userRepository).save(argumentCaptor.capture());

        User oldUser = User.builder()
                .id(1)
                .login("oldLogin")
                .fio("oldFio")
                .email("oldEmail")
                .password("oldPassword")
                .build();

        doReturn(Optional.of(oldUser)).when(userRepository).findById(any());

        profileService = new ProfileServiceImpl(userRepository);

        UserForm user = UserForm.builder()
                .id(1L)
                .login("newLogin")
                .build();

        profileService.updateUserById(user);

        User capturedUser = argumentCaptor.getValue();

        assertThat(capturedUser)
                .hasFieldOrPropertyWithValue("id", 1L)
                .hasFieldOrPropertyWithValue("login", "newLogin")
                .hasFieldOrPropertyWithValue("fio", "oldFio")
                .hasFieldOrPropertyWithValue("email", "oldEmail")
                .hasFieldOrPropertyWithValue("password", "oldPassword");
    }
}
