package ru.kpfu.itis.adaptivetest.profileTests;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.validation.Errors;
import org.springframework.web.util.NestedServletException;
import ru.kpfu.itis.adaptivetest.application.AdaptivetestApplication;
import ru.kpfu.itis.adaptivetest.controllers.ProfileController;
import ru.kpfu.itis.adaptivetest.forms.UserForm;
import ru.kpfu.itis.adaptivetest.models.User;
import ru.kpfu.itis.adaptivetest.services.implementations.ProfileServiceImpl;
import ru.kpfu.itis.adaptivetest.services.interfaces.ProfileServiceInterface;
import ru.kpfu.itis.adaptivetest.validators.ProfileFormValidator;

import java.util.HashMap;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

/**
 * @author Bulat Giniyatullin
 * 08 Апрель 2018
 */

@RunWith(SpringRunner.class)
@SpringBootTest(
        classes = {AdaptivetestApplication.class}
)
public class ProfileControllerTest {
    private ProfileController profileController;

    private MockMvc mockMvc;

    private void prepareData() {
        Map<String, Object> userData = new HashMap<>();
        userData.put("id", 1);
        userData.put("login", "testLogin");
        userData.put("fio", "Surname Name");
        userData.put("e-mail", "test@email.com");

        ProfileServiceInterface profileService = mock(ProfileServiceImpl.class);
        when(profileService.getUserByIdOrLogin(any(), any()))
                .thenReturn(userData);

        ProfileFormValidator profileFormValidator = mock(ProfileFormValidator.class);

        doReturn(true).when(profileFormValidator).supports(any());
        doNothing().when(profileFormValidator).validate(any(), any());

        profileController = new ProfileController(profileService, profileFormValidator);
    }

    private void prepareMockMvc() {
        if (profileController == null) {
            profileController = new ProfileController(
                    mock(ProfileServiceImpl.class),
                    mock(ProfileFormValidator.class));
        }
        mockMvc = standaloneSetup(profileController).build();
    }

    @Test
    @WithUserDetails(value = "testLogin", userDetailsServiceBeanName = "userDetailsService1")
    public void shouldReturnCorrectJSONById() throws Exception {
        prepareData();
        prepareMockMvc();
        mockMvc.perform(get("/internal_api/profile/1"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(1)))
                .andExpect(jsonPath("$.login", is("testLogin")))
                .andExpect(jsonPath("$.fio", is("Surname Name")))
                .andExpect(jsonPath("$.e-mail", is("test@email.com")));
    }

    @Test
    @WithUserDetails(value = "testLogin", userDetailsServiceBeanName = "userDetailsService1")
    public void shouldReturnCorrectJSONByLogin() throws Exception {
        prepareData();
        prepareMockMvc();
        mockMvc.perform(get("/internal_api/profile/testLogin"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(1)))
                .andExpect(jsonPath("$.login", is("testLogin")))
                .andExpect(jsonPath("$.fio", is("Surname Name")))
                .andExpect(jsonPath("$.e-mail", is("test@email.com")));
    }

    @Test
    @WithUserDetails(value = "testLogin", userDetailsServiceBeanName = "userDetailsService1")
    public void shouldReturnForbiddenIfIncorrectId() throws Exception {
        prepareMockMvc();
        try {
            mockMvc.perform(get("/internal_api/profile/2"))
                    .andExpect(status().isForbidden());
        } catch (NestedServletException ignored) {}
    }

    @Test
    @WithUserDetails(value = "testLogin", userDetailsServiceBeanName = "userDetailsService1")
    public void shouldReturnForbiddenIfIncorrectLogin() throws Exception {
        prepareMockMvc();
        try {
            mockMvc.perform(get("/internal_api/profile/incorrectLogin"))
                    .andExpect(status().isForbidden());
        } catch (NestedServletException ignored) {}
    }

    @Test
    @WithUserDetails(value = "testLogin", userDetailsServiceBeanName = "userDetailsService1")
    public void shouldReturnForbiddenIfPostOnIncorrectId() throws Exception {
        prepareMockMvc();
        try {
            mockMvc.perform(post("/internal_api/profile/1"))
                    .andExpect(status().isForbidden());
        } catch (NestedServletException ignored) {}
    }

    @Test
    @WithUserDetails(value = "testLogin", userDetailsServiceBeanName = "userDetailsService1")
    public void shouldSetErrorCodeInHeadersAndReturnBadRequestResponseIfInvalidData() throws Exception {
        // prepare validator
        ProfileFormValidator profileFormValidator = mock(ProfileFormValidator.class);
        doReturn(true).when(profileFormValidator).supports(any());
        doAnswer(invocationOnMock -> {
            Errors errors = invocationOnMock.getArgument(1);
            errors.reject("test.error", "Test error");
            return null;
        }).when(profileFormValidator).validate(any(), any(Errors.class));

        profileController = new ProfileController(mock(ProfileServiceImpl.class), profileFormValidator);

        prepareMockMvc();

        //assert
        try {
            mockMvc.perform(post("/internal_api/profile/1"))
                    .andExpect(status().isBadRequest())
                    .andExpect(header().string("error_code", "test.error"));
        } catch (NestedServletException ignored) {}
    }

    @Test
    @WithUserDetails(value = "testLogin", userDetailsServiceBeanName = "userDetailsService1")
    public void shouldInvokeUpdateMethodOfProfileService() throws Exception {
        ProfileServiceInterface profileService = mock(ProfileServiceImpl.class);
        ArgumentCaptor<UserForm> argumentCaptor = ArgumentCaptor.forClass(UserForm.class);
        doNothing().when(profileService).updateUserById(argumentCaptor.capture());
        ProfileFormValidator profileFormValidator = mock(ProfileFormValidator.class);
        doReturn(true).when(profileFormValidator).supports(any());

        profileController = new ProfileController(profileService, profileFormValidator);

        prepareMockMvc();

        mockMvc.perform(post("/internal_api/profile/1")
                .param("id", "1")
                .param("login", "login")
                .param("fio", "fio")
                .param("email", "email")
                .param("password", "password"));

        assertThat(argumentCaptor.getValue())
                .hasFieldOrPropertyWithValue("id", 1L)
                .hasFieldOrPropertyWithValue("login", "login")
                .hasFieldOrPropertyWithValue("fio", "fio")
                .hasFieldOrPropertyWithValue("email", "email")
                .hasFieldOrPropertyWithValue("password", "password");
    }
}
