package ru.kpfu.itis.adaptivetest.LevelCreationTest;

import org.junit.Test;
import org.mockito.ArgumentCaptor;
import ru.kpfu.itis.adaptivetest.exceptions.NotFoundException;
import ru.kpfu.itis.adaptivetest.forms.AdaptedQuestionForm;
import ru.kpfu.itis.adaptivetest.models.AdaptedQuestion;
import ru.kpfu.itis.adaptivetest.models.Question;
import ru.kpfu.itis.adaptivetest.repositories.AdaptedQuestionRepository;
import ru.kpfu.itis.adaptivetest.repositories.QuestionRepository;
import ru.kpfu.itis.adaptivetest.services.implementations.LevelServiceImpl;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class LevelCreationServiceTest {
    @Test(expected = NotFoundException.class)
    public void shouldReturnNotFoundException() throws NotFoundException{
        AdaptedQuestionRepository adaptedQuestionRepository = mock(AdaptedQuestionRepository.class);
        QuestionRepository questionRepository = mock(QuestionRepository.class);
        when(questionRepository.findById(any())).thenReturn(Optional.empty());
        LevelServiceImpl levelService = new LevelServiceImpl(adaptedQuestionRepository);
        levelService.setQuestionRepository(questionRepository);
        levelService.createLevel(new AdaptedQuestionForm());
    }

    @Test
    public void shouldSaveAdaptedQuestionWithCorrectData() throws NotFoundException{
        AdaptedQuestionRepository adaptedQuestionRepository = mock(AdaptedQuestionRepository.class);

        //нужен для того чтобы узнать, с какими именно атрибутами мы собираемся сохранять объект AvailableTest
        ArgumentCaptor<AdaptedQuestion> questionArgumentCaptor = ArgumentCaptor.forClass(AdaptedQuestion.class);

        Question question = Question.builder().id(1).build();
        QuestionRepository questionRepository = mock(QuestionRepository.class);
        when(questionRepository.findById(any())).thenReturn(Optional.of(question));

        //мокируем метод save у репозитория, чтобы он не выполнялся
        doReturn(null).when(adaptedQuestionRepository).save(questionArgumentCaptor.capture());

        //говорим, чтобы репозиторий возвращал 1 на запрос о количестве уже созданных уровней для данного вопроса
        doReturn(1).when(adaptedQuestionRepository).countAdaptedQuestionByQuestionId(question.getId());

        AdaptedQuestionForm form = new AdaptedQuestionForm();
        form.setQuestion_id(1);
        form.setRight_answer("answer");
        form.setScore(5);
        form.setText("text");

        LevelServiceImpl levelService = new LevelServiceImpl(adaptedQuestionRepository);
        levelService.setQuestionRepository(questionRepository);

        levelService.createLevel(form);
        AdaptedQuestion adaptedQuestion = questionArgumentCaptor.getValue();
        assertThat(adaptedQuestion)
                .hasFieldOrPropertyWithValue("question", question)
                .hasFieldOrPropertyWithValue("rightAnswer", "answer")
                .hasFieldOrPropertyWithValue("text", "text")
                .hasFieldOrPropertyWithValue("score", 5)
                .hasFieldOrPropertyWithValue("level", 2);//так как репозиторий вернул 1 на запрос о количестве уже созданных уровней,
                                                                    // этот уровень должен быть вторым
    }
}
