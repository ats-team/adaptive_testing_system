package ru.kpfu.itis.adaptivetest.LevelCreationTest;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.validation.Errors;
import ru.kpfu.itis.adaptivetest.application.AdaptivetestApplication;
import ru.kpfu.itis.adaptivetest.controllers.LevelsController;
import ru.kpfu.itis.adaptivetest.exceptions.NotFoundException;
import ru.kpfu.itis.adaptivetest.forms.AdaptedQuestionForm;
import ru.kpfu.itis.adaptivetest.models.User;
import ru.kpfu.itis.adaptivetest.services.implementations.LevelServiceImpl;
import ru.kpfu.itis.adaptivetest.services.implementations.QuestionServiceImpl;
import ru.kpfu.itis.adaptivetest.validators.AdaptedQuestionValidator;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

@RunWith(SpringRunner.class)
@SpringBootTest(
        classes = {AdaptivetestApplication.class}
)
public class LevelCreationControllerTest {
    private MockMvc mockMvc;

    @Test
    @WithUserDetails(value = "testLogin", userDetailsServiceBeanName = "userDetailsService1")
    public void shouldSetErrorCodeInHeaderAndReturnResponseWithStatus400() {
        AdaptedQuestionValidator adaptedQuestionValidator = mock(AdaptedQuestionValidator.class);
        when(adaptedQuestionValidator.supports(any())).thenReturn(true);
        doAnswer(invocationOnMock -> {
            Errors errors = invocationOnMock.getArgument(1);
            errors.reject("empty.text", "empty text");
            return null;
        }).when(adaptedQuestionValidator).validate(any(), any(Errors.class));

        LevelsController levelsController = new LevelsController(mock(LevelServiceImpl.class), adaptedQuestionValidator);
        //levelsController.setAdaptedQuestionValidator(adaptedQuestionValidator);

        mockMvc = standaloneSetup(levelsController).build();

        try {
            mockMvc.perform(post("/internal_api/levels"))
                    .andExpect(status().isBadRequest())
                    .andExpect(header().string("error_code", "empty.text"));
        } catch (Exception e) {}
    }

    @Test
    @WithUserDetails(value = "testLogin", userDetailsServiceBeanName = "userDetailsService1")
    public  void shouldReturnResponseWithStatus403() throws NotFoundException{
        AdaptedQuestionValidator adaptedQuestionValidator = mock(AdaptedQuestionValidator.class);
        when(adaptedQuestionValidator.supports(any())).thenReturn(true);
        doNothing().when(adaptedQuestionValidator).validate(any(), any(Errors.class));
        User author = User.builder().id(2).build();
        QuestionServiceImpl questionService = mock(QuestionServiceImpl.class);
        doReturn(author).when(questionService).getAuthorOfTestByQuestionId(any());
        LevelsController levelsController = new LevelsController(mock(LevelServiceImpl.class), adaptedQuestionValidator);
        levelsController.setQuestionService(questionService);

        mockMvc = standaloneSetup(levelsController).build();

        try {
            mockMvc.perform(post("/internal_api/levels")
                    .param("question_id", "1"))
                    .andExpect(status().isForbidden());
        } catch (Exception e) {}
    }

    @Test
    @WithUserDetails(value = "testLogin", userDetailsServiceBeanName = "userDetailsService1")
    public void shouldInvokeCreateMethodOfServiceWithRightParams() throws NotFoundException{
        AdaptedQuestionValidator adaptedQuestionValidator = mock(AdaptedQuestionValidator.class);
        when(adaptedQuestionValidator.supports(any())).thenReturn(true);
        doNothing().when(adaptedQuestionValidator).validate(any(), any(Errors.class));
        User author = User.builder().id(1).build();
        QuestionServiceImpl questionService = mock(QuestionServiceImpl.class);
        doReturn(author).when(questionService).getAuthorOfTestByQuestionId(any());
        LevelServiceImpl levelService = mock(LevelServiceImpl.class);
        ArgumentCaptor<AdaptedQuestionForm> adaptedQuestionArgumentCaptor = ArgumentCaptor.forClass(AdaptedQuestionForm.class);

        doNothing().when(levelService).createLevel(adaptedQuestionArgumentCaptor.capture());

        LevelsController levelsController = new LevelsController(levelService, adaptedQuestionValidator);
        levelsController.setQuestionService(questionService);

        mockMvc = standaloneSetup(levelsController).build();

        try {
            mockMvc.perform(post("/internal_api/levels")
                    .param("question_id", "1")
                    .param("text", "text")
                    .param("right_answer", "answer")
                    .param("score", "5")
            );
            assertThat(adaptedQuestionArgumentCaptor.getValue())
                    .hasFieldOrPropertyWithValue("question_id", 1L)
                    .hasFieldOrPropertyWithValue("text", "text")
                    .hasFieldOrPropertyWithValue("right_answer", "answer")
                    .hasFieldOrPropertyWithValue("score", 5);
        } catch (Exception e) {}
    }
}
