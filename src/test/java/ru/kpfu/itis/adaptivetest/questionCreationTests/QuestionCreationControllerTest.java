package ru.kpfu.itis.adaptivetest.questionCreationTests;

import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.springframework.test.web.servlet.MockMvc;
import ru.kpfu.itis.adaptivetest.controllers.QuestionController;
import ru.kpfu.itis.adaptivetest.exceptions.NotFoundException;
import ru.kpfu.itis.adaptivetest.forms.QuestionCreationForm;
import ru.kpfu.itis.adaptivetest.services.implementations.QuestionServiceImpl;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.AdditionalMatchers.not;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

/**
 * @author Bulat Giniyatullin
 * 13 April 2018
 */

public class QuestionCreationControllerTest {
    private static MockMvc mockMvc;
    private static ArgumentCaptor<QuestionCreationForm> serviceCreationMethodArgumentCaptor;

    @BeforeClass
    public static void prepareMockMvc() {
        QuestionServiceImpl questionService = mock(QuestionServiceImpl.class);
        serviceCreationMethodArgumentCaptor = ArgumentCaptor.forClass(QuestionCreationForm.class);

        try {
            doNothing().when(questionService)
                    .createQuestion(serviceCreationMethodArgumentCaptor.capture());
            doThrow(new NotFoundException(ru.kpfu.itis.adaptivetest.models.Test.class))
                    .when(questionService)
                    .createQuestion(not(eq(QuestionCreationForm.builder()
                            .name("name")
                            .testId(1L)
                            .build())));
        } catch (NotFoundException e) {
            e.printStackTrace();
        }

        QuestionController questionController = new QuestionController(questionService);

        mockMvc = standaloneSetup(questionController).build();
    }

    @Test
    public void shouldReturnOkHttpCodeIfTestIdIsCorrect() throws Exception {
        mockMvc.perform(post("/internal_api/questions/create")
                .param("testId", "1")
                .param("name", "name"))
                .andExpect(status().isOk());
    }

    @Test
    public void shouldInvokeServiceCreationMethodIfTestIdIsCorrect() throws Exception {
        mockMvc.perform(post("/internal_api/questions/create")
                .param("testId", "1")
                .param("name", "name"));

        assertThat(serviceCreationMethodArgumentCaptor.getValue())
                .isEqualTo(QuestionCreationForm.builder()
                        .name("name")
                        .testId(1L)
                        .build());
    }

    @Test
    public void shouldReturnBadRequestCodeAndSetErrorHeaderIfTestIdIdIncorrect() throws Exception {
        mockMvc.perform(post("/internal_api/questions/create")
                .param("testId", "0"))
                .andExpect(status().isNotFound())
                .andExpect(header().string("error_code", "not_found.test"));
    }
}
