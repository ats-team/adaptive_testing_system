package ru.kpfu.itis.adaptivetest.questionCreationTests;

import org.junit.BeforeClass;
import org.mockito.ArgumentCaptor;
import ru.kpfu.itis.adaptivetest.exceptions.NotFoundException;
import ru.kpfu.itis.adaptivetest.forms.QuestionCreationForm;
import ru.kpfu.itis.adaptivetest.models.Question;
import ru.kpfu.itis.adaptivetest.models.Test;
import ru.kpfu.itis.adaptivetest.repositories.QuestionRepository;
import ru.kpfu.itis.adaptivetest.repositories.TestRepository;
import ru.kpfu.itis.adaptivetest.services.implementations.QuestionServiceImpl;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.fail;
import static org.mockito.AdditionalMatchers.not;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;

/**
 * @author Bulat Giniyatullin
 * 13 April 2018
 */

public class QuestionCreationServiceTest {
    private static QuestionServiceImpl questionService;
    private static ArgumentCaptor<Question> questionRepositorySaveMethodArgumentCaptor;

    @BeforeClass
    public static void prepareQuestionService() {
        Test test = Test.builder()
                .id(1)
                .name("testName")
                .build();
        TestRepository testRepository = mock(TestRepository.class);
        doReturn(Optional.of(test)).when(testRepository).findById(1L);
        doReturn(Optional.empty()).when(testRepository).findById(not(eq(1L)));

        QuestionRepository questionRepository = mock(QuestionRepository.class);
        questionRepositorySaveMethodArgumentCaptor = ArgumentCaptor.forClass(Question.class);
        doReturn(null).when(questionRepository)
                .save(questionRepositorySaveMethodArgumentCaptor.capture());

        questionService = new QuestionServiceImpl(questionRepository, testRepository);
    }

    @org.junit.Test
    public void shouldInvokeRepositorySaveMethodIfIdIsCorrect() {
        Question expected = Question.builder()
                .name("name")
                .test(Test.builder()
                        .id(1L)
                        .build())
                .build();

        try {
            questionService.createQuestion(QuestionCreationForm.builder()
                    .name("name")
                    .testId(1L)
                    .build());

            assertThat(questionRepositorySaveMethodArgumentCaptor.getValue())
                    .isEqualToComparingFieldByFieldRecursively(expected);

        } catch (NotFoundException e) {
            fail();
        }
    }

    @org.junit.Test(expected = NotFoundException.class)
    public void shouldThrowNotFoundExceptionIfTestIdIsIncorrect() throws NotFoundException {
        questionService.createQuestion(QuestionCreationForm.builder()
                .name("name")
                .testId(0L)
                .build());
    }
}
