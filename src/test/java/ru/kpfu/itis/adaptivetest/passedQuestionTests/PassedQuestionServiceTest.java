package ru.kpfu.itis.adaptivetest.passedQuestionTests;

import org.junit.BeforeClass;
import ru.kpfu.itis.adaptivetest.models.*;
import ru.kpfu.itis.adaptivetest.repositories.PassedQuestionRepository;
import ru.kpfu.itis.adaptivetest.services.implementations.PassedQuestionServiceImpl;
import ru.kpfu.itis.adaptivetest.services.interfaces.PassedQuestionServiceInterface;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.AdditionalMatchers.not;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;

/**
 * @author Bulat Giniyatullin
 * 25 April 2018
 */

public class PassedQuestionServiceTest {
    private static PassedQuestionServiceInterface passedQuestionService;

    @BeforeClass
    public static void prepareQuestionService() {
        Test test = Test.builder()
                .id(1)
                .name("testName")
                .build();
        Question question = Question.builder()
                .id(1)
                .name("name")
                .levelCount(1)
                .test(test)
                .build();
        AdaptedQuestion adaptedQuestion = AdaptedQuestion.builder()
                .id(1)
                .question(question)
                .rightAnswer("rightAnswer")
                .text("text")
                .photoPath("photo")
                .filePath("file")
                .build();
        PassedTest passedTest = PassedTest.builder()
                .id(1)
                .test(test)
                .build();
        PassedQuestion passedQuestion = PassedQuestion.builder()
                .id(1)
                .answer("answer")
                .adaptedQuestion(adaptedQuestion)
                .passedTest(passedTest)
                .build();

        PassedQuestionRepository passedQuestionRepository = mock(PassedQuestionRepository.class);
        doReturn(Optional.of(passedQuestion)).when(passedQuestionRepository)
                .findById(1L);
        doReturn(Optional.empty()).when(passedQuestionRepository)
                .findById(not(eq(1L)));

        passedQuestionService = new PassedQuestionServiceImpl(passedQuestionRepository);
    }

    @org.junit.Test
    public void shouldReturnCorrectData() {
        Map<String, Object> expected = new HashMap<>();
        expected.put("passed_question_id", 1L);
        expected.put("test_name", "testName");
        expected.put("question_name", "name");
        expected.put("picture_path", "photo");
        expected.put("file_path", "file");
        expected.put("text", "text");
        expected.put("user_answer", "answer");
        expected.put("right_answer", "rightAnswer");

        Map<String, Object> received = passedQuestionService.getPassedQuestionById(1L);

        assertThat(received)
                .hasSameSizeAs(expected)
                .containsAllEntriesOf(expected);
    }

    @org.junit.Test
    public void shouldReturnEmptyMapIfIdIsIncorrect() {
        Map<String, Object> received = passedQuestionService.getPassedQuestionById(0L);

        assertThat(received)
                .isEmpty();
    }
}
