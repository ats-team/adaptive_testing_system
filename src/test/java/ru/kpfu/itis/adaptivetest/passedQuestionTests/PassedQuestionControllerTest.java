package ru.kpfu.itis.adaptivetest.passedQuestionTests;

import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import ru.kpfu.itis.adaptivetest.controllers.PassedQuestionController;
import ru.kpfu.itis.adaptivetest.services.implementations.PassedQuestionServiceImpl;
import ru.kpfu.itis.adaptivetest.services.interfaces.PassedQuestionServiceInterface;

import java.util.HashMap;
import java.util.Map;

import static org.hamcrest.Matchers.is;
import static org.mockito.AdditionalMatchers.not;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

/**
 * @author Bulat Giniyatullin
 * 25 April 2018
 */

public class PassedQuestionControllerTest {
    private static MockMvc mockMvc;

    @BeforeClass
    public static void prepareMockMvc() {
        PassedQuestionServiceInterface passedQuestionService =
                mock(PassedQuestionServiceImpl.class);
        Map<String, Object> data = new HashMap<>();
        data.put("passed_question_id", 1L);
        data.put("test_name", "testName");
        data.put("picture_path", "photo");
        data.put("file_path", "file");
        data.put("text", "text");
        data.put("user_answer", "answer");
        data.put("right_answer", "rightAnswer");

        doReturn(data).when(passedQuestionService).getPassedQuestionById(1L);
        doReturn(new HashMap<>()).when(passedQuestionService).getPassedQuestionById(not(eq(1L)));

        PassedQuestionController passedQuestionController =
                new PassedQuestionController(passedQuestionService);

        mockMvc = standaloneSetup(passedQuestionController).build();
    }

    @Test
    public void shouldReturnCorrectJson() throws Exception {
        mockMvc.perform(get("/internal_api/passed_questions/1"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.passed_question_id", is(1)))
                .andExpect(jsonPath("$.test_name", is("testName")))
                .andExpect(jsonPath("$.picture_path", is("photo")))
                .andExpect(jsonPath("$.file_path", is("file")))
                .andExpect(jsonPath("$.text", is("text")))
                .andExpect(jsonPath("$.user_answer", is("answer")))
                .andExpect(jsonPath("$.right_answer", is("rightAnswer")));
    }

    @Test
    public void shouldReturnEmptyJsonIfIdIsIncorrect() throws Exception {
        mockMvc.perform(get("/internal_api/passed_questions/0"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(content().string("{}"));
    }
}
