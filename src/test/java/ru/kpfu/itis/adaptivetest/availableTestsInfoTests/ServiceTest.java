package ru.kpfu.itis.adaptivetest.availableTestsInfoTests;

import org.junit.BeforeClass;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.security.core.Authentication;
import ru.kpfu.itis.adaptivetest.models.AvailableTest;
import ru.kpfu.itis.adaptivetest.models.Test;
import ru.kpfu.itis.adaptivetest.models.User;
import ru.kpfu.itis.adaptivetest.repositories.AvailableTestRepository;
import ru.kpfu.itis.adaptivetest.security.details.UserDetailsImpl;
import ru.kpfu.itis.adaptivetest.services.implementations.TestServiceImpl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ServiceTest {
    private static List<AvailableTest> initialData;

    @BeforeClass
    public static void prepareData() {
        initialData = new ArrayList<>();
        User author = new User();
        author.setFio("FIO");
        author.setLogin("author login");

        User student = new User();

        Test test = new Test();
        test.setAuthor(author);
        test.setId(1);
        test.setMaxDuration(10);
        test.setName("Test name");
        test.setShortDescription("Short description");

        AvailableTest testForStudent = new AvailableTest();
        testForStudent.setTest(test);
        testForStudent.setUser(student);

        initialData.add(testForStudent);
    }

    @org.junit.Test
    public void shouldReturnCorrectDataSet() {
        JpaSpecificationExecutor<AvailableTest> testRepository = mock(AvailableTestRepository.class);
        Specification<AvailableTest> availableTestSpecification = any();
        when(testRepository.findAll(availableTestSpecification, any(Pageable.class)))
                .thenReturn(new PageImpl<>(initialData));

        TestServiceImpl testService = new TestServiceImpl();
        testService.setAvailableTestRepository((AvailableTestRepository) testRepository);
        User user = new User();
        UserDetailsImpl userDetails = new UserDetailsImpl(user);
        Authentication auth = mock(Authentication.class);
        when(auth.getPrincipal()).thenReturn(userDetails);
        List<Map<String, Object>> result = testService.getAvailableTests(auth, null, null, null, null, null);

        List<Map<String, Object>> expectedResult = new ArrayList<>();

        Map<String, Object> test = new HashMap<>();
        test.put("id", 1L);
        test.put("name", "Test name");
        test.put("short_description", "Short description" );
        test.put("creator_login", "author login");
        test.put("creator_fio", "FIO");
        test.put("max_duration", 10);

        expectedResult.add(test);

        assertThat(result)
                .hasSameSizeAs(expectedResult)
                .containsAll(expectedResult);
    }
}
