package ru.kpfu.itis.adaptivetest.availableTestsInfoTests;

import org.springframework.test.web.servlet.MockMvc;
import ru.kpfu.itis.adaptivetest.controllers.TestInfoController;
import ru.kpfu.itis.adaptivetest.services.implementations.TestServiceImpl;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

public class ControllerTest {
    private Map<String, Object> getSimpleTestMap(int number) {
        String numberString = Integer.toString(number);
        Map<String, Object> test = new HashMap<>();
        test.put("id", number);
        test.put("name", "Test" + numberString);
        test.put("short_description", "Short description of test" + numberString);
        test.put("creator_login", "login of author" + numberString);
        test.put("creator_fio", "FIO of author" + numberString);
        test.put("max_duration", 600);
        return test;
    }


    @org.junit.Test
    public void shouldReturnRightJSON() throws Exception{
        List<Map<String, Object>> testData = new ArrayList<>();
        for (int i = 0; i < 2; i++) {
            testData.add(getSimpleTestMap(i));
        }

        TestServiceImpl testService = mock(TestServiceImpl.class);
        when(testService.getAvailableTests(any(), any(), any(), any(), any(), any())).thenReturn(testData);
        TestInfoController controller = new TestInfoController(testService);

        MockMvc mockMvc = standaloneSetup(controller).build();
        mockMvc.perform(get("/internal_api/tests/available"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].id", is(0)))
                .andExpect(jsonPath("$[0].name", is("Test0")))
                .andExpect(jsonPath("$[0].short_description", is("Short description of test0")))
                .andExpect(jsonPath("$[0].creator_login", is("login of author0")))
                .andExpect(jsonPath("$[0].creator_fio", is("FIO of author0")))
                .andExpect(jsonPath("$[0].max_duration", is(600)))

                .andExpect(jsonPath("$[1].id", is(1)))
                .andExpect(jsonPath("$[1].name", is("Test1")))
                .andExpect(jsonPath("$[1].short_description", is("Short description of test1")))
                .andExpect(jsonPath("$[1].creator_login", is("login of author1")))
                .andExpect(jsonPath("$[1].creator_fio", is("FIO of author1")))
                .andExpect(jsonPath("$[1].max_duration", is(600)))
        ;
    }
}
