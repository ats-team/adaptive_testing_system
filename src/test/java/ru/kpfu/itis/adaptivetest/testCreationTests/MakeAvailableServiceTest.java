package ru.kpfu.itis.adaptivetest.testCreationTests;

import org.junit.Before;
import ru.kpfu.itis.adaptivetest.exceptions.BadQuestionsException;
import ru.kpfu.itis.adaptivetest.models.AdaptedQuestion;
import ru.kpfu.itis.adaptivetest.models.Question;
import ru.kpfu.itis.adaptivetest.models.Test;
import ru.kpfu.itis.adaptivetest.repositories.TestRepository;
import ru.kpfu.itis.adaptivetest.services.implementations.TestServiceImpl;

import java.util.LinkedList;
import java.util.List;

import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.mockito.Mockito.mock;

/**
 * @author Rustem Saitgareev
 * 11-602
 * 000
 */
public class MakeAvailableServiceTest {
    private Test test = Test.builder()
            .id(1)
            .isAvailable(false)
            .build();
    private Question q1 = Question.builder()
            .test(test)
            .adaptedQuestions(new LinkedList<>())
            .build();
    private TestServiceImpl testService;

    @Before
    public void init() {
        List<Question> questions = new LinkedList<>();
        questions.add(q1);
        test.setQuestions(questions);

        TestRepository testRepository = mock(TestRepository.class);
        testService = new TestServiceImpl();
        testService.setTestRepository(testRepository);
    }

    @org.junit.Test(expected = BadQuestionsException.class)
    public void shouldThrowExceptionIfQuestionHasNoLevels() throws Exception {
        testService.makeAvailable(test);
    }

    @org.junit.Test
    public void shouldSetAvailableIfTestIsOk() throws Exception {
        AdaptedQuestion aq1 = AdaptedQuestion.builder()
                .question(q1)
                .score(5)
                .build();
        AdaptedQuestion aq2 = AdaptedQuestion.builder()
                .question(q1)
                .score(2)
                .build();
        List<AdaptedQuestion> adaptedQuestions = new LinkedList<>();
        adaptedQuestions.add(aq1);
        adaptedQuestions.add(aq2);
        q1.setAdaptedQuestions(adaptedQuestions);

        testService.makeAvailable(test);
        assertThat(test)
                .hasFieldOrPropertyWithValue("isAvailable", true)
                .hasFieldOrPropertyWithValue("maxScore", 5);
    }
}
