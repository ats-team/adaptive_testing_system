package ru.kpfu.itis.adaptivetest.testCreationTests;

import org.junit.Before;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import ru.kpfu.itis.adaptivetest.application.AdaptivetestApplication;
import ru.kpfu.itis.adaptivetest.forms.TestCreationForm;
import ru.kpfu.itis.adaptivetest.models.Question;
import ru.kpfu.itis.adaptivetest.models.Test;
import ru.kpfu.itis.adaptivetest.models.User;
import ru.kpfu.itis.adaptivetest.repositories.AvailableTestRepository;
import ru.kpfu.itis.adaptivetest.repositories.QuestionRepository;
import ru.kpfu.itis.adaptivetest.repositories.TestRepository;
import ru.kpfu.itis.adaptivetest.services.implementations.TestServiceImpl;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.mockito.Mockito.*;

/**
 * @author Rustem Saitgareev
 * 11-602
 * 000
 */
@RunWith(SpringRunner.class)
@SpringBootTest(
        classes = {AdaptivetestApplication.class}
)
public class ServiceTest {
    private TestCreationForm testCreationForm;
    private User user;
    private Test test;
    private Question q1;
    private Question q2;
    private ArgumentCaptor<Test> testArgumentCaptor;
    private TestServiceImpl testService;

    @Before
    public void init() {
        testCreationForm = TestCreationForm.builder()
                .name("test_name")
                .shortDescription("short_description")
                .detailedDescription("detailed_description")
                .maxDuration(10)
                .isPrivate("isPrivate")
                .isQuestionsDetailsAvailable("")
                .build();
        user = User.builder()
                .id(1L)
                .build();
        q1 = Question.builder()
                .id(1)
                .name("name1")
                .levelCount(2)
                .build();
        q2 = Question.builder()
                .id(2)
                .name("name2")
                .levelCount(2)
                .build();
        List<Question> questions = new LinkedList<>();
        questions.add(q1);
        questions.add(q2);
        test = Test.builder()
                .id(2L)
                .name("old_test_name")
                .shortDescription("old_short_description")
                .detailedDescription("old_detailed_description")
                .maxDuration(5)
                .isPrivate(false)
                .isQuestionsDetailsAvailable(true)
                .author(user)
                .questions(questions)
                .build();

        testArgumentCaptor = ArgumentCaptor.forClass(Test.class);
        TestRepository testRepository = mock(TestRepository.class);
        doReturn(test).when(testRepository).save(testArgumentCaptor.capture());
        doReturn(test).when(testRepository).getOne(any());
        QuestionRepository questionRepository = mock(QuestionRepository.class);
        AvailableTestRepository availableTestRepository = mock(AvailableTestRepository.class);

        testService = new TestServiceImpl();
        testService.setTestRepository(testRepository);
        testService.setAvailableTestRepository(availableTestRepository);
        testService.setQuestionRepository(questionRepository);
    }

    @org.junit.Test
    public void shouldInvokeSaveMethodOfRepositoryWithRightArgumentsWhileCreating() {
        Map<String, Object> expected = new HashMap<>();
        expected.put("id", test.getId());
        Map<String, Object> actual = testService.create(testCreationForm, user);
        assertThat(testArgumentCaptor.getValue())
                .hasFieldOrPropertyWithValue("name", "test_name")
                .hasFieldOrPropertyWithValue("shortDescription", "short_description")
                .hasFieldOrPropertyWithValue("detailedDescription", "detailed_description")
                .hasFieldOrPropertyWithValue("maxDuration", 10)
                .hasFieldOrPropertyWithValue("isPrivate", true)
                .hasFieldOrPropertyWithValue("author", user);
        assertThat(actual)
                .hasSameClassAs(expected)
                .containsAllEntriesOf(expected);
    }

    @org.junit.Test
    public void shouldInvokeSaveMethodOfRepositoryWithRightArgumentsWhileEditing() {
        testService.edit(test, testCreationForm);
        assertThat(testArgumentCaptor.getValue())
                .hasFieldOrPropertyWithValue("id", 2L)
                .hasFieldOrPropertyWithValue("name", "test_name")
                .hasFieldOrPropertyWithValue("shortDescription", "short_description")
                .hasFieldOrPropertyWithValue("detailedDescription", "detailed_description")
                .hasFieldOrPropertyWithValue("maxDuration", 10)
                .hasFieldOrPropertyWithValue("isPrivate", true)
                .hasFieldOrPropertyWithValue("author", user)
                .hasFieldOrPropertyWithValue("questions", test.getQuestions());
    }

    @org.junit.Test
    public void shouldReturnCorrectMapForEditPage() {
        Map<String, Object> expected = new HashMap<>();
        expected.put("test_id", test.getId());
        expected.put("test_name", test.getName());
        expected.put("short_description", test.getShortDescription());
        expected.put("detailed_description", test.getDetailedDescription());
        expected.put("max_duration", test.getMaxDuration());
        expected.put("is_private", test.getIsPrivate());
        expected.put("is_questions_details_available", test.getIsQuestionsDetailsAvailable());
        List<Map<String, Object>> questions = new LinkedList<>();
        for (Question question : test.getQuestions()) {
            Map<String, Object> questionMap = new HashMap<>();
            questionMap.put("question_id", question.getId());
            questionMap.put("question_name", question.getName());
            questionMap.put("level_count", question.getLevelCount());
            questions.add(questionMap);
        }
        expected.put("questions", questions);
        Map<String, Object> received = testService.getTestInfoForEditPage(2L);
        assertThat(received)
                .hasSameSizeAs(expected)
                .containsAllEntriesOf(expected);
    }
}
