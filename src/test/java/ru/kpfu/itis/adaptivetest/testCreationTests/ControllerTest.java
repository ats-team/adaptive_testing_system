package ru.kpfu.itis.adaptivetest.testCreationTests;

import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.validation.Errors;
import ru.kpfu.itis.adaptivetest.application.AdaptivetestApplication;
import ru.kpfu.itis.adaptivetest.controllers.TestCreateController;
import ru.kpfu.itis.adaptivetest.forms.TestCreationForm;
import ru.kpfu.itis.adaptivetest.models.Test;
import ru.kpfu.itis.adaptivetest.models.User;
import ru.kpfu.itis.adaptivetest.services.implementations.TestServiceImpl;
import ru.kpfu.itis.adaptivetest.services.interfaces.TestServiceInterface;
import ru.kpfu.itis.adaptivetest.validators.TestCreationFormValidator;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.isEmptyOrNullString;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

/**
 * @author Rustem Saitgareev
 * 11-602
 * 000
 */
@RunWith(SpringRunner.class)
@SpringBootTest(
        classes = {AdaptivetestApplication.class}
)
@Configuration
public class ControllerTest {
    private TestCreateController testCreateController;
    private MockMvc mockMvc;

    private void prepareMockMvc() {
        if (testCreateController == null) {
            testCreateController = new TestCreateController(
                    mock(TestServiceInterface.class),
                    mock(TestCreationFormValidator.class));
        }
        mockMvc = standaloneSetup(testCreateController).build();
    }

    // ====================== TEST CREATE ====================== //

    @org.junit.Test
    @WithUserDetails(value = "testLogin", userDetailsServiceBeanName = "userDetailsService1")
    public void shouldReturnOkIfHasAuthorization() throws Exception {
        TestServiceInterface testService = mock(TestServiceImpl.class);
        ArgumentCaptor<TestCreationForm> testFormArgumentCaptor = ArgumentCaptor.forClass(TestCreationForm.class);
        ArgumentCaptor<User> userArgumentCaptor = ArgumentCaptor.forClass(User.class);
        doReturn(new HashMap<>()).when(testService).create(testFormArgumentCaptor.capture(), userArgumentCaptor.capture());
        TestCreationFormValidator testCreationFormValidator = mock(TestCreationFormValidator.class);
        doReturn(true).when(testCreationFormValidator).supports(any());

        testCreateController = new TestCreateController(testService, testCreationFormValidator);
        prepareMockMvc();

        mockMvc.perform(post("/internal_api/tests/create")
                .param("name", "test_name")
                .param("shortDescription", "short_description")
                .param("detailedDescription", "detailed_descrription")
                .param("maxDuration", "10")
                .param("isPrivate", "isPrivate"))
                .andExpect(status().isOk());

        assertThat(testFormArgumentCaptor.getValue())
                .hasFieldOrPropertyWithValue("name", "test_name")
                .hasFieldOrPropertyWithValue("shortDescription", "short_description")
                .hasFieldOrPropertyWithValue("detailedDescription", "detailed_descrription")
                .hasFieldOrPropertyWithValue("maxDuration", 10)
                .hasFieldOrPropertyWithValue("isPrivate", "isPrivate");
    }

    @org.junit.Test
    @WithUserDetails(value = "testLogin", userDetailsServiceBeanName = "userDetailsService1")
    public void shouldSetErrorCodeInHeadersAndReturnBadRequestResponseIfInvalidData() throws Exception {
        TestCreationFormValidator testCreationFormValidator = mock(TestCreationFormValidator.class);
        doReturn(true).when(testCreationFormValidator).supports(any());
        doAnswer(invocationOnMock -> {
            Errors errors = invocationOnMock.getArgument(1);
            errors.reject("test.error", "Test error");
            return null;
        }).when(testCreationFormValidator).validate(any(), any(Errors.class));

        testCreateController = new TestCreateController(mock(TestServiceImpl.class), testCreationFormValidator);
        prepareMockMvc();

        mockMvc.perform(post("/internal_api/tests/create"))
                .andExpect(status().isBadRequest())
                .andExpect(header().string("error_code", "test.error"));
    }

    private void prepareNoAuthorization() {
        TestCreationFormValidator testCreationFormValidator = mock(TestCreationFormValidator.class);
        doReturn(true).when(testCreationFormValidator).supports(any());
        doNothing().when(testCreationFormValidator).validate(any(), any());
        testCreateController = new TestCreateController(mock(TestServiceImpl.class), testCreationFormValidator);
        prepareMockMvc();
    }

    @org.junit.Test
    public void shouldReturnForbiddenIfHasNoAuthorization() throws Exception {
        prepareNoAuthorization();
        mockMvc.perform(post("/internal_api/tests/create"))
                .andExpect(status().isUnauthorized());
    }

    // ====================== TEST EDIT AND MAKE AVAILABLE ====================== //

    @org.junit.Test
    public void shouldReturnForbiddenIfHasNoAuthorizationForMakeAvailablePage() throws Exception {
        prepareNoAuthorization();
        mockMvc.perform(post("/internal_api/tests/1/make_available"))
                .andExpect(status().isUnauthorized());
    }

    @org.junit.Test
    public void shouldReturnForbiddenIfHasNoAuthorizationGetEdit() throws Exception {
        prepareNoAuthorization();
        mockMvc.perform(get("/internal_api/tests/1/edit"))
                .andExpect(status().isUnauthorized());
    }

    @org.junit.Test
    public void shouldReturnForbiddenIfHasNoAuthorizationPostEdit() throws Exception {
        prepareNoAuthorization();
        mockMvc.perform(post("/internal_api/tests/1/edit"))
                .andExpect(status().isUnauthorized());
    }

    private void prepare404() {
        TestServiceInterface testService = mock(TestServiceImpl.class);
        doReturn(Optional.empty()).when(testService).getTestById(1L);

        TestCreationFormValidator testCreationFormValidator = mock(TestCreationFormValidator.class);
        doReturn(true).when(testCreationFormValidator).supports(any());
        doNothing().when(testCreationFormValidator).validate(any(), any());

        testCreateController = new TestCreateController(testService, testCreationFormValidator);

        prepareMockMvc();
    }

    @org.junit.Test
    @WithUserDetails(value = "testLogin", userDetailsServiceBeanName = "userDetailsService1")
    public void shouldReturn404IfTestDoesNotExistGetMethod() throws Exception {
        prepare404();
        mockMvc.perform(get("/internal_api/tests/2/edit"))
                .andExpect(status().isNotFound());
    }

    @org.junit.Test
    @WithUserDetails(value = "testLogin", userDetailsServiceBeanName = "userDetailsService1")
    public void shouldReturn404IfTestDoesNotExistPostMethod() throws Exception {
        prepare404();
        mockMvc.perform(post("/internal_api/tests/2/edit"))
                .andExpect(status().isNotFound());
    }

    @org.junit.Test
    @WithUserDetails(value = "testLogin", userDetailsServiceBeanName = "userDetailsService1")
    public void shouldReturn404IfTestDoesNotExistForMakeAvailablePage() throws Exception {
        prepare404();
        mockMvc.perform(post("/internal_api/tests/2/make_available"))
                .andExpect(status().isNotFound());
    }

    private void prepareForbidden() {
        Test test = Test.builder()
                .id(1L)
                .author(User.builder().id(2).build())
                .build();

        TestServiceInterface testService = mock(TestServiceImpl.class);
        doReturn(Optional.of(test)).when(testService).getTestById(anyLong());

        TestCreationFormValidator testCreationFormValidator = mock(TestCreationFormValidator.class);
        doReturn(true).when(testCreationFormValidator).supports(any());
        doNothing().when(testCreationFormValidator).validate(any(), any());

        testCreateController = new TestCreateController(testService, testCreationFormValidator);
        prepareMockMvc();
    }

    @org.junit.Test
    @WithUserDetails(value = "testLogin", userDetailsServiceBeanName = "userDetailsService1")
    public void shouldReturnForbiddenIfUserIsNotAuthorGetMethod() throws Exception {
        prepareForbidden();
        mockMvc.perform(get("/internal_api/tests/1/edit"))
                .andExpect(status().isForbidden());
    }

    @org.junit.Test
    @WithUserDetails(value = "testLogin", userDetailsServiceBeanName = "userDetailsService1")
    public void shouldReturnForbiddenIfUserIsNotAuthorPostMethod() throws Exception {
        prepareForbidden();
        mockMvc.perform(post("/internal_api/tests/1/edit"))
                .andExpect(status().isForbidden());
    }

    @org.junit.Test
    @WithUserDetails(value = "testLogin", userDetailsServiceBeanName = "userDetailsService1")
    public void shouldReturnForbiddenIfUserIsNotAuthorForMakeAvailablePage() throws Exception {
        prepareForbidden();
        mockMvc.perform(post("/internal_api/tests/1/make_available"))
                .andExpect(status().isForbidden());
    }

    private void prepareAvailable() {
        Test test = Test.builder()
                .id(1L)
                .author(User.builder().id(1).build())
                .isAvailable(true)
                .build();
        TestServiceInterface testService = mock(TestServiceImpl.class);
        doReturn(Optional.of(test)).when(testService).getTestById(anyLong());

        TestCreationFormValidator testCreationFormValidator = mock(TestCreationFormValidator.class);
        doReturn(true).when(testCreationFormValidator).supports(any());
        doNothing().when(testCreationFormValidator).validate(any(), any());

        testCreateController = new TestCreateController(testService, testCreationFormValidator);
        prepareMockMvc();
    }

    @org.junit.Test
    @WithUserDetails(value = "testLogin", userDetailsServiceBeanName = "userDetailsService1")
    public void shouldReturnNotAcceptableIfTestIsAvailableGetMethod() throws Exception {
        prepareAvailable();
        mockMvc.perform(get("/internal_api/tests/1/edit"))
                .andExpect(status().isNotAcceptable());
    }

    @org.junit.Test
    @WithUserDetails(value = "testLogin", userDetailsServiceBeanName = "userDetailsService1")
    public void shouldReturnNotAcceptableIfTestIsAvailablePostMethod() throws Exception {
        prepareAvailable();
        mockMvc.perform(post("/internal_api/tests/1/edit"))
                .andExpect(status().isNotAcceptable());
    }

    @org.junit.Test
    @WithUserDetails(value = "testLogin", userDetailsServiceBeanName = "userDetailsService1")
    public void shouldReturnNotAcceptableIfTestIsAvailableForMakeAvailablePage() throws Exception {
        prepareAvailable();
        mockMvc.perform(post("/internal_api/tests/1/make_available"))
                .andExpect(status().isNotAcceptable());
    }

    private ArgumentCaptor<TestCreationForm> prepareOk() {
        Map<String, Object> testData = new HashMap<>();
        testData.put("test_id", 1);
        testData.put("test_name", "name");
        testData.put("shortDescription", null);
        testData.put("detailedDescription", "");
        testData.put("maxDuration", 6);
        testData.put("is_private", true);

        Test test = Test.builder()
                .id(1L)
                .author(User.builder().id(1).build())
                .isAvailable(false)
                .build();

        TestServiceInterface testService = mock(TestServiceImpl.class);
        doReturn(testData).when(testService).getTestInfoForEditPage(anyLong());
        doReturn(Optional.of(test)).when(testService).getTestById(anyLong());

        ArgumentCaptor<TestCreationForm> testFormArgumentCaptor = ArgumentCaptor.forClass(TestCreationForm.class);
        ArgumentCaptor<Test> testArgumentCaptor = ArgumentCaptor.forClass(Test.class);
        doNothing().when(testService).edit(testArgumentCaptor.capture(), testFormArgumentCaptor.capture());

        TestCreationFormValidator testCreationFormValidator = mock(TestCreationFormValidator.class);
        doReturn(true).when(testCreationFormValidator).supports(any());
        doNothing().when(testCreationFormValidator).validate(any(), any());

        testCreateController = new TestCreateController(testService, testCreationFormValidator);
        prepareMockMvc();

        return testFormArgumentCaptor;
    }

    @org.junit.Test
    @WithUserDetails(value = "testLogin", userDetailsServiceBeanName = "userDetailsService1")
    public void shouldReturnCorrectJSONByIdIfUserIsAuthor() throws Exception {
        prepareOk();
        mockMvc.perform(get("/internal_api/tests/1/edit"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.test_id", is(1)))
                .andExpect(jsonPath("$.test_name", is("name")))
                .andExpect(jsonPath("$.shortDescription", isEmptyOrNullString()))
                .andExpect(jsonPath("$.detailedDescription", is("")))
                .andExpect(jsonPath("$.maxDuration", is(6)))
                .andExpect(jsonPath("$.is_private", is(true)));
    }

    @org.junit.Test
    @WithUserDetails(value = "testLogin", userDetailsServiceBeanName = "userDetailsService1")
    public void shouldReturnOkIfUserIsAuthorOfTest() throws Exception {
        ArgumentCaptor<TestCreationForm> testFormArgumentCaptor = prepareOk();
        mockMvc.perform(post("/internal_api/tests/1/edit")
                .param("name", "test_name")
                .param("shortDescription", "short_description")
                .param("detailedDescription", "detailed_description")
                .param("maxDuration", "10")
                .param("isPrivate", "isPrivate"))
                .andExpect(status().isOk());

        assertThat(testFormArgumentCaptor.getValue())
                .hasFieldOrPropertyWithValue("name", "test_name")
                .hasFieldOrPropertyWithValue("shortDescription", "short_description")
                .hasFieldOrPropertyWithValue("detailedDescription", "detailed_description")
                .hasFieldOrPropertyWithValue("maxDuration", 10)
                .hasFieldOrPropertyWithValue("isPrivate", "isPrivate");
    }

    @org.junit.Test
    @WithUserDetails(value = "testLogin", userDetailsServiceBeanName = "userDetailsService1")
    public void shouldReturnOkIfEverythingIsCorrectForMakeAvailableTest() throws Exception {
        prepareOk();
        mockMvc.perform(post("/internal_api/tests/1/make_available"))
                .andExpect(status().isOk());
    }

    @org.junit.Test
    @WithUserDetails(value = "testLogin", userDetailsServiceBeanName = "userDetailsService1")
    public void shouldSetErrorCodeInHeadersAndReturnBadRequestResponseIfInvalidDataWhileEditing() throws Exception {
        Test test = Test.builder()
                .id(1L)
                .author(User.builder().id(1).build())
                .isAvailable(false)
                .build();

        TestServiceInterface testService = mock(TestServiceImpl.class);
        doReturn(Optional.of(test)).when(testService).getTestById(anyLong());

        TestCreationFormValidator testCreationFormValidator = mock(TestCreationFormValidator.class);
        doReturn(true).when(testCreationFormValidator).supports(any());
        doAnswer(invocationOnMock -> {
            Errors errors = invocationOnMock.getArgument(1);
            errors.reject("test.error", "Test error");
            return null;
        }).when(testCreationFormValidator).validate(any(), any(Errors.class));

        testCreateController = new TestCreateController(testService, testCreationFormValidator);
        prepareMockMvc();

        mockMvc.perform(post("/internal_api/tests/1/edit"))
                .andExpect(status().isBadRequest())
                .andExpect(header().string("error_code", "test.error"));
    }
}
