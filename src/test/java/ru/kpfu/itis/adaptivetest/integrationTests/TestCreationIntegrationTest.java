package ru.kpfu.itis.adaptivetest.integrationTests;

import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlGroup;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import ru.kpfu.itis.adaptivetest.application.AdaptivetestApplication;
import ru.kpfu.itis.adaptivetest.models.AdaptedQuestion;
import ru.kpfu.itis.adaptivetest.models.Question;
import ru.kpfu.itis.adaptivetest.models.Test;
import ru.kpfu.itis.adaptivetest.models.User;
import ru.kpfu.itis.adaptivetest.repositories.AdaptedQuestionRepository;
import ru.kpfu.itis.adaptivetest.repositories.TestRepository;

import java.util.Date;

import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * @author Rustem Saitgareev
 * 11-602
 * 000
 */
@RunWith(SpringRunner.class)
@SpringBootTest(
        classes = AdaptivetestApplication.class,
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@TestPropertySource(
        locations = "/application.properties",
        properties = {
                "spring.datasource.url=jdbc:postgresql://localhost:5432/ats_test_db"
        })
@SqlGroup({
        @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD,
                scripts = "/TestCreationTestData.sql"),
        @Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD,
                scripts = "/TestCreationTestDataCleanUp.sql")
})
public class TestCreationIntegrationTest {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private TestRepository testRepository;

    @Autowired
    private AdaptedQuestionRepository adaptedQuestionRepository;

    @org.junit.Test
    @WithUserDetails(value = "login", userDetailsServiceBeanName = "userDetailsServiceImpl")
    public void shouldReturnOkHttpCodeAndCreateEntityIfCorrectData() throws Exception {
        mockMvc.perform(post("/internal_api/tests/create")
                .param("name", "test_name2")
                .param("shortDescription", "short_description")
                .param("detailedDescription", "detailed_descrription")
                .param("maxDuration", "10")
                .param("isPrivate", "isPrivate"))
                .andExpect(status().isOk());

        assertThat(testRepository.findAll())
                .hasSize(2)
                .last()
                .extracting("name")
                .containsOnlyOnce("test_name2");
    }

    @org.junit.Test
    @WithUserDetails(value = "login", userDetailsServiceBeanName = "userDetailsServiceImpl")
    public void shouldReturnOkHttpCodeWhileEditingGetMethod() throws Exception {
        mockMvc.perform(get("/internal_api/tests/1/edit"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.test_id", is(1)))
                .andExpect(jsonPath("$.test_name", is("Name of test")))
                .andExpect(jsonPath("$.short_description", is("Short description")))
                .andExpect(jsonPath("$.detailed_description", is("Detailed description")))
                .andExpect(jsonPath("$.max_duration", is(600)))
                .andExpect(jsonPath("$.is_private", is(false)))
                .andExpect(jsonPath("$.is_questions_details_available", is(true)))
                .andExpect(jsonPath("$.questions", hasSize(2)))
                .andExpect(jsonPath("$.questions[*].question_id", containsInAnyOrder(1, 2)));
    }

    @org.junit.Test
    @WithUserDetails(value = "login", userDetailsServiceBeanName = "userDetailsServiceImpl")
    public void shouldReturnOkHttpCodeWhileEditingPostMethod() throws Exception {
        mockMvc.perform(post("/internal_api/tests/1/edit")
                .param("name", "test_name3")
                .param("shortDescription", "short_description")
                .param("detailedDescription", "detailed_descrription")
                .param("maxDuration", "10")
                .param("isPrivate", "isPrivate"))
                .andExpect(status().isOk());
        assertThat(testRepository.findAll())
                .hasSize(1)
                .first()
                .extracting("name")
                .containsOnlyOnce("test_name3");
    }

    @org.junit.Test
    @WithUserDetails(value = "login", userDetailsServiceBeanName = "userDetailsServiceImpl")
    public void shouldReturnBadRequestIfQuestionHasNoLevelsForMakeAvailablePage() throws Exception {
        mockMvc.perform(post("/internal_api/tests/1/make_available"))
                .andExpect(status().isBadRequest());
    }

    @org.junit.Test
    @WithUserDetails(value = "login", userDetailsServiceBeanName = "userDetailsServiceImpl")
    public void shouldReturnBadRequestIfHasNoQuestionsForMakeAvailablePage() throws Exception {
        Test newTest = Test.builder()
                .name("Test55")
                .shortDescription("sh")
                .detailedDescription("det")
                .maxScore(30)
                .isPrivate(true)
                .isAvailable(false)
                .author(User.builder().id(1).build())
                .dateCreated(new Date(1000))
                .isQuestionsDetailsAvailable(false)
                .build();
        testRepository.save(newTest);
        mockMvc.perform(post("/internal_api/tests/2/make_available"))
                .andExpect(status().isBadRequest());
    }

    @org.junit.Test
    @WithUserDetails(value = "login", userDetailsServiceBeanName = "userDetailsServiceImpl")
    public void shouldReturnOkForMakeAvailablePage() throws Exception {
        AdaptedQuestion adaptedQuestion = AdaptedQuestion.builder()
                .question(Question.builder().id(2).build())
                .text("1")
                .rightAnswer("aa")
                .score(5)
                .level(0)
                .build();
        adaptedQuestionRepository.save(adaptedQuestion);
        mockMvc.perform(post("/internal_api/tests/1/make_available"))
                .andExpect(status().isOk());
        Test test = testRepository.findAll().get(0);
        assertThat(test)
                .hasFieldOrPropertyWithValue("isAvailable", true)
                .hasFieldOrPropertyWithValue("maxScore", 10);
    }
}
