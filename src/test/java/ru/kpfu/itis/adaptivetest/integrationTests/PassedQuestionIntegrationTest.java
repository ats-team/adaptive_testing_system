package ru.kpfu.itis.adaptivetest.integrationTests;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlGroup;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import ru.kpfu.itis.adaptivetest.application.AdaptivetestApplication;

import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * @author Bulat Giniyatullin
 * 25 April 2018
 */

@RunWith(SpringRunner.class)
@SpringBootTest(
        classes = AdaptivetestApplication.class,
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@TestPropertySource(
        locations = "/application.properties",
        properties = {
                "spring.datasource.url=jdbc:postgresql://localhost:5432/ats_test_db"
        })
@SqlGroup({
        @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD,
                scripts = "/PassedQuestionTestData.sql"),
        @Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD,
                scripts = "/PassedQuestionTestDataCleanUp.sql")
})
public class PassedQuestionIntegrationTest {
    @Autowired
    private MockMvc mockMvc;

    @Test
    @WithUserDetails(value = "login1", userDetailsServiceBeanName = "userDetailsServiceImpl")
    public void shouldReturnCorrectJson() throws Exception {
        mockMvc.perform(get("/internal_api/passed_questions/1"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.passed_question_id", is(1)))
                .andExpect(jsonPath("$.test_name", is("name1")))
                .andExpect(jsonPath("$.question_name", is("name")))
                .andExpect(jsonPath("$.picture_path", is("photo1")))
                .andExpect(jsonPath("$.file_path", is("file1")))
                .andExpect(jsonPath("$.text", is("text1")))
                .andExpect(jsonPath("$.user_answer", is("answer1")))
                .andExpect(jsonPath("$.right_answer", is("rightAnswer1")));
    }

    @Test
    @WithUserDetails(value = "login1", userDetailsServiceBeanName = "userDetailsServiceImpl")
    public void shouldReturn404IfIdIsIncorrect() throws Exception {
        mockMvc.perform(get("/internal_api/passed_questions/0"))
                .andExpect(status().isNotFound());
    }

    @Test
    @WithUserDetails(value = "login1", userDetailsServiceBeanName = "userDetailsServiceImpl")
    public void shouldReturnOkIfAuthor() throws Exception {
        mockMvc.perform(get("/internal_api/passed_questions/1"))
                .andExpect(status().isOk());
    }

    @Test
    @WithUserDetails(value = "login2", userDetailsServiceBeanName = "userDetailsServiceImpl")
    public void shouldReturnOkIsUserWhoPassedTheTestAndTestResultsIsNotHidden() throws Exception {
        mockMvc.perform(get("/internal_api/passed_questions/2"))
                .andExpect(status().isOk());
    }

    @Test
    @WithUserDetails(value = "login3", userDetailsServiceBeanName = "userDetailsServiceImpl")
    public void shouldReturnForbiddenIfIsNotAuthorOrUserWhoPassedTheTestAndTestResultsIsNotHidden() throws Exception {
        mockMvc.perform(get("/internal_api/passed_questions/2"))
                .andExpect(status().isForbidden());
    }

    @Test
    @WithUserDetails(value = "login2", userDetailsServiceBeanName = "userDetailsServiceImpl")
    public void shouldReturnForbiddenIfIsUserWhoPassedTheTestButTestResultsIsHidden() throws Exception {
        mockMvc.perform(get("/internal_api/passed_questions/1"))
                .andExpect(status().isForbidden());
    }
}
