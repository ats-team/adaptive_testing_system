package ru.kpfu.itis.adaptivetest.integrationTests;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlGroup;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import ru.kpfu.itis.adaptivetest.application.AdaptivetestApplication;
import ru.kpfu.itis.adaptivetest.repositories.QuestionRepository;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * @author Bulat Giniyatullin
 * 13 April 2018
 */

@RunWith(SpringRunner.class)
@SpringBootTest(
        classes = AdaptivetestApplication.class,
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@TestPropertySource(
        locations = "/application.properties",
        properties = {
                "spring.datasource.url=jdbc:postgresql://localhost:5432/ats_test_db"
        })
@SqlGroup({
        @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD,
                scripts = "/QuestionCreationTestData.sql"),
        @Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD,
                scripts = "/QuestionCreationTestDataCleanUp.sql")
})
public class QuestionCreationIntegrationTest {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private QuestionRepository questionRepository;

    @Test
    @WithUserDetails(value = "login", userDetailsServiceBeanName = "userDetailsServiceImpl")
    public void shouldReturnOkHttpCodeIfTestIdIsCorrectAndPersistQuestion() throws Exception {
        mockMvc.perform(post("/internal_api/questions/create")
                .param("testId", "1")
                .param("name", "name"))
                .andExpect(status().isOk());

        assertThat(questionRepository.findAll())
                .hasSize(1)
                .first()
                    .extracting("name")
                        .containsOnlyOnce("name");
        assertThat(questionRepository.findAll())
                .hasSize(1)
                .first()
                    .extracting("test")
                        .extracting("id")
                            .containsOnlyOnce(1L);
    }

    @Test
    @WithUserDetails(value = "login", userDetailsServiceBeanName = "userDetailsServiceImpl")
    public void shouldReturnNotFoundCodeAndSetErrorHeaderIfTestIdIsIncorrect() throws Exception {
        mockMvc.perform(post("/internal_api/questions/create")
                .param("testId", "0")
                .param("name", "name"))
                .andExpect(status().isNotFound())
                .andExpect(header().string("error_code", "not_found.test"));
    }
}
