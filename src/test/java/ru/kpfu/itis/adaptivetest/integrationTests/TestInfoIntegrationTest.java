package ru.kpfu.itis.adaptivetest.integrationTests;

import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlGroup;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.util.NestedServletException;
import ru.kpfu.itis.adaptivetest.application.AdaptivetestApplication;

import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@RunWith(SpringRunner.class)
@SpringBootTest(
        classes = AdaptivetestApplication.class,
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@TestPropertySource(
        locations = "/application.properties",
        properties = {
                "spring.datasource.url=jdbc:postgresql://localhost:5432/ats_test_db"
        })
@SqlGroup({
        @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD,
                scripts = "/TestInfoTestData.sql"),
        @Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD,
                scripts = "/TestInfoTestDataCleanUp.sql")
})
public class TestInfoIntegrationTest {
    @Autowired
    private MockMvc mockMvc;

    @org.junit.Test
    @WithMockUser(authorities="USER")
    public void controllerShouldReturnCorrectJSON() throws Exception {
        mockMvc.perform(get("/internal_api/tests/1"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.test_id", is(1)))
                .andExpect(jsonPath("$.name", is("Name of test")))
                .andExpect(jsonPath("$.detailed_description", is("Detailed description")))
                .andExpect(jsonPath("$.creator_login", is("login1")))
                .andExpect(jsonPath("$.question_count", is(1)))
                .andExpect(jsonPath("$.creator_fio", is("Surname Name")))
                .andExpect(jsonPath("$.max_duration", is(600)))
                .andExpect(jsonPath("$.date_created", is(1521922433)));
    }

    @org.junit.Test(expected = NestedServletException.class)
    @WithMockUser(authorities="USER")
    public void controllerShouldReturnError() throws Exception {
        mockMvc.perform(get("/internal_api/tests/2"));
    }

}
