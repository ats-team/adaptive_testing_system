package ru.kpfu.itis.adaptivetest.integrationTests;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlGroup;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import ru.kpfu.itis.adaptivetest.application.AdaptivetestApplication;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Date;
import java.util.TimeZone;

import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * @author Bulat Giniyatullin
 * 19 April 2018
 */

@RunWith(SpringRunner.class)
@SpringBootTest(
        classes = AdaptivetestApplication.class,
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@TestPropertySource(
        locations = "/application.properties",
        properties = {
                "spring.datasource.url=jdbc:postgresql://localhost:5432/ats_test_db"
        })
@SqlGroup({
        @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD,
                scripts = "/ResultsOfTestCreatedByMeTestData.sql"),
        @Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD,
                scripts = "/ResultsOfTestCreatedByMeTestDataCleanUp.sql")
})
public class ResultsOfTestCreatedByMeIntegrationTest {
    @Autowired
    private MockMvc mockMvc;

    @Test
    @WithUserDetails(value = "userLogin", userDetailsServiceBeanName = "userDetailsServiceImpl")
    public void shouldReturnCorrectJson() throws Exception {
        mockMvc.perform(get("/internal_api/tests/my/1/results"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.test_id", is(1)))
                .andExpect(jsonPath("$.name", is("testName")))
                .andExpect(jsonPath("$.detailed_description", is("Detailed Description")))
                .andExpect(jsonPath("$.max_duration", is(120)))
                .andExpect(jsonPath("$.count_of_participants", is(2)))
                .andExpect(jsonPath("$.question_count", is(1)))
                .andExpect(jsonPath("$.date_created",
                        is(1)))
                .andExpect(jsonPath("$.participants", hasSize(2)))
                .andExpect(jsonPath("$.participants[?(" +
                        "@.passed_test_id == 1 " +
                        "&& @.login == 'userLogin' " +
                        "&& @.fio == 'userFio' " +
                        "&& @.score == 5 " +
                        "&& @.complexity == 3 " +
                        "&& @.duration == 120 " +
                        "&& @.date == 1)]").exists())
                .andExpect(jsonPath("$.participants[?(" +
                        "@.passed_test_id == 2 " +
                        "&& @.login == 'userLogin2' " +
                        "&& @.fio == 'userFio2' " +
                        "&& @.score == 6 " +
                        "&& @.complexity == 4 " +
                        "&& @.duration == 120 " +
                        "&& @.date == 1)]").exists());
    }

    @Test
    @WithUserDetails(value = "userLogin", userDetailsServiceBeanName = "userDetailsServiceImpl")
    public void shouldReturnEmptyResponseWith404StatusIfTestIdIsIncorrect() throws Exception {
        mockMvc.perform(get("/internal_api/tests/my/2/results"))
                .andExpect(status().isNotFound())
                .andExpect(content().string(isEmptyString()));
    }

    @Test
    @WithUserDetails(value = "userLogin2", userDetailsServiceBeanName = "userDetailsServiceImpl")
    public void shouldReturnAccessDeniedIfUserNotAuthorized() throws Exception {
        mockMvc.perform(get("/internal_api/tests/my/1/results"))
                .andExpect(status().isForbidden());
    }
}
