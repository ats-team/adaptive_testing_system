/*
package ru.kpfu.itis.adaptivetest.integrationTests;

import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlGroup;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import ru.kpfu.itis.adaptivetest.application.AdaptivetestApplication;
import ru.kpfu.itis.adaptivetest.models.PassedTest;
import ru.kpfu.itis.adaptivetest.models.Test;
import ru.kpfu.itis.adaptivetest.models.User;
import ru.kpfu.itis.adaptivetest.security.details.UserDetailsImpl;

import java.util.Date;

import static org.hamcrest.Matchers.anyOf;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.hamcrest.Matchers.is;

@RunWith(SpringRunner.class)
@SpringBootTest(
        classes = AdaptivetestApplication.class,
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@TestPropertySource(
        locations = "/application-local.properties",
        properties = {
                "spring.datasource.url=jdbc:postgresql://localhost:5432/ats_test_db"
        })
@SqlGroup({
        @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD,
                scripts = "/TestPassingTestData.sql"),
        @Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD,
                scripts = "/TestPassingTestDataCleanUp.sql")
})
public class PassingTestIntegrationTest {
    public PassedTest getPassedTest() {
        User user = ((UserDetailsImpl)SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUser();
        Test test = Test.builder()
                .id(3)
                .author(user)
                .maxDuration(1200)
                .isAvailable(true)
                .isPrivate(false)
                .build();
        PassedTest passedTest = PassedTest.builder()
                .id(1)
                .complexity(50)
                .user(user)
                .score(3)
                .duration(1200)
                .date(new Date())
                .test(test)
                .build();
        return passedTest;
    }

    @Autowired
    private MockMvc mockMvc;

    @org.junit.Test
    @WithUserDetails(value = "login1", userDetailsServiceBeanName = "userDetailsServiceImpl")
    public void shouldReturn403WhenStartingNotAvailableTest() throws Exception{
        mockMvc.perform(post("/internal_api/test/2/start"))
                .andExpect(status().isForbidden());
    }

    @org.junit.Test
    @WithUserDetails(value = "login1", userDetailsServiceBeanName = "userDetailsServiceImpl")
    public void shouldReturn400WhenStartingAlreadyStartedTest() throws Exception{
        mockMvc.perform(post("/internal_api/test/3/start"))
                .andExpect(status().isBadRequest())
                .andExpect(header().string("error_code", "already_started"));
    }

    @org.junit.Test
    @WithUserDetails(value = "login1", userDetailsServiceBeanName = "userDetailsServiceImpl")
    public void shouldReturn400WhenStartingNotExistingTest() throws Exception{
        mockMvc.perform(post("/internal_api/test/4/start"))
                .andExpect(status().isBadRequest())
                .andExpect(header().string("error_code", "not_found.test"));
    }

    @org.junit.Test
    @WithUserDetails(value = "login1", userDetailsServiceBeanName = "userDetailsServiceImpl")
    public void shouldReturnIdOfNextQuestionWhenStartingTest() throws Exception{
        mockMvc.perform(post("/internal_api/test/1/start"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.next_question_id", anyOf(is(1), is(2), is(3))));
    }

    @org.junit.Test
    @WithUserDetails(value = "login1", userDetailsServiceBeanName = "userDetailsServiceImpl")
    public void shouldReturn404WhenAnsweringNotExistingLevel() throws Exception{
        mockMvc.perform(post("/internal_api/test/1/start"));
        mockMvc.perform(post("/internal_api/questions/1")
                .param("level_id", "20")
                .param("answer", "answer"))
                .andExpect(status().isBadRequest())
                .andExpect(header().string("error_code", "not_found.level"));
    }

    @org.junit.Test
    @WithUserDetails(value = "login1", userDetailsServiceBeanName = "userDetailsServiceImpl")
    public void shouldReturn404WhenAnsweringAlreadyPassedQuestion() throws Exception{
        PassedTest passedTest = getPassedTest();
        mockMvc.perform(post("/internal_api/questions/4").sessionAttr("passed_test", passedTest)
                .param("level_id", "11")
                .param("answer", "answer"))
                .andExpect(status().isBadRequest())
                .andExpect(header().string("error_code", "already_passed"));
    }

    @org.junit.Test
    @WithUserDetails(value = "login1", userDetailsServiceBeanName = "userDetailsServiceImpl")
    public void shouldReturn403WhenAnsweringQuestionNotFromPassedTest() throws Exception{
        PassedTest passedTest = getPassedTest();
        mockMvc.perform(post("/internal_api/questions/1").sessionAttr("passed_test", passedTest)
                .param("level_id", "1")
                .param("answer", "answer"))
                .andExpect(status().isForbidden());
    }

    @org.junit.Test
    @WithUserDetails(value = "login1", userDetailsServiceBeanName = "userDetailsServiceImpl")
    public void shouldIncreaseScoreAndComplexityWhenAnswerIsRight() throws Exception{
        PassedTest passedTest = getPassedTest();
        mockMvc.perform(post("/internal_api/questions/5").sessionAttr("passed_test", passedTest)
                .param("level_id", "14")
                .param("answer", "answer1")).andExpect(status().isOk());

        mockMvc.perform(get("/internal_api/tests/my/3/results"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.participants[0].passed_test_id", is(1)))
                .andExpect(jsonPath("$.participants[0].score", is(6)))
                .andExpect(jsonPath("$.participants[0].complexity", is(83)));
    }

    @org.junit.Test
    @WithUserDetails(value = "login1", userDetailsServiceBeanName = "userDetailsServiceImpl")
    public void shouldNotIncreaseScoreAndShouldDecreaseComplexityWhenAnswerIsWrong() throws Exception{
        PassedTest passedTest = getPassedTest();
        mockMvc.perform(post("/internal_api/questions/5").sessionAttr("passed_test", passedTest)
                .param("level_id", "14")
                .param("answer", "answer")).andExpect(status().isOk());

        mockMvc.perform(get("/internal_api/tests/my/3/results"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.participants[0].passed_test_id", is(1)))
                .andExpect(jsonPath("$.participants[0].score", is(3)))
                .andExpect(jsonPath("$.participants[0].complexity", is(17)));
    }

    @org.junit.Test
    @WithUserDetails(value = "login1", userDetailsServiceBeanName = "userDetailsServiceImpl")
    public void shouldReturn404WhenGettingInfoAboutAlreadyPassedQuestion() throws Exception{
        PassedTest passedTest = getPassedTest();
        mockMvc.perform(get("/internal_api/questions/4").sessionAttr("passed_test", passedTest))
                .andExpect(status().isBadRequest())
                .andExpect(header().string("error_code", "already_passed"));
    }

    @org.junit.Test
    @WithUserDetails(value = "login1", userDetailsServiceBeanName = "userDetailsServiceImpl")
    public void shouldReturn404WhenGettingInfoAboutQuestionNotFromPassedTest() throws Exception{
        PassedTest passedTest = getPassedTest();
        mockMvc.perform(get("/internal_api/questions/1").sessionAttr("passed_test", passedTest))
                .andExpect(status().isForbidden());
    }

    @org.junit.Test
    @WithUserDetails(value = "login1", userDetailsServiceBeanName = "userDetailsServiceImpl")
    public void shouldReturn404WhenGettingInfoAboutNotExisting() throws Exception{
        PassedTest passedTest = getPassedTest();
        mockMvc.perform(get("/internal_api/questions/7").sessionAttr("passed_test", passedTest))
                .andExpect(status().isBadRequest());
    }
}
*/
