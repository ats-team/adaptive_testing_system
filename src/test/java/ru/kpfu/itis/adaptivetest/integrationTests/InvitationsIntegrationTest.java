package ru.kpfu.itis.adaptivetest.integrationTests;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlGroup;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import ru.kpfu.itis.adaptivetest.application.AdaptivetestApplication;
import ru.kpfu.itis.adaptivetest.models.AvailableTest;
import ru.kpfu.itis.adaptivetest.repositories.AvailableTestRepository;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * @author Bulat Giniyatullin
 * 27 April 2018
 */

@RunWith(SpringRunner.class)
@SpringBootTest(
        classes = AdaptivetestApplication.class,
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@TestPropertySource(
        locations = "/application.properties",
        properties = {
                "spring.datasource.url=jdbc:postgresql://localhost:5432/ats_test_db"
        })
@SqlGroup({
        @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD,
                scripts = "/InvitationsTestData.sql"),
        @Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD,
                scripts = "/InvitationsTestDataCleanUp.sql")
})
public class InvitationsIntegrationTest {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private AvailableTestRepository availableTestRepository;

    @Test
    @WithUserDetails(value = "login1", userDetailsServiceBeanName = "userDetailsServiceImpl")
    public void uninviteMustRemoveInvitation() throws Exception {
        Optional<AvailableTest> optionalAvailableTestBefore =
                availableTestRepository.findOneByTestIdAndUserLogin(1L, "login2");

        mockMvc.perform(post("/internal_api/invitations/uninvite")
                .param("test_id", "1")
                .param("login", "login2"))
                .andExpect(status().isOk());

        Optional<AvailableTest> optionalAvailableTestAfter =
                availableTestRepository.findOneByTestIdAndUserLogin(1L, "login2");

        assertThat(optionalAvailableTestBefore.isPresent())
                .isTrue();

        assertThat(optionalAvailableTestAfter.isPresent())
                .isFalse();
    }

    @Test
    @WithUserDetails(value = "login1", userDetailsServiceBeanName = "userDetailsServiceImpl")
    public void uninviteMustDoNothingIfInvitationDoesNotExist() throws Exception {
        long countBefore = availableTestRepository.count();

        mockMvc.perform(post("/internal_api/invitations/uninvite")
                .param("test_id", "1")
                .param("login", "login1"))
                .andExpect(status().isOk());

        long countAfter = availableTestRepository.count();

        assertEquals(countBefore, countAfter);
    }

    @Test
    @WithUserDetails(value = "login1", userDetailsServiceBeanName = "userDetailsServiceImpl")
    public void inviteMustCreateInvitation() throws Exception {
        Optional<AvailableTest> optionalAvailableTestBefore =
                availableTestRepository.findOneByTestIdAndUserLogin(3L, "login2");

        mockMvc.perform(post("/internal_api/invitations/invite")
                .param("test_id", "3")
                .param("login", "login2"))
                .andExpect(status().isOk());

        Optional<AvailableTest> optionalAvailableTestAfter =
                availableTestRepository.findOneByTestIdAndUserLogin(3L, "login2");

        assertThat(optionalAvailableTestBefore.isPresent())
                .isFalse();

        assertThat(optionalAvailableTestAfter.isPresent())
                .isTrue();
    }

    @Test
    @WithUserDetails(value = "login1", userDetailsServiceBeanName = "userDetailsServiceImpl")
    public void inviteDoNotHaveToDuplicateInvitation() throws Exception {
        long countBefore = availableTestRepository.count();

        mockMvc.perform(post("/internal_api/invitations/invite")
                .param("test_id", "2")
                .param("login", "login2"))
                .andExpect(status().isOk());

        long countAfter = availableTestRepository.count();

        assertEquals(countBefore, countAfter);
    }

    @Test
    @WithUserDetails(value = "login1", userDetailsServiceBeanName = "userDetailsServiceImpl")
    public void inviteMustReturn404IfUserDoesNotExist() throws Exception {
        mockMvc.perform(post("/internal_api/invitations/invite")
                .param("test_id", "1")
                .param("login", "unknownLogin"))
                .andExpect(status().isNotFound());
    }

    @Test
    @WithUserDetails(value = "login1", userDetailsServiceBeanName = "userDetailsServiceImpl")
    public void inviteMustReturn404IfTestDoesNotExist() throws Exception {
        mockMvc.perform(post("/internal_api/invitations/invite")
                .param("test_id", "10")
                .param("login", "login2"))
                .andExpect(status().isNotFound());
    }

    @Test
    @WithUserDetails(value = "login2", userDetailsServiceBeanName = "userDetailsServiceImpl")
    public void inviteMustReturnForbiddenIfUserIsNotAuthor() throws Exception {
        mockMvc.perform(post("/internal_api/invitations/invite")
                .param("test_id", "1")
                .param("login", "login1"))
                .andExpect(status().isForbidden());
    }
}
