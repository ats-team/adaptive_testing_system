package ru.kpfu.itis.adaptivetest.integrationTests;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlGroup;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.util.NestedServletException;
import ru.kpfu.itis.adaptivetest.application.AdaptivetestApplication;
import ru.kpfu.itis.adaptivetest.models.AdaptedQuestion;
import ru.kpfu.itis.adaptivetest.models.Question;
import ru.kpfu.itis.adaptivetest.repositories.AdaptedQuestionRepository;
import ru.kpfu.itis.adaptivetest.repositories.QuestionRepository;


import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(
        classes = AdaptivetestApplication.class,
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@TestPropertySource(
        locations = "/application.properties",
        properties = {
                "spring.datasource.url=jdbc:postgresql://localhost:5432/ats_test_db"
        })
@SqlGroup({
        @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD,
                scripts = "/LevelCreationTestData.sql"),
        @Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD,
                scripts = "/LevelCreationTestDataCleanUp.sql")
})
public class LevelCreationIntegrationTest {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private AdaptedQuestionRepository adaptedQuestionRepository;

    @Autowired
    private QuestionRepository questionRepository;

    @Test
    @WithUserDetails(value = "login1", userDetailsServiceBeanName = "userDetailsServiceImpl")
    public void shouldReturnCorrectJSON() throws Exception {
        mockMvc.perform(get("/internal_api/levels_creation_data/1"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.level_id", is(1)))
                .andExpect(jsonPath("$.question_id", is(1)))
                .andExpect(jsonPath("$.text", is("text")))
                .andExpect(jsonPath("$.right_answer", is("answer")))
                .andExpect(jsonPath("$.score", is(5)));
    }



    @Test
    @WithUserDetails(value = "login2", userDetailsServiceBeanName = "userDetailsServiceImpl")
    public void shouldReturnForbiddenOnCreatingLevel() throws Exception {
        try {
            mockMvc.perform(post("/internal_api/levels")
                    .param("question_id", "1")
                    .param("text", "text 2")
                    .param("right_answer", "answer2")
                    .param("score", "3")
                ).andExpect(status().isForbidden());
        } catch (NestedServletException ignored) {}
    }


    @Test
    @WithUserDetails(value = "login1", userDetailsServiceBeanName = "userDetailsServiceImpl")
    public void shouldSetErrorCodeInHeadersAndReturnBadRequestResponseIfInvalidData() throws Exception {
        try {
            mockMvc.perform(post("/internal_api/levels")
                    .param("question_id", "1")
                    .param("text", "")
                    .param("right_answer", "answer2")
                    .param("score", "3"))
                    .andExpect(header().string("error_code", "empty.text"));
        } catch (NestedServletException ignored) {}
    }

    @Test
    @WithUserDetails(value = "login1", userDetailsServiceBeanName = "userDetailsServiceImpl")
    public void shouldSetErrorCodeInHeadersAndReturnBadRequestResponseIfQuestionNotFound() throws Exception {
        try {
            mockMvc.perform(post("/internal_api/levels")
                    .param("question_id", "2")
                    .param("text", "text")
                    .param("right_answer", "answer2")
                    .param("score", "3"))
                    .andExpect(header().string("error_code", "not_found.question"));
        } catch (NestedServletException ignored) {}
    }

    /*@Test
    @WithUserDetails(value = "login1", userDetailsServiceBeanName = "userDetailsServiceImpl")
    @Transactional
    public void shouldCreateNewLevel() throws Exception {
        mockMvc.perform(post("/internal_api/levels")
                .param("question_id", "1")
                .param("text", "text")
                .param("right_answer", "answer2")
                .param("score", "3"));

        AdaptedQuestion adaptedQuestion = adaptedQuestionRepository.findAllByQuestionIdAndLevel(1L, 2).get(0);
        Question question = questionRepository.findById(1L).get();
        assertThat(adaptedQuestion)
                .hasFieldOrPropertyWithValue("text", "text")
                .hasFieldOrPropertyWithValue("rightAnswer", "answer2")
                .hasFieldOrPropertyWithValue("score", 3)
                .hasFieldOrPropertyWithValue("level", 2);
        assertThat(adaptedQuestion.getQuestion()).isEqualToComparingFieldByField(question);
    }*/
}
