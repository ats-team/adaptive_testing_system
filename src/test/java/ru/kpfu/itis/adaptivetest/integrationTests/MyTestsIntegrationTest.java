package ru.kpfu.itis.adaptivetest.integrationTests;

import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlGroup;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import ru.kpfu.itis.adaptivetest.application.AdaptivetestApplication;
import ru.kpfu.itis.adaptivetest.repositories.TestRepository;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * @author Rustem Saitgareev
 * 11-602
 * 000
 */
@RunWith(SpringRunner.class)
@SpringBootTest(
        classes = AdaptivetestApplication.class,
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@TestPropertySource(
        locations = "/application.properties",
        properties = {
                "spring.datasource.url=jdbc:postgresql://localhost:5432/ats_test_db"
        })
@SqlGroup({
        @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD,
                scripts = "/MyTestsData.sql"),
        @Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD,
                scripts = "/MyTestsDataCleanUp.sql")
})
public class MyTestsIntegrationTest {
    @Autowired
    private MockMvc mockMvc;

    @org.junit.Test
    @WithUserDetails(value = "login1", userDetailsServiceBeanName = "userDetailsServiceImpl")
    public void shouldReturnOkAndCorrectJson() throws Exception {
        mockMvc.perform(get("/internal_api/tests/my"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].id", is(1)))
                .andExpect(jsonPath("$[0].name", is("Name of test1")))
                .andExpect(jsonPath("$[0].short_description", is("Short description")))
                .andExpect(jsonPath("$[0].is_private", is(false)))
                .andExpect(jsonPath("$[0].is_available", is(true)))
                .andExpect(jsonPath("$[0].max_duration", is(600)))
                .andExpect(jsonPath("$[0].max_score", is(10)))
                .andExpect(jsonPath("$[0].count_of_participants", is(2)))
                .andExpect(jsonPath("$[0].date_created", is(1521922433)))
                .andExpect(jsonPath("$[1].id", is(2)))
                .andExpect(jsonPath("$[1].name", is("Name of test2")))
                .andExpect(jsonPath("$[1].short_description", is("Short description")))
                .andExpect(jsonPath("$[1].is_private", is(false)))
                .andExpect(jsonPath("$[1].is_available", is(true)))
                .andExpect(jsonPath("$[1].max_duration", is(600)))
                .andExpect(jsonPath("$[1].max_score", is(10)))
                .andExpect(jsonPath("$[1].count_of_participants", is(0)))
                .andExpect(jsonPath("$[1].date_created", is(1521922433)));
    }

    @org.junit.Test
    @WithUserDetails(value = "login1", userDetailsServiceBeanName = "userDetailsServiceImpl")
    public void shouldReturnOkAndCorrectJsonWithFilter() throws Exception {
        mockMvc.perform(get("/internal_api/tests/my?name=Name of test1"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].id", is(1)))
                .andExpect(jsonPath("$[0].name", is("Name of test1")))
                .andExpect(jsonPath("$[0].short_description", is("Short description")))
                .andExpect(jsonPath("$[0].is_private", is(false)))
                .andExpect(jsonPath("$[0].is_available", is(true)))
                .andExpect(jsonPath("$[0].max_duration", is(600)))
                .andExpect(jsonPath("$[0].max_score", is(10)))
                .andExpect(jsonPath("$[0].count_of_participants", is(2)))
                .andExpect(jsonPath("$[0].date_created", is(1521922433)));
    }

    @org.junit.Test
    @WithUserDetails(value = "login1", userDetailsServiceBeanName = "userDetailsServiceImpl")
    public void shouldReturnOkAndCorrectJsonWithPagination() throws Exception {
        mockMvc.perform(get("/internal_api/tests/my?pageNumber=0&pageSize=1"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].id", is(1)))
                .andExpect(jsonPath("$[0].name", is("Name of test1")))
                .andExpect(jsonPath("$[0].short_description", is("Short description")))
                .andExpect(jsonPath("$[0].is_private", is(false)))
                .andExpect(jsonPath("$[0].is_available", is(true)))
                .andExpect(jsonPath("$[0].max_duration", is(600)))
                .andExpect(jsonPath("$[0].max_score", is(10)))
                .andExpect(jsonPath("$[0].count_of_participants", is(2)))
                .andExpect(jsonPath("$[0].date_created", is(1521922433)));
    }
}
