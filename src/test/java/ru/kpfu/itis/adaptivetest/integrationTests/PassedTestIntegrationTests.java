package ru.kpfu.itis.adaptivetest.integrationTests;

import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlGroup;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import ru.kpfu.itis.adaptivetest.application.AdaptivetestApplication;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * @author Rustem Saitgareev
 * 11-602
 * 000
 */
@RunWith(SpringRunner.class)
@SpringBootTest(
        classes = AdaptivetestApplication.class,
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@TestPropertySource(
        locations = "/application.properties",
        properties = {
                "spring.datasource.url=jdbc:postgresql://localhost:5432/ats_test_db"
        })
@SqlGroup({
        @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD,
                scripts = "/PassedTestData.sql"),
        @Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD,
                scripts = "/PassedTestDataCleanUp.sql")
})
public class PassedTestIntegrationTests {
    @Autowired
    private MockMvc mockMvc;

    @org.junit.Test
    @WithUserDetails(value = "author", userDetailsServiceBeanName = "userDetailsServiceImpl")
    public void shouldReturnOkAndCorrectJsonForAuthor() throws Exception {
        mockMvc.perform(get("/internal_api/passed_tests/1"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.passed_test_id", is(1)))
                .andExpect(jsonPath("$.name", is("Название теста1")))
                .andExpect(jsonPath("$.detailed_description", is("Detailed description")))
                .andExpect(jsonPath("$.creator_login", is("author")))
                .andExpect(jsonPath("$.creator_fio", is("fio1")))
                .andExpect(jsonPath("$.passing_date", is(1)))
                .andExpect(jsonPath("$.duration", is(120)))
                .andExpect(jsonPath("$.received_score", is(5)))
                .andExpect(jsonPath("$.max_score", is(10)))
                .andExpect(jsonPath("$.passed_questions_list", hasSize(2)));
    }

    @org.junit.Test
    @WithUserDetails(value = "student1", userDetailsServiceBeanName = "userDetailsServiceImpl")
    public void shouldReturnOkAndCorrectJsonForStudent() throws Exception {
        mockMvc.perform(get("/internal_api/passed_tests/1"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.passed_test_id", is(1)))
                .andExpect(jsonPath("$.name", is("Название теста1")))
                .andExpect(jsonPath("$.detailed_description", is("Detailed description")))
                .andExpect(jsonPath("$.creator_login", is("author")))
                .andExpect(jsonPath("$.creator_fio", is("fio1")))
                .andExpect(jsonPath("$.passing_date", is(1)))
                .andExpect(jsonPath("$.duration", is(120)))
                .andExpect(jsonPath("$.received_score", is(5)))
                .andExpect(jsonPath("$.max_score", is(10)))
                .andExpect(jsonPath("$.passed_questions_list").doesNotExist());
    }

    @org.junit.Test
    @WithUserDetails(value = "student1", userDetailsServiceBeanName = "userDetailsServiceImpl")
    public void shouldReturnOkAndCorrectJsonForStudentWithQuestionList() throws Exception {
        mockMvc.perform(get("/internal_api/passed_tests/2"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.passed_test_id", is(2)))
                .andExpect(jsonPath("$.name", is("Name of test2")))
                .andExpect(jsonPath("$.detailed_description", is("Detailed description")))
                .andExpect(jsonPath("$.creator_login", is("author")))
                .andExpect(jsonPath("$.creator_fio", is("fio1")))
                .andExpect(jsonPath("$.passing_date", is(1)))
                .andExpect(jsonPath("$.duration", is(120)))
                .andExpect(jsonPath("$.received_score", is(5)))
                .andExpect(jsonPath("$.max_score", is(10)))
                .andExpect(jsonPath("$.passed_questions_list", hasSize(2)));
    }

    @org.junit.Test
    @WithUserDetails(value = "student2", userDetailsServiceBeanName = "userDetailsServiceImpl")
    public void shouldReturn404IfPassedTestDoesNotExist() throws Exception {
        mockMvc.perform(get("/internal_api/passed_tests/3"))
                .andExpect(status().isNotFound())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8));
    }

    @org.junit.Test
    @WithUserDetails(value = "student2", userDetailsServiceBeanName = "userDetailsServiceImpl")
    public void shouldReturn403IfHasNoPermission() throws Exception {
        mockMvc.perform(get("/internal_api/passed_tests/1"))
                .andExpect(status().isForbidden())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8));
    }

    @org.junit.Test
    public void shouldReturnRedirectToLoginIfHasNoAuthentication() throws Exception {
        mockMvc.perform(get("/internal_api/passed_tests/1"))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrlPattern("**/login"));
    }

    @org.junit.Test
    @WithUserDetails(value = "student1", userDetailsServiceBeanName = "userDetailsServiceImpl")
    public void shouldReturnOkAndCorrectJsonList() throws Exception {
        mockMvc.perform(get("/internal_api/passed_tests"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].id", is(1)))
                .andExpect(jsonPath("$[0].name", is("Название теста1")))
                .andExpect(jsonPath("$[0].short_description", is("Short description")))
                .andExpect(jsonPath("$[0].creator_login", is("author")))
                .andExpect(jsonPath("$[0].creator_fio", is("fio1")))
                .andExpect(jsonPath("$[0].passing_date", is(1)))
                .andExpect(jsonPath("$[0].duration", is(120)))
                .andExpect(jsonPath("$[0].received_score", is(5)))
                .andExpect(jsonPath("$[0].max_score", is(10)))
                .andExpect(jsonPath("$[1].id", is(2)))
                .andExpect(jsonPath("$[1].name", is("Name of test2")))
                .andExpect(jsonPath("$[1].short_description", is("Short description")))
                .andExpect(jsonPath("$[1].creator_login", is("author")))
                .andExpect(jsonPath("$[1].creator_fio", is("fio1")))
                .andExpect(jsonPath("$[1].passing_date", is(1)))
                .andExpect(jsonPath("$[1].duration", is(120)))
                .andExpect(jsonPath("$[1].received_score", is(5)))
                .andExpect(jsonPath("$[1].max_score", is(10)));
    }

    @org.junit.Test
    @WithUserDetails(value = "student1", userDetailsServiceBeanName = "userDetailsServiceImpl")
    public void shouldReturnOkAndCorrectJsonListWithFilterParameter() throws Exception {
        mockMvc.perform(get("/internal_api/passed_tests?name=Название теста1&login=author&fio=fio1"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].id", is(1)))
                .andExpect(jsonPath("$[0].name", is("Название теста1")))
                .andExpect(jsonPath("$[0].short_description", is("Short description")))
                .andExpect(jsonPath("$[0].creator_login", is("author")))
                .andExpect(jsonPath("$[0].creator_fio", is("fio1")))
                .andExpect(jsonPath("$[0].passing_date", is(1)))
                .andExpect(jsonPath("$[0].duration", is(120)))
                .andExpect(jsonPath("$[0].received_score", is(5)))
                .andExpect(jsonPath("$[0].max_score", is(10)));
    }

    @org.junit.Test
    @WithUserDetails(value = "student1", userDetailsServiceBeanName = "userDetailsServiceImpl")
    public void shouldReturnOkAndCorrectJsonListWithPagination() throws Exception {
        mockMvc.perform(get("/internal_api/passed_tests?pageNumber=1&pageSize=1"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].id", is(2)))
                .andExpect(jsonPath("$[0].name", is("Name of test2")))
                .andExpect(jsonPath("$[0].short_description", is("Short description")))
                .andExpect(jsonPath("$[0].creator_login", is("author")))
                .andExpect(jsonPath("$[0].creator_fio", is("fio1")))
                .andExpect(jsonPath("$[0].passing_date", is(1)))
                .andExpect(jsonPath("$[0].duration", is(120)))
                .andExpect(jsonPath("$[0].received_score", is(5)))
                .andExpect(jsonPath("$[0].max_score", is(10)));
    }
}
