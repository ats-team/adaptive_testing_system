package ru.kpfu.itis.adaptivetest.integrationTests;

import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlGroup;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import ru.kpfu.itis.adaptivetest.application.AdaptivetestApplication;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * @author Bulat Giniyatullin
 * 21 Март 2018
 */

@RunWith(SpringRunner.class)
@SpringBootTest(
        classes = AdaptivetestApplication.class,
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@TestPropertySource(
        locations = "/application.properties",
        properties = {
                "spring.datasource.url=jdbc:postgresql://localhost:5432/ats_test_db"
        })
@SqlGroup({
        @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD,
                scripts = "/AllPublicTestsInfoTestData.sql"),
        @Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD,
                scripts = "/AllPublicTestsInfoTestDataCleanUp.sql")
})
public class AllPublicTestsInfoIntegrationTest {
    @Autowired
    private MockMvc mockMvc;

    @org.junit.Test
    @WithMockUser(username = "login1")
    public void controllerShouldReturnCorrectJSON() throws Exception {
        mockMvc.perform(get("/internal_api/tests/all_public"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].id", is(1)))
                .andExpect(jsonPath("$[0].name", is("test1")))
                .andExpect(jsonPath("$[0].short_description", is("short desc1")))
                .andExpect(jsonPath("$[0].creator_login", is("login1")))
                .andExpect(jsonPath("$[0].creator_fio", is("Surname1 Name1")))
                .andExpect(jsonPath("$[0].max_duration", is(42)))
                .andExpect(jsonPath("$[0].date_created", is(1521922433)))
                .andExpect(jsonPath("$[1].id", is(2)))
                .andExpect(jsonPath("$[1].name", is("test2")))
                .andExpect(jsonPath("$[1].short_description", is("short desc2")))
                .andExpect(jsonPath("$[1].creator_login", is("login2")))
                .andExpect(jsonPath("$[1].creator_fio", is("Surname2 Name2")))
                .andExpect(jsonPath("$[1].max_duration", is(42)))
                .andExpect(jsonPath("$[1].date_created", is(1521922433)));
    }

    @org.junit.Test
    @WithMockUser(username = "login1")
    public void controllerShouldReturnCorrectJSONWithActiveNameFilter() throws Exception {
        mockMvc.perform(get("/internal_api/tests/all_public?name=test1"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].id", is(1)))
                .andExpect(jsonPath("$[0].name", is("test1")))
                .andExpect(jsonPath("$[0].short_description", is("short desc1")))
                .andExpect(jsonPath("$[0].creator_login", is("login1")))
                .andExpect(jsonPath("$[0].creator_fio", is("Surname1 Name1")))
                .andExpect(jsonPath("$[0].max_duration", is(42)))
                .andExpect(jsonPath("$[0].date_created", is(1521922433)));
    }

    @org.junit.Test
    @WithMockUser(username = "login1")
    public void controllerShouldReturnCorrectJSONWithActiveAuthorLoginFilter() throws Exception {
        mockMvc.perform(get("/internal_api/tests/all_public?login=login1"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].id", is(1)))
                .andExpect(jsonPath("$[0].name", is("test1")))
                .andExpect(jsonPath("$[0].short_description", is("short desc1")))
                .andExpect(jsonPath("$[0].creator_login", is("login1")))
                .andExpect(jsonPath("$[0].creator_fio", is("Surname1 Name1")))
                .andExpect(jsonPath("$[0].max_duration", is(42)))
                .andExpect(jsonPath("$[0].date_created", is(1521922433)));
    }

    @org.junit.Test
    @WithMockUser(username = "login1")
    public void controllerShouldReturnCorrectJSONWithActiveAuthorFioFilter() throws Exception {
        mockMvc.perform(get("/internal_api/tests/all_public?fio=Name1 Surname1"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].id", is(1)))
                .andExpect(jsonPath("$[0].name", is("test1")))
                .andExpect(jsonPath("$[0].short_description", is("short desc1")))
                .andExpect(jsonPath("$[0].creator_login", is("login1")))
                .andExpect(jsonPath("$[0].creator_fio", is("Surname1 Name1")))
                .andExpect(jsonPath("$[0].max_duration", is(42)))
                .andExpect(jsonPath("$[0].date_created", is(1521922433)));
    }

    @org.junit.Test
    @WithMockUser(username = "login1")
    public void controllerShouldReturnEmptyJSONWithUnsuitableFilter() throws Exception {
        mockMvc.perform(get("/internal_api/tests/all_public?fio=Name2 Surname1"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$", hasSize(0)));
    }
}
