package ru.kpfu.itis.adaptivetest.integrationTests;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlGroup;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.util.NestedServletException;
import ru.kpfu.itis.adaptivetest.application.AdaptivetestApplication;
import ru.kpfu.itis.adaptivetest.models.User;
import ru.kpfu.itis.adaptivetest.repositories.UserRepository;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * @author Bulat Giniyatullin
 * 09 Апрель 2018
 */

@RunWith(SpringRunner.class)
@SpringBootTest(
        classes = AdaptivetestApplication.class,
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@TestPropertySource(
        locations = "/application.properties",
        properties = {
                "spring.datasource.url=jdbc:postgresql://localhost:5432/ats_test_db"
        })
@SqlGroup({
        @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD,
                scripts = "/ProfileTestData.sql"),
        @Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD,
                scripts = "/ProfileDataCleanUp.sql")
})
public class ProfileIntegrationTests {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private UserRepository userRepository;

    @Test
    @WithUserDetails(value = "login1", userDetailsServiceBeanName = "userDetailsServiceImpl")
    public void shouldReturnCorrectJSONById() throws Exception {
        mockMvc.perform(get("/internal_api/profile/1"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(1)))
                .andExpect(jsonPath("$.login", is("login1")))
                .andExpect(jsonPath("$.fio", is("fio1")))
                .andExpect(jsonPath("$.email", is("test@email1.com")));
    }

    @Test
    @WithUserDetails(value = "login1", userDetailsServiceBeanName = "userDetailsServiceImpl")
    public void shouldReturnCorrectJSONByLogin() throws Exception {
        mockMvc.perform(get("/internal_api/profile/login1"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(1)))
                .andExpect(jsonPath("$.login", is("login1")))
                .andExpect(jsonPath("$.fio", is("fio1")))
                .andExpect(jsonPath("$.email", is("test@email1.com")));
    }

    @Test
    @WithMockUser(roles = "USER")
    public void shouldReturnForbiddenIfIncorrectId() throws Exception {
        try {
            mockMvc.perform(get("/internal_api/profile/2"))
                    .andExpect(status().isForbidden());
        } catch (NestedServletException ignored) {}
    }

    @Test
    @WithMockUser(roles = "USER")
    public void shouldReturnForbiddenIfIncorrectLogin() throws Exception {
        try {
            mockMvc.perform(get("/internal_api/profile/incorrectLogin"))
                    .andExpect(status().isForbidden());
        } catch (NestedServletException ignored) {}
    }

    @Test
    @WithMockUser(roles = "USER")
    public void shouldReturnForbiddenIfPostOnIncorrectId() throws Exception {
        try {
            mockMvc.perform(post("/internal_api/profile/1"))
                    .andExpect(status().isForbidden());
        } catch (NestedServletException ignored) {}
    }

    @Test
    @WithMockUser(roles = "USER")
    public void shouldSetErrorCodeInHeadersAndReturnBadRequestResponseIfInvalidData() throws Exception {
        try {
            mockMvc.perform(post("/internal_api/profile/1")
                    .param("id", "1")
                    .param("fio", "132fio"))
                    .andExpect(status().isBadRequest())
                    .andExpect(header().string("error_code", "numericValue.fio"));
        } catch (NestedServletException ignored) {}
    }

    @Test
    @WithUserDetails(value = "login1", userDetailsServiceBeanName = "userDetailsServiceImpl")
    public void shouldUpdateUserData() throws Exception {
        mockMvc.perform(post("/internal_api/profile/1")
                .param("id", "1")
                .param("login", "login")
                .param("fio", "fio")
                .param("email", "new@email.com")
                .param("password", "newPassword"));

        User user = userRepository.findById(1L).get();

        assertThat(user)
                .hasFieldOrPropertyWithValue("id", 1L)
                .hasFieldOrPropertyWithValue("login", "login")
                .hasFieldOrPropertyWithValue("fio", "fio")
                .hasFieldOrPropertyWithValue("email", "new@email.com");
        assertTrue(new BCryptPasswordEncoder().matches("newPassword", user.getPassword()));
    }
}
