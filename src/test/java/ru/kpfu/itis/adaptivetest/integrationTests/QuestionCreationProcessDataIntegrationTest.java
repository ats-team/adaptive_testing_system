package ru.kpfu.itis.adaptivetest.integrationTests;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlGroup;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import ru.kpfu.itis.adaptivetest.application.AdaptivetestApplication;

import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * @author Bulat Giniyatullin
 * 12 April 2018
 */

@RunWith(SpringRunner.class)
@SpringBootTest(
        classes = AdaptivetestApplication.class,
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@TestPropertySource(
        locations = "/application.properties",
        properties = {
                "spring.datasource.url=jdbc:postgresql://localhost:5432/ats_test_db"
        })
@SqlGroup({
        @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD,
                scripts = "/QuestionCreationDataTestData.sql"),
        @Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD,
                scripts = "/QuestionCreationDataTestDataCleanUp.sql")
})
public class QuestionCreationProcessDataIntegrationTest {
    @Autowired
    private MockMvc mockMvc;

    @Test
    @WithUserDetails(value = "login", userDetailsServiceBeanName = "userDetailsServiceImpl")
    public void shouldReturnCorrectJson() throws Exception {
        mockMvc.perform(get("/internal_api/question_creation_data/1"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.test_id", is(1)))
                .andExpect(jsonPath("$.test_name", is("testName")))
                .andExpect(jsonPath("$.question_name", is("name")))
                .andExpect(jsonPath("$.levels", hasSize(2)))
                .andExpect(jsonPath("$.levels[*].id", containsInAnyOrder(1, 2)));
    }

    @Test
    @WithUserDetails(value = "login", userDetailsServiceBeanName = "userDetailsServiceImpl")
    public void shouldReturnEmptyJsonIfIdIdIncorrect() throws Exception {
        mockMvc.perform(get("/internal_api/question_creation_data/2"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(content().string("{}"));
    }
}
