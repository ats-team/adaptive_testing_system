package ru.kpfu.itis.adaptivetest.integrationTests;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlGroup;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import ru.kpfu.itis.adaptivetest.application.AdaptivetestApplication;
import ru.kpfu.itis.adaptivetest.models.AdaptedQuestion;
import ru.kpfu.itis.adaptivetest.repositories.AdaptedQuestionRepository;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.not;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * @author Bulat Giniyatullin
 * 12 April 2018
 */

@RunWith(SpringRunner.class)
@SpringBootTest(
        classes = AdaptivetestApplication.class,
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@TestPropertySource(
        locations = "/application.properties",
        properties = {
                "spring.datasource.url=jdbc:postgresql://localhost:5432/ats_test_db"
        })
@SqlGroup({
        @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD,
                scripts = "/LevelDeletingTestData.sql"),
        @Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD,
                scripts = "/LevelDeletingDataCleanUp.sql")
})
public class LevelDeletingIntegrationTest {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private AdaptedQuestionRepository adaptedQuestionRepository;

    @Test
    @WithUserDetails(value = "login", userDetailsServiceBeanName = "userDetailsServiceImpl")
    public void shouldReturnOkStatus() throws Exception {
        mockMvc.perform(delete("/internal_api/levels")
                .param("level_id_to_delete", "1"))
                .andExpect(status().isOk());
    }

    @Test
    @WithUserDetails(value = "login", userDetailsServiceBeanName = "userDetailsServiceImpl")
    public void shouldInvokeServiceMethod() throws Exception {
        Long id = 2L;
        mockMvc.perform(delete("/internal_api/levels")
                .param("level_id_to_delete", String.valueOf(id)));

        Optional<AdaptedQuestion> optionalAdaptedQuestion =
                adaptedQuestionRepository.findById(id);

        assertThat(optionalAdaptedQuestion)
                .isNotPresent();
    }
}
