package ru.kpfu.itis.adaptivetest.integrationTests;

import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlGroup;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import ru.kpfu.itis.adaptivetest.application.AdaptivetestApplication;
import ru.kpfu.itis.adaptivetest.models.User;
import ru.kpfu.itis.adaptivetest.security.details.UserDetailsImpl;
import ru.kpfu.itis.adaptivetest.security.role.Role;

import java.time.Duration;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(
        classes = AdaptivetestApplication.class,
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@TestPropertySource(
        locations = "/application.properties",
        properties = {
                "spring.datasource.url=jdbc:postgresql://localhost:5432/ats_test_db"
        })
@SqlGroup({
        @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD,
                scripts = "/AvailableTestsInfoTestData.sql"),
        @Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD,
                scripts = "/AvailableTestsInfoTestDataCleanUp.sql")
})
public class AvailableTestsIntegrationTest {
    @Autowired
    private MockMvc mockMvc;

    private static UserDetailsImpl userDetails;

    @BeforeClass
    public static void prepareData(){
        User user = new User();
        user.setId(3);
        user.setRole(Role.USER);
        userDetails = new UserDetailsImpl(user);
    }


    @org.junit.Test
    public void controllerShouldReturnCorrectJSON() throws Exception {
        mockMvc.perform(get("/internal_api/tests/available").with(user(userDetails)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].id", is(1)))
                .andExpect(jsonPath("$[0].name", is("Name of test")))
                .andExpect(jsonPath("$[0].short_description", is("Short description")))
                .andExpect(jsonPath("$[0].creator_login", is("teacher1")))
                .andExpect(jsonPath("$[0].creator_fio", is("Surname Name")))
                .andExpect(jsonPath("$[0].max_duration", is(10)))
                .andExpect(jsonPath("$[1].id", is(2)))
                .andExpect(jsonPath("$[1].name", is("Test 2")))
                .andExpect(jsonPath("$[1].short_description", is("Short description")))
                .andExpect(jsonPath("$[1].creator_login", is("teacher2")))
                .andExpect(jsonPath("$[1].creator_fio", is("LastName Name")))
                .andExpect(jsonPath("$[1].max_duration", is(10)));
    }

    @org.junit.Test
    public void controllerShouldReturnCorrectJSONWithActiveNameFilter() throws Exception {
        mockMvc.perform(get("/internal_api/tests/available?name=nam").with(user(userDetails)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].id", is(1)))
                .andExpect(jsonPath("$[0].name", is("Name of test")))
                .andExpect(jsonPath("$[0].short_description", is("Short description")))
                .andExpect(jsonPath("$[0].creator_login", is("teacher1")))
                .andExpect(jsonPath("$[0].creator_fio", is("Surname Name")))
                .andExpect(jsonPath("$[0].max_duration", is(10)));
    }

    @org.junit.Test
    public void controllerShouldReturnCorrectJSONWithActiveAuthorLoginFilter() throws Exception {
        mockMvc.perform(get("/internal_api/tests/available?login=teacher1").with(user(userDetails)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].id", is(1)))
                .andExpect(jsonPath("$[0].name", is("Name of test")))
                .andExpect(jsonPath("$[0].short_description", is("Short description")))
                .andExpect(jsonPath("$[0].creator_login", is("teacher1")))
                .andExpect(jsonPath("$[0].creator_fio", is("Surname Name")))
                .andExpect(jsonPath("$[0].max_duration", is(10)));
    }

    @org.junit.Test
    public void controllerShouldReturnCorrectJSONWithActiveAuthorFioFilter() throws Exception {
        mockMvc.perform(get("/internal_api/tests/available?fio=Surn").with(user(userDetails)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].id", is(1)))
                .andExpect(jsonPath("$[0].name", is("Name of test")))
                .andExpect(jsonPath("$[0].short_description", is("Short description")))
                .andExpect(jsonPath("$[0].creator_login", is("teacher1")))
                .andExpect(jsonPath("$[0].creator_fio", is("Surname Name")))
                .andExpect(jsonPath("$[0].max_duration", is(10)));
    }

    @org.junit.Test
    public void controllerShouldReturnEmptyJSONWithUnsuitableFilter() throws Exception {
        mockMvc.perform(get("/internal_api/tests/available?fio=AAAA").with(user(userDetails)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$", hasSize(0)));
    }
}

