package ru.kpfu.itis.adaptivetest.allPublicTestsInfoTests;

import org.junit.BeforeClass;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import ru.kpfu.itis.adaptivetest.models.Test;
import ru.kpfu.itis.adaptivetest.models.User;
import ru.kpfu.itis.adaptivetest.repositories.TestRepository;
import ru.kpfu.itis.adaptivetest.services.implementations.TestServiceImpl;

import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @author Bulat Giniyatullin
 * 21 Март 2018
 */

public class AllPublicTestsServiceTest {

    private static TestServiceImpl testService;

    @BeforeClass
    public static void prepareData() {
        User author1 = new User();
        author1.setLogin("login1");
        author1.setFio("Surname1 Name1");
        User author2 = new User();
        author2.setLogin("login2");
        author2.setFio("Surname2 Name2");

        Test test1 = new Test();
        test1.setId(1);
        test1.setAuthor(author1);
        test1.setName("test1");
        test1.setShortDescription("short desc1");
        test1.setMaxDuration(42);
        test1.setIsAvailable(true);
        test1.setDateCreated(new Date(1521922433000L));
        Test test2 = new Test();
        test2.setId(2);
        test2.setAuthor(author2);
        test2.setName("test2");
        test2.setShortDescription("short desc2");
        test2.setMaxDuration(42);
        test2.setIsAvailable(true);
        test2.setDateCreated(new Date(1521922433000L));

        List<Test> tests = Arrays.asList(test1, test2);

        JpaSpecificationExecutor<Test> testRepository = mock(TestRepository.class);
        Specification<Test> testSpecification = any();
        when(testRepository.findAll(testSpecification, any(Pageable.class)))
                .thenReturn(new PageImpl<>(tests));

        testService = new TestServiceImpl();
        testService.setTestRepository((TestRepository) testRepository);
    }

    @org.junit.Test
    public void shouldReturnCorrectDataSet() {
        List<Map<String, Object>> data = testService.getAllPublicTests(
                null,
                null,
                null, null, null);

        List<Map<String, Object>> expected = new ArrayList<>();
        Map<String, Object> firstTestData = new HashMap<>();
        firstTestData.put("id", 1L);
        firstTestData.put("name", "test1");
        firstTestData.put("short_description", "short desc1");
        firstTestData.put("creator_login", "login1");
        firstTestData.put("creator_fio", "Surname1 Name1");
        firstTestData.put("max_duration", 42);
        firstTestData.put("date_created", 1521922433L);

        expected.add(firstTestData);

        Map<String, Object> secondTestData = new HashMap<>();
        secondTestData.put("id", 2L);
        secondTestData.put("name", "test2");
        secondTestData.put("short_description", "short desc2");
        secondTestData.put("creator_login", "login2");
        secondTestData.put("creator_fio", "Surname2 Name2");
        secondTestData.put("max_duration", 42);
        secondTestData.put("date_created", 1521922433L);
        expected.add(secondTestData);

        assertThat(data)
                .hasSameSizeAs(expected)
                .containsAll(expected);
    }
}
