package ru.kpfu.itis.adaptivetest.allPublicTestsInfoTests;

import org.junit.Before;
import org.junit.BeforeClass;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import ru.kpfu.itis.adaptivetest.controllers.TestInfoController;
import ru.kpfu.itis.adaptivetest.services.implementations.TestServiceImpl;

import java.util.*;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

/**
 * @author Bulat Giniyatullin
 * 21 Март 2018
 */

public class AllPublicTestsControllerTest {

    private static TestInfoController testInfoController;
    private MockMvc mockMvc;

    @BeforeClass
    public static void prepareData() {
        List<Map<String, Object>> data = new ArrayList<>();
        Map<String, Object> firstTestData = new HashMap<>();
        firstTestData.put("id", 1L);
        firstTestData.put("name", "test1");
        firstTestData.put("short_description", "short desc1");
        firstTestData.put("creator_login", "login1");
        firstTestData.put("creator_fio", "Surname1 Name1");
        firstTestData.put("max_duration", 42);
        firstTestData.put("date_created", new Date(1521922433));
        data.add(firstTestData);

        Map<String, Object> secondTestData = new HashMap<>();
        secondTestData.put("id", 2L);
        secondTestData.put("name", "test2");
        secondTestData.put("short_description", "short desc2");
        secondTestData.put("creator_login", "login2");
        secondTestData.put("creator_fio", "Surname2 Name2");
        secondTestData.put("max_duration", 42);
        secondTestData.put("date_created", new Date(1521922433));
        data.add(secondTestData);

        TestServiceImpl testService = mock(TestServiceImpl.class);
        when(testService.getAllPublicTests(any(), any(), any(), any(), any()))
                .thenReturn(data);

        testInfoController = new TestInfoController(testService);
    }

    @Before
    public void prepareMockMvc() {
        mockMvc = standaloneSetup(testInfoController).build();
    }

    @org.junit.Test
    public void shouldReturnCorrectJSON() throws Exception {
        mockMvc.perform(get("/internal_api/tests/all_public"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].id", is(1)))
                .andExpect(jsonPath("$[0].name", is("test1")))
                .andExpect(jsonPath("$[0].short_description", is("short desc1")))
                .andExpect(jsonPath("$[0].creator_login", is("login1")))
                .andExpect(jsonPath("$[0].creator_fio", is("Surname1 Name1")))
                .andExpect(jsonPath("$[0].max_duration", is(42)))
                .andExpect(jsonPath("$[0].date_created", is(1521922433)))
                .andExpect(jsonPath("$[1].id", is(2)))
                .andExpect(jsonPath("$[1].name", is("test2")))
                .andExpect(jsonPath("$[1].short_description", is("short desc2")))
                .andExpect(jsonPath("$[1].creator_login", is("login2")))
                .andExpect(jsonPath("$[1].creator_fio", is("Surname2 Name2")))
                .andExpect(jsonPath("$[1].max_duration", is(42)))
                .andExpect(jsonPath("$[1].date_created", is(1521922433)));
    }
}
