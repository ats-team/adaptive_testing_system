package ru.kpfu.itis.adaptivetest.questionCreationProcessDataTests;

import org.junit.BeforeClass;
import ru.kpfu.itis.adaptivetest.models.AdaptedQuestion;
import ru.kpfu.itis.adaptivetest.models.Question;
import ru.kpfu.itis.adaptivetest.models.Test;
import ru.kpfu.itis.adaptivetest.repositories.QuestionRepository;
import ru.kpfu.itis.adaptivetest.repositories.TestRepository;
import ru.kpfu.itis.adaptivetest.services.implementations.QuestionServiceImpl;

import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.mockito.AdditionalMatchers.not;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;

/**
 * @author Bulat Giniyatullin
 * 12 April 2018
 */

public class QuestionCreationProcessDataServiceTest {
    private static QuestionServiceImpl questionService;

    @BeforeClass
    public static void prepareQuestionService() {
        Test test = Test.builder()
                .id(1)
                .name("testName")
                .build();
        List<AdaptedQuestion> levelsSet = new LinkedList<>();
        levelsSet.add(AdaptedQuestion.builder().id(1).level(1).build());
        levelsSet.add(AdaptedQuestion.builder().id(2).level(2).build());
        Question question = Question.builder()
                .id(1)
                .name("name")
                .levelCount(1)
                .test(test)
                .adaptedQuestions(levelsSet)
                .build();
        QuestionRepository questionRepository = mock(QuestionRepository.class);
        doReturn(Optional.of(question)).when(questionRepository).findById(0L);
        doReturn(Optional.empty()).when(questionRepository).findById(not(eq(0L)));

        questionService = new QuestionServiceImpl(questionRepository, mock(TestRepository.class));
    }

    @org.junit.Test
    public void shouldReturnCorrectData() {
        Map<String, Object> expected = new HashMap<>();
        expected.put("test_id", 1L);
        expected.put("test_name", "testName");
        expected.put("question_name", "name");
        List<Map<String, Object>> levels = new LinkedList<>();
        HashMap<String, Object> levelMap = new HashMap<>();
        levelMap.put("id", 1L);
        levelMap.put("level", 1);
        levels.add(levelMap);
        HashMap<String, Object> levelMap2 = new HashMap<>();
        levelMap2.put("id", 2L);
        levelMap2.put("level", 2);
        levels.add(levelMap2);
        expected.put("levels", levels);

        Map<String, Object> received = questionService.getQuestionWithLevelsById(0L);

        assertEquals(expected, received);
    }

    @org.junit.Test
    public void shouldReturnEmptyMapIfIdIsIncorrect() {
        Map<String, Object> received = questionService.getQuestionWithLevelsById(1L);

        assertThat(received)
                .isEmpty();
    }
}
