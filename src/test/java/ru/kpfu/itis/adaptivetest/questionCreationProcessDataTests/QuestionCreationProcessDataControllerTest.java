package ru.kpfu.itis.adaptivetest.questionCreationProcessDataTests;

import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import ru.kpfu.itis.adaptivetest.controllers.QuestionCreationProcessController;
import ru.kpfu.itis.adaptivetest.services.implementations.QuestionServiceImpl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.AdditionalMatchers.not;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

/**
 * @author Bulat Giniyatullin
 * 12 April 2018
 */

public class QuestionCreationProcessDataControllerTest {
    private static MockMvc mockMvc;

    @BeforeClass
    public static void prepareMockMvc() {
        QuestionServiceImpl questionService = mock(QuestionServiceImpl.class);
        Map<String, Object> data = new HashMap<>();
        data.put("test_id", 1L);
        data.put("test_name", "testName");
        List<Map<String, Object>> levels = new ArrayList<>();
        HashMap<String, Object> levelMap = new HashMap<>();
        levelMap.put("id", 1L);
        levels.add(levelMap);
        HashMap<String, Object> levelMap2 = new HashMap<>();
        levelMap2.put("id", 2L);
        levels.add(levelMap2);
        data.put("levels", levels);

        doReturn(data).when(questionService).getQuestionWithLevelsById(0L);
        doReturn(new HashMap<>()).when(questionService).getQuestionWithLevelsById(not(eq(0L)));

        QuestionCreationProcessController questionCreationProcessController =
                new QuestionCreationProcessController(questionService);

        mockMvc = standaloneSetup(questionCreationProcessController).build();
    }

    @Test
    public void shouldReturnCorrectJson() throws Exception {
        mockMvc.perform(get("/internal_api/question_creation_data/0"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.test_id", is(1)))
                .andExpect(jsonPath("$.test_name", is("testName")))
                .andExpect(jsonPath("$.levels", hasSize(2)))
                .andExpect(jsonPath("$.levels[*].id", containsInAnyOrder(1, 2)));
    }

    @Test
    public void shouldReturnEmptyJsonIfIdIdIncorrect() throws Exception {
        mockMvc.perform(get("/internal_api/question_creation_data/1"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(content().string("{}"));
    }
}
